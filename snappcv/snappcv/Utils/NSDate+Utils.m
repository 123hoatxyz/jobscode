//
//  NSDate+Utils.m
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)

+(NSString*)dateFormatDefault:(NSDate*)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];
}

+(NSString*)dateFormatCustom:(NSDate*)date format:(NSString*)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:date];
}

+(NSDate*)stringToDate:(NSString*)dateStr{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return date;
}

+(NSString*)birthdayFormat:(NSDate*)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    return [dateFormatter stringFromDate:date];
}

+(NSString*)titleDateFromNumber:(NSString*)number{
    NSInteger temp = [number integerValue];
    if (temp == 0) {
        return @"Today";
    }else if(temp == 1){
        return @"Yesterday";
    }
    return [NSString stringWithFormat:@"%@ days", number];
}

+(BOOL)isValidDateOfBirth:(NSString *)date{
    NSInteger age = 0;
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *birthDay = [dateFormat dateFromString:date];
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthDay
                                       toDate:now
                                       options:0];
    age = [ageComponents year];
    NSLog(@"isValidDateOfBirth %ld", (long)[ageComponents year]);
    if (!age) {
        return false;
    }
    if (age > 16) {
        return true;
    }
    return false;
}
@end
