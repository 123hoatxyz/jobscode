//
//  NSNull+Utils.h
//  snappcv
//
//  Created by Staff on 4/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNull (Utils)
+(BOOL)isNullClass:(id)object;
@end
