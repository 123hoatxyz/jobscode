//
//  NSNotificationCenter+Utils.m
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSNotificationCenter+Utils.h"

@implementation NSNotificationCenter (Utils)

+(void)postNotificationName:(NSString*)name{
    [[NSNotificationCenter defaultCenter] postNotificationName:name object:nil];
}

+(void)removeObserver:(NSString*)registerName{
    [[NSNotificationCenter defaultCenter] removeObserver:registerName];
}
@end
