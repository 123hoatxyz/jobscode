//
//  NSMutableDictionary+FilterUtils.h
//  snappcv
//
//  Created by Hoat Ha Van on 7/8/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (FilterUtils)

+(instancetype)dictWithTitle:(NSString*)title checked:(BOOL)checked;

+(instancetype)dictWithTitle:(NSString*)title valueId:(NSString*)valueId checked:(BOOL)checked;

-(void)setChecked:(BOOL)checked;

-(BOOL)isChecked;
//checked
+(instancetype)dictWithTitle:(NSString*)title subTitles:(NSArray*)subTitles identifier:(NSString*)identifier;

-(NSString*)getTitle;

-(NSString*)getIdentifier;

-(NSInteger)countItemsOfSubTitle;

//salary
+(instancetype)dictWithMinSalary:(NSString*)minSalary maxSalary:(NSString*)maxSalary;

-(void)setMaxSalary:(NSNumber*)value;

-(void)setMinSalary:(NSNumber*)value;

-(NSNumber*)getMaxSalary;

-(NSNumber*)getMinSalary;

//experience
+(instancetype)dictWithExperienceYear:(NSString*)year;

-(void)setExperienceYear:(NSString*)year;

-(NSString*)getExperienceYear;

-(void)setOpen:(BOOL)isOpen;

-(BOOL)getIsOpen;
@end
