//
//  NSFileManager+Utils.m
//  Agento
//
//  Created by Staff on 2/24/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSFileManager+Utils.h"

@implementation NSFileManager (Utils)
+(BOOL)fileExistsAtPath:(NSString*)path{
    NSFileManager *fileManger = [NSFileManager defaultManager];
    if ([fileManger fileExistsAtPath:path]) {
        return YES;
    }
    return NO;
}

+(BOOL)copyItemAtPath:(NSString*)source toPath:(NSString*)target{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    BOOL success = [fileManager copyItemAtPath:source toPath:target error:&error];
    if (!success) {
        NSLog(@"Error: %@ copy file from path: %@", error, source);
    }
    return success;
}
@end
