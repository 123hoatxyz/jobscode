//
//  NSArray+TSCUtils.h

#import <Foundation/Foundation.h>

@interface NSArray (TSCUtils)
-(BOOL)containsString:(NSString *)value;
//return NSArray with distinct object with a given keypath
-(NSArray *)distinctUnionObjectsForKeyPath:(NSString *)keypath;
//return NSArray with objects from array and a keypath
-(NSArray*)arrayByAddingObjectsFromArrayForKeyPath:(NSString*)keypath;
@end
