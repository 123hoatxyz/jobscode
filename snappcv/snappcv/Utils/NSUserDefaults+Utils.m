//
//  NSUserDefaults+Utils.m
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSUserDefaults+Utils.h"
#import "Constants.h"
#import "NSString+Utils.h"

@implementation NSUserDefaults (Utils)

+(void)deleteToken{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:kDictToken];
    [userDefault synchronize];
}

+(void)updateNewToken:(NSString*)token{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if (![NSString isEmptyString:[userDefault valueForKey:kDictToken]]) {
        [userDefault removeObjectForKey:kDictToken];
        [userDefault synchronize];
    }
    //update new value for Token key
    [userDefault setValue:token forKey:kDictToken];
    [userDefault synchronize];
}

+(BOOL)isHasToken{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [NSString isEmptyString:[userDefault valueForKey:kDictToken]]? NO: YES;
}

+(NSString*)token{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [self isHasToken]? [userDefault valueForKey:kDictToken] : nil;
}

+(void)updateProfile:(id)profile key:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:profile];
    [userDefault setObject:data forKey:key];
    [userDefault synchronize];
}

+(id)getProfile:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefault objectForKey:key];
    id result = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return result;
}
+(void)avatarBase64String:(NSString*)base64String{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:base64String forKey:kDictAvatarContentBase64String];
    [userDefault synchronize];
}

+(NSString*)avatarBase64String{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return  [userDefault objectForKey:kDictAvatarContentBase64String];
}

+(void)updateFunctionList:(id)function key:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:function];
    [userDefault setObject:data forKey:key];
    [userDefault synchronize];
}

+(id)getFunctionList:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *dictionaryData = [userDefault objectForKey:key];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    return dictionary;
}

+(void)updateNotificationState:(BOOL)state
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:state forKey:kDictNotification];
    [userDefault synchronize];
}

+(id)getNotificationState{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:kDictNotification];
}

+(void)saveSearchParams:(NSDictionary*)params{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:params];
    [userDefault setObject:data forKey:kDictParams];
    [userDefault synchronize];
}

+(void)deleteSearchParams{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:kDictParams];
    [userDefault synchronize];
}

+(NSDictionary*)getSearchParams{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *dictionaryData = [userDefault objectForKey:kDictParams];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    return dictionary;
}

+(void)saveRegisterInfo:(NSDictionary*)info{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:info];
    [userDefault setObject:data forKey:kDictRegisterInfo];
    [userDefault synchronize];
}

+(NSDictionary*)getRegisterInfo{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *dictionaryData = [userDefault objectForKey:kDictRegisterInfo];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    return dictionary;
}

+(void)saveDeviceToken:(NSString*)deviceToken{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:deviceToken forKey:kDictDeviceToken];
    [userDefault synchronize];
}

+(NSString*)getDeviceToken{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault valueForKey:kDictDeviceToken];
}

+(void)updateNationalityList:(id)function key:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:function];
    [userDefault setObject:data forKey:key];
    [userDefault synchronize];
}

+(NSDictionary*)getNationality:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *dictionaryData = [userDefault objectForKey:key];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    return dictionary;
}

@end
