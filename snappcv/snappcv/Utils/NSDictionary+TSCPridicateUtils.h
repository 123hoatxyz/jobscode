//
//  NSDictionary+TSCPridicateUtils.h
//  snappcv
//
//  Created by Hoat Ha Van on 7/5/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (TSCPridicateUtils)
// return list of SELF predicates
-(NSArray *)predicates;
// return and predicate
-(NSPredicate *)andPredicate;
// return or predicate
-(NSPredicate *)orPredicate;
@end
