//
//  NSNotificationCenter+Utils.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNotificationCenter (Utils)

+(void)postNotificationName:(NSString*)name;

+(void)removeObserver:(NSString*)registerName;

@end
