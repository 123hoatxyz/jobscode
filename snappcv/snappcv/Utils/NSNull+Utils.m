//
//  NSNull+Utils.m
//  snappcv
//
//  Created by Staff on 4/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSNull+Utils.h"

@implementation NSNull (Utils)
+(BOOL)isNullClass:(id)object{
    if ([object isKindOfClass:[NSNull class]]) {
        return YES;
    }
    return NO;
}
@end
