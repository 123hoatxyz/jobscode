//
//  UIApplication+Utils.m
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "UIApplication+Utils.h"
#import "SnappCVApplication.h"

@implementation UIApplication (Utils)

+(BOOL)canOpenURLAddress:(NSURL *)url{
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        return YES;
    }
    return NO;
}

+(void)openUrlAddress:(NSURL *)url{
    [[UIApplication sharedApplication] openURL:url];
}

+(void)setRootViewController:(UIViewController*)viewController{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    appDelegate.window.rootViewController = viewController;
    [appDelegate.window makeKeyAndVisible];
}

+(void)setWorkMode:(SnappCVWorkMode)workMode{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    appDelegate.workMode = workMode;
}

+(SnappCVWorkMode)isWorkMode{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    return appDelegate.workMode;
}

+(void)setChangedCenter:(BOOL)changed{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    appDelegate.isChangedCenter = changed;
}

+(BOOL)isChangedCenter{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    return appDelegate.isChangedCenter;
}

+(void)setLogin:(BOOL)state{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    appDelegate.isLogin = state;
}

+(BOOL)getLogin{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    return appDelegate.isLogin;
}

+(void)setOpenDropbox:(BOOL)state{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    appDelegate.isOpenDropbox = state;
}

+(BOOL)isLoginOpenDropbox{
    AppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
    return appDelegate.isOpenDropbox;
}

+(void)setEnableMultiTouches:(BOOL)enable{
    SnappCVApplication *application = (id)[self sharedApplication];
    [application enableMultiTouches:enable];
}
@end
