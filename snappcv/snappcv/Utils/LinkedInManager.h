//
//  LinkedInManager.h
//  Schmoozer
//
//  Created by Staff on 5/19/15.
//  Copyright (c) 2015 Larion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOSLinkedInAPI/LIALinkedInApplication.h>
#import <IOSLinkedInAPI/LIALinkedInAuthorizationViewController.h>
#import <IOSLinkedInAPI/LIALinkedInHttpClient.h>

@interface LinkedInManager : NSObject

@property(nonatomic, strong)        NSMutableDictionary      *linkedInUser;

+ (LinkedInManager *)sharedInstanced;

- (void)loginWithLinkedInSuccess:(void(^)(NSString * accessTokenData)) successBlock
                          cancel:(void(^)()) cancelBlock
                         failure:(void(^)(NSError * error)) failureBlock;

@end
