//
//  NSMutableDictionary+FilterUtils.m
//  snappcv
//
//  Created by Hoat Ha Van on 7/8/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSMutableDictionary+FilterUtils.h"
#import "Constants.h"

@implementation NSMutableDictionary (FilterUtils)
//default
+(instancetype)dictWithTitle:(NSString*)title checked:(BOOL)checked{
    return [NSMutableDictionary dictionaryWithObjects:@[title,@(checked)] forKeys:@[kDictTitle, kDictChecked]];
}

+(instancetype)dictWithTitle:(NSString*)title valueId:(NSString*)valueId checked:(BOOL)checked{
    return [NSMutableDictionary dictionaryWithObjects:@[title,@(checked), valueId] forKeys:@[kDictTitle, kDictChecked, kDictValueId]];
}

-(NSString*)getValueId{
    return [self valueForKey:kDictValueId];
}

-(void)setChecked:(BOOL)checked{
    [self setObject:@(checked) forKey:kDictChecked];
}

-(BOOL)isChecked{
    return [[self valueForKey:kDictChecked] isEqual:@1]? YES : NO;
}

//checked
+(instancetype)dictWithTitle:(NSString*)title subTitles:(NSArray*)subTitles identifier:(NSString*)identifier{
    return [self dictionaryWithObjects:@[title, (!subTitles?[NSArray array]:subTitles), identifier] forKeys:@[kDictTitle, kDictSubTitles, kDictIdentifier]];
}

-(NSString*)getTitle{
    return  [self valueForKey:kDictTitle];
}

-(NSString*)getIdentifier{
    return [self valueForKey:kDictIdentifier];
}

-(NSInteger)countItemsOfSubTitle{
    return [[self valueForKey:kDictSubTitles] count];
}

//salary
+(instancetype)dictWithMinSalary:(NSString*)minSalary maxSalary:(NSString*)maxSalary{
    return [NSMutableDictionary dictionaryWithObjects:@[minSalary, maxSalary] forKeys:@[kDictMinSalary, kDictMaxSalary]];
}

-(void)setMaxSalary:(NSNumber*)value{
    [self setValue:value forKey:kDictMaxSalary];
}

-(void)setMinSalary:(NSNumber*)value{
    [self setValue:value forKey:kDictMinSalary];
}

-(NSNumber*)getMaxSalary{
    return [self valueForKey:kDictMaxSalary];
}

-(NSNumber*)getMinSalary{
    return [self valueForKey:kDictMinSalary];
}

//experience
+(instancetype)dictWithExperienceYear:(NSString*)year{
    return [NSMutableDictionary dictionaryWithObject:year forKey:kDictExperienceYear];
}

-(void)setExperienceYear:(NSString*)year{
    [self setValue:year forKey:kDictExperienceYear];
}

-(NSString*)getExperienceYear{
    return [self valueForKey:kDictExperienceYear];
}

-(void)setOpen:(BOOL)isOpen{
    [self setValue:@(isOpen) forKey:@"isOpen"];
}

-(BOOL)getIsOpen{
    BOOL flag = [[self valueForKey:@"isOpen"] isEqual:@1]?YES:NO;
    return flag;
}
@end
