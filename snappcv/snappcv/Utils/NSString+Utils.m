//
//  NSString+Utils.m
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NSString+Utils.h"
@import UIKit;
@import MobileCoreServices;
@implementation NSString (Utils)
+(BOOL)isEmptyString:(NSString*)string{
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (string.length > 0) {
        return NO;
    }
    if (!string || [string isEqualToString:@""]) {
        return YES;
    }
    return NO;
}
+(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)isMale:(NSString*)value{
    return [[value lowercaseString] isEqualToString:@"male"]?YES:NO;
}

+(id)getGender:(NSString*)value{
    return [[value lowercaseString] isEqualToString:@"0"]?@YES:@NO;
}

+(NSString*)encodeToBase64String:(UIImage*)image{
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (NSString*) mimeTypeFromString: (NSString *) path {
    path = [path lowercaseString];
    if([path hasSuffix:@".pdf"] || [path hasSuffix:@"pdf"]){
        return @"application/pdf";
    }else if([path hasSuffix:@".txt"] || [path hasSuffix:@"txt"]){
        return @"text/plain";
    }else if([path hasSuffix:@".doc"] || [path hasSuffix:@".docx"] || [path hasSuffix:@"doc"] || [path hasSuffix:@"docx"]){
        return @"application/msword";
    }else if([path hasSuffix:@".mov"]){
        return @"video/quicktime";
    }else if([path hasSuffix:@".mp4"]){
        return @"video/mp4";
    }
    return @"";
}

+ (NSString*)getNationalityName:(NSString*)nationId dict:(NSDictionary*)dict{
    NSString *name = @"";
    for (NSDictionary *dictItem in dict)
    {
        NSString *itemId = [dictItem objectForKey:@"nationality_id"];
        
        if ([itemId isEqualToString:nationId])
        {
            name = [dictItem objectForKey:@"nationality_name"];
            break;
        }
    }
    return name;
}

+ (NSString*)getFunctionName:(NSString*)functionId dict:(NSDictionary*)dict{
    NSString *name = @"";
    for (NSDictionary *dictItem in dict)
    {
        NSInteger itemId = [[dictItem objectForKey:@"function_id"] integerValue];
        
        if (itemId == [functionId integerValue])
        {
            name = [dictItem objectForKey:@"function_name"];
            break;
        }
    }
    return name;
}

+ (NSString*)getIndustryName:(NSString*)industryId dict:(NSDictionary*)dict{
    NSString *name = @"";
    for (NSDictionary *dictItem in dict)
    {
        NSInteger itemId = [[dictItem objectForKey:@"industry_id"] integerValue];
        
        if (itemId == [industryId integerValue])
        {
            name = [dictItem objectForKey:@"industry_name"];
            break;
        }
    }
    return name;
}

+(BOOL)validPhone:(NSString *)phoneString{
    NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    
    NSRange inputRange = NSMakeRange(0, [phoneString length]);
    NSArray *matches = [detector matchesInString:phoneString options:0 range:inputRange];
    
    // no match at all
    if ([matches count] == 0) {
        return NO;
    }
    
    // found match but we need to check if it matched the whole string
    NSTextCheckingResult *result = (NSTextCheckingResult *)[matches objectAtIndex:0];
    
    if ([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length) {
        // it matched the whole string
        return YES;
    }
    else {
        // it only matched partial string
        return NO;
    }
}

+(BOOL)validExMonth:(NSString *)monthString{
    int month= [monthString intValue];
    if (month > 11 || month < 0) {
        return NO;
    }
    return YES;
}

+(BOOL)validExYear:(NSString *)yearString{
    int year= [yearString intValue];
    if (year > 40 || year < 0) {
        return NO;
    }
    return YES;
}

@end
