//
//  NSFileManager+Utils.h
//  Agento
//
//  Created by Staff on 2/24/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Utils)
//return YES if a file at specified path exists
+(BOOL)fileExistsAtPath:(NSString*)path;

//return YES if the item was copied successfully
+(BOOL)copyItemAtPath:(NSString*)source toPath:(NSString*)target;
@end
