//
//  UIImage+Addition.h

#import <UIKit/UIKit.h>


@interface UIImage (Addition)
+ (UIImage *) makeNinePatchImagedNamed:(NSString *)imageName;
+ (UIImage *) makeNinePatchImagedNamedNotClip:(NSString *)imageName;
+ (UIImage*)decodeBase64StringToImage:(NSString*)encodeString;
+ (UIImage *)compressImage:(UIImage *)original size:(CGSize)newSize;
@end
