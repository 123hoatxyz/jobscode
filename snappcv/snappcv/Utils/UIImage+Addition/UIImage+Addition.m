//
//  UIImage+Addition.m

#import "UIImage+Addition.h"
@import Foundation;
@import CoreGraphics;

@implementation UIImage (Addition)
+ (UIImage *) makeNinePatchImagedNamed:(NSString *)imageName;
{
    UIImage* image = [UIImage imageNamed:imageName];
    //Clear the black 9patch regions
    CGRect imageRect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(image.size, NO, [UIScreen mainScreen].scale);
    [image drawInRect:imageRect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextClearRect(context, CGRectMake(0, 0, image.size.width, 1));
    CGContextClearRect(context, CGRectMake(0, 0, 1, image.size.height));
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIEdgeInsets insets;
    
    //hard coded for now, could easily read the black regions if necessary
    insets.left = insets.right = image.size.width / 2 - 1;
    insets.top = insets.bottom = image.size.height / 2 - 1;
    
    UIImage *nineImage = [image resizableImageWithCapInsets:insets
                                               resizingMode:UIImageResizingModeStretch];
        
    return nineImage;
    
}


+ (UIImage *) makeNinePatchImagedNamedNotClip:(NSString *)imageName
{
    UIImage* image = [UIImage imageNamed:imageName];
    //Clear the black 9patch regions
    CGRect imageRect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(image.size, NO, [UIScreen mainScreen].scale);
    [image drawInRect:imageRect];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIEdgeInsets insets;
    
    //hard coded for now, could easily read the black regions if necessary
    insets.left = insets.right = image.size.width / 2 - 1;
    insets.top = insets.bottom = image.size.height / 2 - 1;
    
    UIImage *nineImage = [image resizableImageWithCapInsets:insets
                                               resizingMode:UIImageResizingModeStretch];
    
    return nineImage;
}

+(UIImage*)decodeBase64StringToImage:(NSString*)encodeString{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:encodeString options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

+ (UIImage *)compressImage:(UIImage *)original size:(CGSize)newSize
{
    // Calculate new size given scale factor.
    //CGSize originalSize = original.size;
    //CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    
    // Scale the original image to match the new size.
    UIGraphicsBeginImageContext(newSize);
    [original drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* compressedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return compressedImage;
}
@end
