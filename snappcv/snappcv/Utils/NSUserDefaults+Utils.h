//
//  NSUserDefaults+Utils.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Utils)
//
//Delete a token
//
+(void)deleteToken;

//
//Update a new token to NSUserDefaults
//
+(void)updateNewToken:(NSString*)token;

//
//Check a token is existence
//
+(BOOL)isHasToken;

//
//Get token
//
+(NSString*)token;

//
//Save a profile to local application
//
+(void)updateProfile:(id)profile key:(NSString*)key;

//
//get a profile with a given key
//
+(id)getProfile:(NSString*)key;

//
//save avatar image
//
+(void)avatarBase64String:(NSString*)base64String;
//
//get a avatar image
//
+(NSString*)avatarBase64String;

+(void)updateFunctionList:(id)function key:(NSString*)key;

+(id)getFunctionList:(NSString*)key;

+(void)updateNotificationState:(BOOL)state;

+(id)getNotificationState;

+(void)saveSearchParams:(NSDictionary*)params;

+(void)deleteSearchParams;

+(NSDictionary*)getSearchParams;

+(NSDictionary*)getRegisterInfo;

+(void)saveDeviceToken:(NSString*)deviceToken;

+(NSString*)getDeviceToken;

+(void)updateNationalityList:(id)function key:(NSString*)key;

+(NSDictionary*)getNationality:(NSString*)key;

@end
