//
//  NSDate+Utils.h
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

+(NSString*)dateFormatDefault:(NSDate*)date;

+(NSDate*)stringToDate:(NSString*)dateStr;

+(NSString*)birthdayFormat:(NSDate*)date;

+(NSString*)dateFormatCustom:(NSDate*)date format:(NSString*)format;

+(NSString*)titleDateFromNumber:(NSString*)number;

+(BOOL)isValidDateOfBirth:(NSString*)date;

@end

