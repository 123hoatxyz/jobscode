//
//  LinkedInManager.m
//  Schmoozer
//
//  Created by Staff on 5/19/15.
//  Copyright (c) 2015 Larion. All rights reserved.
//

#import "LinkedInManager.h"
#import "Constants.h"

@implementation LinkedInManager{
    LIALinkedInHttpClient *_client;
}

+ (LinkedInManager *)sharedInstanced
{
    static dispatch_once_t predicate = 0;
    __strong static LinkedInManager * sharedObject = nil;
    
    dispatch_once(&predicate, ^(){
        sharedObject = [[self alloc] init];
    });
    
    return sharedObject;
}

- (LIALinkedInHttpClient *)client {
    
    NSString *callBackUrl = @"https://developer.linkedin.com/";//@"x-oauthflow-linkedin://litestcalback";
    
    LIALinkedInApplication *application = [LIALinkedInApplication applicationWithRedirectURL:callBackUrl
                                                                                    clientId:LINKEDIN_CLIENT_ID
                                                                                clientSecret:LINKEDIN_CLIENT_SECRET
                                                                                       state:@"DCEEFWF45453sdffef424"
                                                                               grantedAccess:@[@"r_basicprofile", @"r_emailaddress", @"w_share"]];
    return [LIALinkedInHttpClient clientForApplication:application presentingViewController:nil];
}

- (void)requestMeWithToken:(NSString *)accessToken {
    [self.client GET:[NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~?oauth2_access_token=%@&format=json", accessToken] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *result) {
        NSLog(@"current user %@", result);
    }        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failed to fetch current user %@", error);
    }];
}

- (void)loginWithLinkedInSuccess:(void(^)(NSString * accessToken)) successBlock
                                cancel:(void(^)()) cancelBlock
                                    failure:(void(^)(NSError * error)) failureBlock
{
    NSLog(@"did press login");
    [self.client getAuthorizationCode:^(NSString *code) {
        [self.client getAccessToken:code success:^(NSDictionary *accessTokenData) {
            
            
            NSString *accessToken = [accessTokenData objectForKey:@"access_token"];
            [self.client GET:[NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~?oauth2_access_token=%@&format=json", accessToken] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *result) {
                NSLog(@"current user %@", result);
                successBlock(accessToken);

            }            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                failureBlock(error);
                NSLog(@"failed to fetch current user %@", error);
            }];
        }                   failure:^(NSError *error) {
            failureBlock(error);
            NSLog(@"Quering accessToken failed %@", error);
        }];
    }                          cancel:^{
        cancelBlock();
        NSLog(@"Authorization was cancelled by user");
    }                         failure:^(NSError *error) {
        failureBlock(error);
        NSLog(@"Authorization failed %@", error);
    }];
    
}

- (void) loadSiteStandardProfileRequest: (NSString *) url
{
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary * responseObject) {
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}


- (NSMutableDictionary *)linkedInUser
{
    if (!_linkedInUser) {
        _linkedInUser = [[NSMutableDictionary alloc] init];
    }
    
    return _linkedInUser;
}

@end
