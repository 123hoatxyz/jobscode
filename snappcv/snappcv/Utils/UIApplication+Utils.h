//
//  UIApplication+Utils.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UIApplication (Utils)

//can open url by UIApplication URL or not
+(BOOL)canOpenURLAddress:(NSURL*)url;

//open the url address by UIApplication
+(void)openUrlAddress:(NSURL*)url;

+(void)setRootViewController:(UIViewController*)viewController;

+(void)setWorkMode:(SnappCVWorkMode)workMode;

+(SnappCVWorkMode)isWorkMode;

+(void)setChangedCenter:(BOOL)changed;

+(BOOL)isChangedCenter;

+(void)setLogin:(BOOL)state;

+(BOOL)getLogin;

+(void)setOpenDropbox:(BOOL)state;

+(BOOL)isLoginOpenDropbox;

+(void)setEnableMultiTouches:(BOOL)enable;

@end
