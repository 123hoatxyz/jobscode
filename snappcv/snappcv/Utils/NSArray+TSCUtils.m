//
//  NSArray+TSCUtils.m


#import "NSArray+TSCUtils.h"

@implementation NSArray (TSCUtils)
-(BOOL)containsString:(NSString *)value
{
    for (id element in self) {
        if ([value isEqualToString:element]) {
            return YES;
        }
    }
    return NO;
}
//return NSArray with distinct object with a given keypath
-(NSArray *)distinctUnionObjectsForKeyPath:(NSString *)keypath
{
    return [self valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.%@", keypath]];
}
//return NSArray with objects from array and a keypath
-(NSArray*)arrayByAddingObjectsFromArrayForKeyPath:(NSString*)keypath {
    NSMutableArray *listArray = [NSMutableArray arrayWithCapacity:10];
    for (NSObject *obj in self) {
        if ([obj valueForKeyPath:keypath]) {
            [listArray addObject:@{keypath: [obj valueForKeyPath:keypath]}];
        }
    }
    return listArray;
}

@end
