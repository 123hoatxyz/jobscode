//
//  NSString+Utils.h
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIImage;

@interface NSString (Utils)

+(BOOL)isEmptyString:(NSString*)string;

+(BOOL) isValidEmail:(NSString *)checkString;

+(BOOL)isMale:(NSString*)value;

+(id)getGender:(NSString*)value;

+(NSString*)encodeToBase64String:(UIImage*)image;

+ (NSString*) mimeTypeFromString: (NSString *) path;

+ (NSString*)getNationalityName:(NSString*)nationId dict:(NSDictionary*)dict;

+ (NSString*)getFunctionName:(NSString*)functionId dict:(NSDictionary*)dict;

+ (NSString*)getIndustryName:(NSString*)industryId dict:(NSDictionary*)dict;

+ (BOOL) validPhone:(NSString*) phoneString;

+ (BOOL) validExMonth:(NSString*) monthString;

+ (BOOL) validExYear:(NSString*) yearString;
@end
