//
//  BasicInfo.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasicInfo : NSObject<NSCoding>
@property NSString *email;
@property NSString *password;
@property NSString *password_confirmation;
@property NSString *first_name;
@property NSString *last_name;
@property NSString *token;
@end
