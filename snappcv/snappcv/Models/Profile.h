//
//  Profile.h
//  snappcv
//
//  Created by Staff on 4/8/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasicInfo.h"
#import "PersonalDetail.h"
#import "ProfessionDetail.h"
#import "Preference.h"

@interface Profile : NSObject<NSCoding>
@property (strong, nonatomic) BasicInfo *basicInfo;
@property (strong, nonatomic) PersonalDetail *personalDetail;
@property (strong, nonatomic) ProfessionDetail *professionDetail;
@property (strong, nonatomic) Preference *preference;
@end
