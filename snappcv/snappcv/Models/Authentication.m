//
//  Authentication.m
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "Authentication.h"
#import "Constants.h"
@implementation Authentication
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.authentication_token = [aDecoder decodeObjectForKey:kDictAuthenticationToken];
    self.candidate_id = [aDecoder decodeObjectForKey:kDictCandidateId];
    self.client_id = [aDecoder decodeObjectForKey:kDictClientID];
    self.name = [aDecoder decodeObjectForKey:kDictName];
    self.id = [aDecoder decodeIntegerForKey:kDictID];
    self.name = [aDecoder decodeObjectForKey:kDictName];
    
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    
}
@end
