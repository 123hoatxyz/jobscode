//
//  ProfessionDetail.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfessionDetail : NSObject <NSCoding>

@property NSString *jobType;
@property NSString *experienceYears;
@property NSString *experienceMonths;
@property NSString *currentIndustry;
@property NSString *function;
@property NSString *nation;
@property NSString *keySkills;
@property NSString *cvTitle;
@property NSData *dataCV;
@property NSData *dataAvatar;

@end
