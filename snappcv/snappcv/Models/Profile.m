//
//  Profile.m
//  snappcv
//
//  Created by Staff on 4/8/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "Profile.h"

@implementation Profile
-(instancetype)init{
    self = [super init];
    if (self) {
        self.basicInfo = [[BasicInfo alloc] init];
        self.preference = [[Preference alloc] init];
        self.personalDetail = [[PersonalDetail alloc]init];
        self.professionDetail = [[ProfessionDetail alloc] init];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.basicInfo = [aDecoder decodeObjectForKey:@"basicInfo"];
    self.personalDetail = [aDecoder decodeObjectForKey:@"personalDetail"];
    self.preference = [aDecoder decodeObjectForKey:@"preference"];
    self.professionDetail = [aDecoder decodeObjectForKey:@"professionDetail"];
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.basicInfo forKey:@"basicInfo"];
    [aCoder encodeObject:self.professionDetail forKey:@"professionDetail"];
    [aCoder encodeObject:self.preference forKey:@"preference"];
    [aCoder encodeObject:self.personalDetail forKey:@"personalDetail"];
}
@end
