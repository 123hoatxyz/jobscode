//
//  PersonalDetail.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "PersonalDetail.h"
#import "Constants.h"

@implementation PersonalDetail

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.nationality = [aDecoder decodeObjectForKey:kDictNationality];
    self.currentLocation = [aDecoder decodeObjectForKey:kDictCurrentLocation];
    self.phoneNumber = [aDecoder decodeObjectForKey:kDictPhoneNumber];
    self.dateOfBirth = [aDecoder decodeObjectForKey:kDictDateOfBirth];
    self.avatarURL = [aDecoder decodeObjectForKey:kDictAvatarURL];
    self.gender = [aDecoder decodeObjectForKey:kDictGender];
    self.avatarBase64String = [aDecoder decodeObjectForKey:kDictAvatarContentBase64String];
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.nationality forKey:kDictNationality];
    [aCoder encodeObject:self.currentLocation forKey:kDictCurrentLocation];
    [aCoder encodeObject:self.phoneNumber forKey:kDictPhoneNumber];
    [aCoder encodeObject:self.dateOfBirth forKey:kDictDateOfBirth];
    [aCoder encodeObject:self.avatarURL forKey:kDictAvatarURL];
    [aCoder encodeObject:self.gender forKey:kDictGender];
    [aCoder encodeObject:self.avatarBase64String forKey:kDictAvatarContentBase64String];
}
@end
