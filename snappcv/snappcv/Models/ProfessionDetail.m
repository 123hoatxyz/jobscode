//
//  ProfessionDetail.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "ProfessionDetail.h"
#import "Constants.h"
@implementation ProfessionDetail
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.jobType = [aDecoder decodeObjectForKey:kDictJobType];
    self.experienceYears = [aDecoder decodeObjectForKey:kDictExperienceYear];
    self.experienceMonths = [aDecoder decodeObjectForKey:kDictExperienceMonth];
    self.currentIndustry = [aDecoder decodeObjectForKey:kDictCurrentIndustry];
    self.function = [aDecoder decodeObjectForKey:kDictFunction];
    self.cvTitle = [aDecoder decodeObjectForKey:kDictCVTitle];
    self.keySkills = [aDecoder decodeObjectForKey:kDictKeySkills];
    self.dataAvatar = [aDecoder decodeObjectForKey:@"dataAvatar"];
    self.dataCV = [aDecoder decodeObjectForKey:@"dataCV"];
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.jobType forKey:kDictJobType];
    [aCoder encodeObject:self.experienceYears forKey:kDictExperienceYear];
    [aCoder encodeObject:self.experienceMonths forKey:kDictExperienceMonth];
    [aCoder encodeObject:self.currentIndustry forKey:kDictCurrentIndustry];
    [aCoder encodeObject:self.function forKey:kDictFunction];
    [aCoder encodeObject:self.cvTitle forKey:kDictCVTitle];
    [aCoder encodeObject:self.keySkills forKey:kDictKeySkills];
    [aCoder encodeObject:self.dataAvatar forKey:@"dataAvatar"];
    [aCoder encodeObject:self.dataCV forKey:@"dataCV"];
}
@end
