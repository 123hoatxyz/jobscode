//
//  Authentication.h
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Authentication : NSObject<NSCoding>
@property NSString *authentication_token;
@property NSString *candidate_id;
@property NSString *client_id;
@property NSString *email;
@property NSInteger id;
@property NSString *name;
@property NSString *role;
@property NSString *snapp_role;
@property NSDate *updated_at;
@end
