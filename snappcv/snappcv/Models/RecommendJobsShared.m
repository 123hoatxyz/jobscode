//
//  RecommendJobsShared.m
//  snappcv
//
//  Created by Hoat Ha Van on 7/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "RecommendJobsShared.h"
#import "NSUserDefaults+Utils.h"
#import "Constants.h"
#import "NSArray+TSCUtils.h"
#import "NSPredicate+TSCUtils.h"
#import "NSMutableDictionary+FilterUtils.h"
#import "Job.h"

@implementation RecommendJobsShared

+(instancetype)defaultShared{
    static RecommendJobsShared *obj = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken, ^{
        obj = [[RecommendJobsShared alloc] init];
    });
    return obj;
}

-(NSArray*)arrayDictionaryMatchedLocation{
    if ([self.arrRecommendJobs count]) {
        
        NSArray *arrBaseLocations = [NSUserDefaults getFunctionList:kDictLocationKey];
        NSArray *arrDistinctLocations = [self.arrRecommendJobs distinctUnionObjectsForKeyPath:kDictJobLocationId];
        NSPredicate *predicate = [NSPredicate inPredicateWithValues:arrDistinctLocations forKey:kDictJobLocationId];
        NSArray *result = [arrBaseLocations filteredArrayUsingPredicate:predicate];
        return result;
    }
    return nil;
}

-(NSArray*)arrayCompanyName{
    if ([self.arrRecommendJobs count]) {
        NSArray *arrDistinctCompanyName = [self.arrRecommendJobs distinctUnionObjectsForKeyPath:kDictJobCompanyName];
        return arrDistinctCompanyName;
    }
    return nil;
}
-(NSArray*)arrayDicCompany{

    if ([self.arrRecommendJobs count]) {
        NSMutableArray *arrJobs =[NSMutableArray arrayWithCapacity:[self.arrRecommendJobs count]];
        NSMutableSet *filterJobs = [NSMutableSet setWithCapacity:[self.arrRecommendJobs count]];
        for (Job *job in self.arrRecommendJobs) {
            NSString *stringLocationId = [NSString stringWithFormat:@"%ld",(long)job.company_id];
            if (![filterJobs containsObject:stringLocationId]) {
                NSMutableDictionary *dic = [NSMutableDictionary dictWithTitle:job.company_name valueId:stringLocationId checked:NO];
                [arrJobs addObject:dic];
                [filterJobs addObject:stringLocationId];
            }
            
        }
        return arrJobs;
    }
    return nil;
}
@end
