//
//  PersonalDetail.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonalDetail : NSObject<NSCoding>
@property NSString *nationality;
@property NSString *currentLocation;
@property NSString *phoneNumber;
@property NSString *dateOfBirth;
@property NSURL *avatarURL;
@property NSString *avatarBase64String;
@property NSString *gender;
@end
