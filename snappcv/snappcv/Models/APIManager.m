//
//  APIManager.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "APIManager.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "Authentication.h"
#import "NSString+Utils.h"
#import "NSUserDefaults+Utils.h"
#import "ErrorMessage.h"
#import "Job.h"
#import "NSNull+Utils.h"
#import "Profile.h"
#import "UIDevice+Utils.h"
@implementation APIManager

+(instancetype)sharedManager{
    static APIManager *obj = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        obj = [[APIManager alloc] init];
    });
    return obj;
}


-(void)loginWithEmail:(NSString*)email password:(NSString*)password completion:(void(^)(NSDictionary *data, BOOL success, NSString *error))completion{
    if (DEBUG) {
        NSLog(@"Request to:%@", [NSString stringWithFormat:@"%@%@", kServerAddress, kLoginAPI]);
    }
    
    AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
    [requestOperationManager POST:[NSString stringWithFormat:@"%@%@", kServerAddress, kLoginAPI] parameters:
     @{@"email":email, @"password":password, @"device_token":[NSUserDefaults getDeviceToken], @"device_type":[UIDevice stringNameDeviceType]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
         
         NSLog(@"Device: %@",[UIDevice stringNameDeviceType]);
         
         BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
         
         if (isResult) {
             NSDictionary *data = [responseObject valueForKey:kDictData];
             [NSUserDefaults updateNewToken:[data valueForKey:kDictToken]];
             
             Profile *aProfile = [[Profile alloc] init];
             
             id firstName = [data valueForKey:kDictFirstName];
             
             aProfile.basicInfo.email = email;
             
             aProfile.basicInfo.first_name = [NSNull isNullClass:firstName]?@"":firstName;
             
             id lastName = [data valueForKey:kDictLastName];
             aProfile.basicInfo.last_name = [NSNull isNullClass:lastName]?@"":lastName;
             
             id avatarURL = [data valueForKey:kDictAvatarURL];
             aProfile.personalDetail.avatarURL = [NSNull isNullClass:avatarURL]?nil:[NSURL URLWithString:avatarURL];
             
             id nationality = [data valueForKey:kDictNationality];
             aProfile.personalDetail.nationality = [NSNull isNullClass:nationality]?@"":nationality;
             
             id location = [data valueForKey:kDictCurrentLocation];
             aProfile.personalDetail.currentLocation = [NSNull isNullClass:location]?@"":location;
             
             id phoneNumber = [data valueForKey:kDictPhoneNumber];
             aProfile.personalDetail.phoneNumber = [NSNull isNullClass:phoneNumber]?@"":phoneNumber;
             
             id dateOfBirth = [data valueForKey:kDictDateOfBirth];
             aProfile.personalDetail.dateOfBirth = [NSNull isNullClass:dateOfBirth]?@"":dateOfBirth;
             
             id gender = [data valueForKey:kDictGender];
             aProfile.personalDetail.gender = [NSNull isNullClass:gender]?@"0":[NSString stringWithFormat:@"%ld", (long)[gender integerValue]];
             
             id jobType = [data valueForKey:kDictJobType];
             aProfile.professionDetail.jobType = [NSNull isNullClass:jobType]?@"":jobType;
             
             id years = [data valueForKey:kDictExperienceYear];
             aProfile.professionDetail.experienceYears = [NSNull isNullClass:years]?@"0":[NSString stringWithFormat:@"%ld", (long)[years integerValue]];
             
             id months = [data valueForKey:kDictExperienceMonth];
             aProfile.professionDetail.experienceMonths = [NSNull isNullClass:months]?@"0":[NSString stringWithFormat:@"%ld", (long)[months integerValue]];
             
             id industry = [data valueForKey:kDictCurrentIndustry];
             aProfile.professionDetail.currentIndustry = [NSNull isNullClass:industry]?@"0":[NSString stringWithFormat:@"%ld", (long)[industry integerValue]];
             
             id function = [data valueForKey:kDictFunction];
             aProfile.professionDetail.function = [NSNull isNullClass:function]?@"0":[NSString stringWithFormat:@"%ld", (long)[function integerValue]];
             
             id keySkills = [data valueForKey:kDictKeySkills];
             aProfile.professionDetail.keySkills = [NSNull isNullClass:keySkills]?@"":keySkills;
             
             id cvTitle = [data valueForKey:kDictCVTitle];
             aProfile.professionDetail.cvTitle = [NSNull isNullClass:cvTitle]?@"":cvTitle;
             
             
             aProfile.preference.subscribe = [[data valueForKey:kDictSubscribe] isEqual:@1]?YES:NO;
             
             aProfile.preference.notice = [[data valueForKey:kDictNotice] isEqual:@1]?YES:NO;
             
             //save function list
             NSDictionary *functions = [data valueForKey:kDictAllFunctions];
             [NSUserDefaults updateFunctionList:functions key:kDictFunctionKey];
             
             //save nationallity
             NSDictionary *nation = [data valueForKey:kDictAllNations];
             [NSUserDefaults updateNationalityList:nation key:kDictNationKey];
             
             //save industry list
             NSDictionary *industries = [data valueForKey:kDictAllIndustry];
             [NSUserDefaults updateFunctionList:industries key:kDictIndustryKey];
             
             //save location list
             NSDictionary *locations = [data valueForKey:kDictAllLocation];
             [NSUserDefaults updateFunctionList:locations key:kDictLocationKey];
             
             //save to disk
             [NSUserDefaults updateProfile:aProfile key:kDictProfile];
             completion(data, isResult, [responseObject valueForKey:kDictMessage]);
         }else{
             completion(nil, NO, [operation.responseObject valueForKey:kDictMessage]);
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         completion(nil, NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
     }];
}

-(void)loginWithLinkedIn:(NSString*)accessToken accessSecret:(NSString*)accessSecret completion:(void(^)(NSDictionary *data, BOOL success, NSString *error))completion{
    if (DEBUG) {
        NSLog(@"Request to:%@", [NSString stringWithFormat:@"%@%@", kServerAddress, kLoginLinkedInAPI]);
    }
    
    AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
    [requestOperationManager POST:[NSString stringWithFormat:@"%@%@", kServerAddress, kLoginLinkedInAPI]
                       parameters:@{@"access_token":accessToken, @"device_token":[NSUserDefaults getDeviceToken], @"device_type":[UIDevice stringNameDeviceType]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
                           
                           BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
                           
                           if (isResult) {
                               NSDictionary *data = [responseObject valueForKey:kDictData];
                               [NSUserDefaults updateNewToken:[data valueForKey:kDictToken]];
                               
                               Profile *aProfile = [[Profile alloc] init];
                               
                               id firstName = [data valueForKey:kDictFirstName];
                               
                               aProfile.basicInfo.email = [data valueForKey:kDictEmail];;
                               
                               aProfile.basicInfo.first_name = [NSNull isNullClass:firstName]?@"":firstName;
                               
                               id lastName = [data valueForKey:kDictLastName];
                               aProfile.basicInfo.last_name = [NSNull isNullClass:lastName]?@"":lastName;
                               
                               id avatarURL = [data valueForKey:kDictAvatarURL];
                               aProfile.personalDetail.avatarURL = [NSNull isNullClass:avatarURL]?nil:[NSURL URLWithString:avatarURL];
                               
                               id nationality = [data valueForKey:kDictNationality];
                               aProfile.personalDetail.nationality = [NSNull isNullClass:nationality]?@"":nationality;
                               
                               id location = [data valueForKey:kDictCurrentLocation];
                               aProfile.personalDetail.currentLocation = [NSNull isNullClass:location]?@"":location;
                               
                               id phoneNumber = [data valueForKey:kDictPhoneNumber];
                               aProfile.personalDetail.phoneNumber = [NSNull isNullClass:phoneNumber]?@"":phoneNumber;
                               
                               id dateOfBirth = [data valueForKey:kDictDateOfBirth];
                               aProfile.personalDetail.dateOfBirth = [NSNull isNullClass:dateOfBirth]?@"":dateOfBirth;
                               
                               id gender = [data valueForKey:kDictGender];
                               aProfile.personalDetail.gender = [NSNull isNullClass:gender]?@"0":[NSString stringWithFormat:@"%ld", (long)[gender integerValue]];
                               
                               id jobType = [data valueForKey:kDictJobType];
                               aProfile.professionDetail.jobType = [NSNull isNullClass:jobType]?@"":jobType;
                               
                               id years = [data valueForKey:kDictExperienceYear];
                               aProfile.professionDetail.experienceYears = [NSNull isNullClass:years]?@"0":[NSString stringWithFormat:@"%ld", (long)[years integerValue]];
                               
                               id months = [data valueForKey:kDictExperienceMonth];
                               aProfile.professionDetail.experienceMonths = [NSNull isNullClass:months]?@"0":[NSString stringWithFormat:@"%ld", (long)[months integerValue]];
                               
                               id industry = [data valueForKey:kDictCurrentIndustry];
                               aProfile.professionDetail.currentIndustry = [NSNull isNullClass:industry]?@"0":[NSString stringWithFormat:@"%ld", (long)[industry integerValue]];
                               
                               id function = [data valueForKey:kDictFunction];
                               aProfile.professionDetail.function = [NSNull isNullClass:function]?@"0":[NSString stringWithFormat:@"%ld", (long)[function integerValue]];
                               
                               id keySkills = [data valueForKey:kDictKeySkills];
                               aProfile.professionDetail.keySkills = [NSNull isNullClass:keySkills]?@"":keySkills;
                               
                               id cvTitle = [data valueForKey:kDictCVTitle];
                               aProfile.professionDetail.cvTitle = [NSNull isNullClass:cvTitle]?@"":cvTitle;
                               
                               
                               aProfile.preference.subscribe = [[data valueForKey:kDictSubscribe] isEqual:@1]?YES:NO;
                               
                               aProfile.preference.notice = [[data valueForKey:kDictNotice] isEqual:@1]?YES:NO;
                               
                               //save function list
                               NSDictionary *functions = [data valueForKey:kDictAllFunctions];
                               [NSUserDefaults updateFunctionList:functions key:kDictFunctionKey];
                               
                               //save nationallity
                               NSDictionary *nation = [data valueForKey:kDictAllNations];
                               [NSUserDefaults updateNationalityList:nation key:kDictNationKey];
                               
                               //save industry list
                               NSDictionary *industries = [data valueForKey:kDictAllIndustry];
                               [NSUserDefaults updateFunctionList:industries key:kDictIndustryKey];
                               
                               //save to disk
                               [NSUserDefaults updateProfile:aProfile key:kDictProfile];
                               completion(data, isResult, [responseObject valueForKey:kDictMessage]);
                           }else{
                               completion(nil, NO, [operation.responseObject valueForKey:kDictMessage]);
                           }
                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                           completion(nil, NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
                       }];
}



-(void)logoutUserWithCompletion:(void(^)(BOOL success, NSString *error))completion{
    
    if (DEBUG) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@%@", kServerAddress, kLogoutAPI]);
    }
    
    if ([NSUserDefaults isHasToken]) {
        AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
        requestOperationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        [requestOperationManager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@", [NSUserDefaults token]] forHTTPHeaderField:kHeaderAuthorization];
        
        [requestOperationManager DELETE:[NSString stringWithFormat:@"%@%@", kServerAddress, kLogoutAPI] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
            if (isResult) {
                [NSUserDefaults updateNewToken:[[responseObject valueForKey:kDictData] valueForKey:kDictToken]];
                completion(isResult, [responseObject valueForKey:kDictMessage]);
            }
            else{
                completion(NO, [operation.responseObject valueForKey:kDictMessage]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [NSUserDefaults deleteToken];
            completion(NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
        }];
    }else{
        [NSUserDefaults deleteToken];
        completion(NO, @"");
    }
}


-(void)resetPasswordWithAnEmail:(NSString*)email completion:(void(^)(BOOL success, NSString* message))completion{
    
    if (DEBUG) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@%@", kServerAddress, kResetPasswordAPI]);
    }
    AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
    
    [requestOperationManager POST:[NSString stringWithFormat:@"%@%@", kServerAddress, kResetPasswordAPI] parameters:@{kDictEmail : email} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
        if (isResult) {
            completion(YES, [responseObject valueForKey:kDictMessage]);
        }
        else{
            completion(NO, [operation.responseObject valueForKey:kDictMessage]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completion(NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
    }];
}

-(void)changePassword:(NSString*)currentPassword newPassword:(NSString*)newPassword completion:(void(^)(BOOL success, NSString* message))completion{
    
    if (DEBUG) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@%@", kServerAddress, kChangePasswordAPI]);
    }
    AFHTTPRequestOperationManager *requestOperationManager = [AFHTTPRequestOperationManager manager];
    requestOperationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [requestOperationManager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[NSUserDefaults token]] forHTTPHeaderField:kHeaderAuthorization];
    [requestOperationManager PUT:[NSString stringWithFormat:@"%@%@", kServerAddress, kChangePasswordAPI] parameters:@{@"password_old" : currentPassword, @"password_new" : newPassword} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
        if (isResult) {
            completion(YES, [responseObject valueForKey:kDictMessage]);
        }
        else{
            completion(NO, [operation.responseObject valueForKey:kDictMessage]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completion(NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
    }];
}

-(void)registerUser:(NSDictionary *)parameters completion:(void (^)(BOOL success, NSString* message))completion{
    
    if (DEBUG) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@%@", kServerAddress, kRegisterAPI]);
    }
    //#pragma mark [Test]-HoatHV
    //    completion (YES, nil);
    //    return;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager POST:[NSString stringWithFormat:@"%@%@", kServerAddress, kRegisterAPI] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
        if (isResult) {
            NSDictionary *data = [responseObject valueForKey:kDictData];
            [NSUserDefaults updateNewToken:[data valueForKey:kDictToken]];
            
            //save function list
            NSDictionary *functions = [data valueForKey:kDictAllFunctions];
            [NSUserDefaults updateFunctionList:functions key:kDictFunctionKey];
            
            //save nationallity
            NSDictionary *nation = [data valueForKey:kDictAllNations];
            [NSUserDefaults updateNationalityList:nation key:kDictNationKey];
            
            //save industry list
            NSDictionary *industries = [data valueForKey:kDictAllIndustry];
            [NSUserDefaults updateFunctionList:industries key:kDictIndustryKey];

            completion(isResult, [responseObject valueForKey:kDictMessage]);
        }
        else{
            completion(NO, [operation.responseObject valueForKey:kDictMessage]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        completion(NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
    }];
}

-(void)registerUser2:(NSDictionary *)parameters completion:(void (^)(BOOL success, NSString* message, NSString *token))completion{
    
    if (DEBUG) {
        NSLog(@"%@", [NSString stringWithFormat:@"%@%@", kServerAddress, kRegisterAPI]);
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager POST:[NSString stringWithFormat:@"%@%@", kServerAddress, kRegisterAPI] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
        if (isResult) {
            NSString *token = [responseObject valueForKey:kDictToken];
            [NSUserDefaults updateNewToken:[[responseObject valueForKey:kDictData] valueForKey:kDictToken]];
            completion(isResult, [responseObject valueForKey:kDictMessage], token);
        }
        else{
            completion(NO, [operation.responseObject valueForKey:kDictMessage], nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        completion(NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect, nil);
    }];
}

-(void)updateCandidateProfile:(id)parameters completion:(void(^)(BOOL success, NSString *message, NSString *avatarURL))completion{
    
    if ([NSUserDefaults isHasToken]) {
        
        NSString *linkURL = [NSString stringWithFormat:@"%@%@", kServerAddress, kUpdateCandatidateProfileAPI];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[NSUserDefaults token]] forHTTPHeaderField:kHeaderAuthorization];
        [manager PUT:linkURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
            
            if (isResult) {
                NSDictionary *data = [responseObject valueForKey:kDictData];
                NSString *avatarURL = [data valueForKey:kDictAvatarURL];
                completion(isResult, [responseObject valueForKey:kDictMessage], avatarURL);
            }
            else{
                completion(NO, [operation.responseObject valueForKey:kDictMessage], nil);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            completion(NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect, nil);
        }];
    }else{
        completion( NO, @"Please log-in before update your profile", nil);
    }
}

#pragma mark - Get a list job
-(void)getListJobs:(id)parameter completion:(void(^)(id result, BOOL success, NSString *message))completion{
    
    if (DEBUG) {
        NSLog(@"%@",[NSString stringWithFormat:@"%@%@", kServerAddress, kGetJobsAPI]);
    }
    if ([NSUserDefaults isHasToken]) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[NSUserDefaults token]] forHTTPHeaderField:kHeaderAuthorization];
        [manager GET:[NSString stringWithFormat:@"%@%@", kServerAddress, kGetJobsAPI]
          parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
              if (isResult) {
                  
                  NSArray* listJobs = [[responseObject valueForKey:kDictData] valueForKey:kDictListJobs];
                  if ([listJobs count]) {
                      NSLog(@"getListJobs: %i", [listJobs count]);
                      NSMutableArray *results = [NSMutableArray arrayWithCapacity:[listJobs count]];
                      for (NSInteger i = 0; i < [listJobs count]; i++) {
                          id cursorJob = [listJobs objectAtIndex:i];
                          
                          Job *aJob = [[Job alloc] init];
                          
                          aJob.job_id = [[cursorJob valueForKey:kDictJobID] integerValue];
                          aJob.is_applied = [[cursorJob valueForKey:kDictJobIsApplied] isEqual:@1]?YES:NO;
                          aJob.is_favorite = [[cursorJob valueForKey:kDictJobIsFavourite] isEqual:@1]?YES:NO;
                          aJob.company_logo = [NSURL URLWithString:[cursorJob valueForKey:kDictJobCompanyLogo]];
                          
                          id company_id = [cursorJob valueForKey:kDictJobCompanyId];
                          aJob.company_id = [company_id integerValue];
                          id companyName = [cursorJob valueForKey:kDictJobCompanyName];
                          aJob.company_name = [NSNull isNullClass:companyName]?nil:companyName;
                          
                          id companyAddress = [cursorJob valueForKey:kDictJobCompanyAddress];
                          aJob.company_address = [NSNull isNullClass:companyAddress]?nil:companyAddress;
                          aJob.experience = [[cursorJob valueForKey:kDictJobExperience] integerValue];
                          
                          aJob.exp_year_min = 0;
                          if(![[cursorJob valueForKey:kDictJobExperienceMin] isEqual:[NSNull null]]){
                              aJob.exp_year_min = [[cursorJob valueForKey:kDictJobExperienceMin] integerValue];
                          }
                          
                          aJob.exp_year_max = 0;
                          if(![[cursorJob valueForKey:kDictJobExperienceMax] isEqual:[NSNull null]]){
                              aJob.exp_year_max = [[cursorJob valueForKey:kDictJobExperienceMax] integerValue];
                          }
                          
                          id jobTitle = [cursorJob valueForKey:kDictJobTitle];
                          aJob.job_title =[NSNull isNullClass:jobTitle]?nil:jobTitle;
                          aJob.posted_date = [cursorJob valueForKey:kDictJobPostedDate];
                          
                          [results addObject:aJob];
                          
                          id jobOverview = [cursorJob valueForKey:kDictJobOverview];
                          aJob.job_overview = [NSNull isNullClass:jobOverview]?nil:jobOverview;
                          
                          id jobDescription = [cursorJob valueForKey:kDictJobDescription];
                          aJob.job_description = [NSNull isNullClass:jobDescription]?nil:jobDescription;
                          
                          aJob.location_id = [[cursorJob valueForKey:kDictJobLocationId] integerValue];
                      }
                      completion(results, YES, [responseObject valueForKey:kDictMessage]);
                  }
                  else{
                      completion(nil, YES, [responseObject valueForKey:kDictMessage]);
                  }
                  
              }else{
                  completion(nil, NO, [responseObject valueForKey:kDictMessage]);
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              completion(nil, NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
          }];
    }
}

-(void)favouriteJob:(id)parameter completion:(void(^)(id result, BOOL success, NSString *message))completion{
    if (DEBUG) {
        NSLog(@"%@",[NSString stringWithFormat:@"%@%@", kServerAddress, kSubmitJobAPI]);
    }
    if ([NSUserDefaults isHasToken]) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[NSUserDefaults token]] forHTTPHeaderField:kHeaderAuthorization];
        [manager POST:[NSString stringWithFormat:@"%@%@", kServerAddress, kSubmitJobAPI]
           parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSLog(@"Response: %@", responseObject);
               completion(nil, YES, [operation.responseObject valueForKey:kDictMessage]);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               completion(nil, NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
           }];
    }
}

-(void)submitApplicationJob:(id)parameter dataCV:(id)dataCV dataVideo:(id)dataVideo completion:(void (^)(id result, BOOL success, NSString *message))completion{
    if (DEBUG) {
        NSLog(@"%@",[NSString stringWithFormat:@"%@%@", kServerAddress, kSubmitJobAPI]);
    }
    if ([NSUserDefaults isHasToken]) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",[NSUserDefaults token]] forHTTPHeaderField:kHeaderAuthorization];
        
        
        [manager POST:[NSString stringWithFormat:@"%@%@", kServerAddress, kSubmitJobAPI] parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            if (dataCV) {
                
                [formData appendPartWithFileData:[dataCV objectForKey:kDictFileMyCV]
                                            name:kDictFileMyCV
                                        fileName:[[dataCV valueForKey:kDictFileName] lowercaseString]
                                        mimeType:[dataCV valueForKey:kDictFileMineType]];
            }
            
            if (dataVideo) {
                
                [formData appendPartWithFileData:[dataVideo objectForKey:kDictFilefileMyIntro]
                                            name:kDictFilefileMyIntro fileName:[[dataVideo valueForKey:kDictFileName] lowercaseString]
                                        mimeType:[dataVideo valueForKey:kDictFileMineType]];
            }
            
            
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            BOOL isResult = [[responseObject valueForKey:kDictResult] isEqual:@1]?YES:NO;
            
            if (isResult) {
                completion(nil, YES, [responseObject valueForKey:kDictMessage]);
            }
            else{
                completion(nil, NO, [responseObject valueForKey:kDictMessage]);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Failed!");
            completion(nil, NO, operation.responseObject ? [operation.responseObject valueForKey:kDictMessage]: kErrorMessageNetworkConnect);
        }];
        
    }
}

-(void)downloadFile:(NSString *)url completion:(void (^)(BOOL, NSURL *))completion{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (!error) {
            completion (YES, filePath);
        }else{
            completion(NO, filePath);
        }
    }];
    [downloadTask resume];
}
@end
