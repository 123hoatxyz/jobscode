//
//  Job.h
//  snappcv
//
//  Created by Staff on 4/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Job : NSObject
@property NSUInteger job_id;
@property BOOL is_applied;
@property BOOL is_favorite;
@property NSURL *company_logo;
@property NSString *company_address;
@property NSString *company_name;
@property NSUInteger experience;
@property NSUInteger exp_year_min;
@property NSUInteger exp_year_max;
@property NSString *job_title;
@property NSString *posted_date;
@property NSString *job_overview;
@property NSString *job_description;
@property NSInteger location_id;
@property NSInteger company_id;
@end
