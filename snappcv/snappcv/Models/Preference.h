//
//  Preference.h
//  snappcv
//
//  Created by Staff on 4/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Preference : NSObject<NSCoding>
@property (getter=isNotice) BOOL notice;
@property (getter=isSubscribe) BOOL subscribe;
@end
