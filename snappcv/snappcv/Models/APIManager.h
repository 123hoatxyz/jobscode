//
//  APIManager.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIManager : NSObject

+(instancetype)sharedManager;

-(void)registerUser:(NSDictionary *)parameters completion:(void (^)(BOOL success, NSString* message))completion;
//
//API for Logout: with a given Toket at Header of request
//
-(void)logoutUserWithCompletion:(void(^)(BOOL success, NSString *error))completion;
//
//API for Login: with a given email and password. Return: Will return the request is success or not, user info and a message if have
//
-(void)loginWithEmail:(NSString*)email password:(NSString*)password completion:(void(^)(NSDictionary *data, BOOL success, NSString *error))completion;
//
//API for Login with LinkedIn
//
-(void)loginWithLinkedIn:(NSString*)accessToken accessSecret:(NSString*)accessSecret completion:(void(^)(NSDictionary *data, BOOL success, NSString *error))completion;
//
//API for Reset password: with a given email.
//
-(void)resetPasswordWithAnEmail:(NSString*)email completion:(void(^)(BOOL success, NSString* message))completion;
//
//API for update candidate profile
//
-(void)updateCandidateProfile:(id)parameters completion:(void(^)(BOOL success, NSString *message, NSString *avatarURL))completion;
//
//API get list job, search or filter jobs. Parameter is null, then get all list job and otherwise
//
-(void)getListJobs:(id)parameter completion:(void(^)(id result, BOOL success, NSString *message))completion;
//
//Favourite API: with a given job id. This api can be favourite and unfavourite a job
//
-(void)favouriteJob:(id)parameter completion:(void(^)(id result, BOOL success, NSString *message))completion;
//
//Submit Application API: with a given job id
//
-(void)submitApplicationJob:(id)parameter dataCV:(id)dataCV dataVideo:(id)dataVideo completion:(void (^)(id result, BOOL success, NSString *message))completion;
//
//Change Password
//
-(void)changePassword:(NSString*)currentPassword newPassword:(NSString*)newPassword completion:(void(^)(BOOL success, NSString* message))completion;

//
//Download file
//
-(void)downloadFile:(NSString*)url completion:(void(^)(BOOL success, NSURL* url))completion;

@end
