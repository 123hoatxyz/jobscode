//
//  Preference.m
//  snappcv
//
//  Created by Staff on 4/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "Preference.h"
#import "Constants.h"

@implementation Preference
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.notice = [aDecoder decodeBoolForKey:kDictNotice];
    self.subscribe = [aDecoder decodeBoolForKey:kDictSubscribe];
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeBool:[self isNotice] forKey:kDictNotice];
    [aCoder encodeBool:[self isSubscribe] forKey:kDictSubscribe];
}
@end
