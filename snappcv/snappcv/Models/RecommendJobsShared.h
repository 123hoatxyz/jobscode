//
//  RecommendJobsShared.h
//  snappcv
//
//  Created by Hoat Ha Van on 7/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecommendJobsShared : NSObject
@property (strong, nonatomic) NSArray *arrRecommendJobs;

+(instancetype)defaultShared;

-(NSArray*)arrayDictionaryMatchedLocation;

-(NSArray*)arrayCompanyName;

-(NSArray*)arrayDicCompany;

@end
