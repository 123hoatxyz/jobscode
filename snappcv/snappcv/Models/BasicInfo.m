//
//  BasicInfo.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "BasicInfo.h"
#import "Constants.h"

@implementation BasicInfo

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.email = [aDecoder decodeObjectForKey:kDictEmail];
    self.password = [aDecoder decodeObjectForKey:kDictPassword];
    self.password_confirmation = [aDecoder decodeObjectForKey:kDictPasswordConfirmation];
    self.first_name = [aDecoder decodeObjectForKey:kDictFirstName];
    self.last_name = [aDecoder decodeObjectForKey:kDictLastName];
    self.token = [aDecoder decodeObjectForKey:kDictToken];
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.email forKey:kDictEmail];
    [aCoder encodeObject:self.password forKey:kDictPassword];
    [aCoder encodeObject:self.password_confirmation forKey:kDictPasswordConfirmation];
    [aCoder encodeObject:self.first_name forKey:kDictFirstName];
    [aCoder encodeObject:self.last_name forKey:kDictLastName];
    [aCoder encodeObject:self.token forKey:kDictToken];
}

-(NSString *)description{
    return [NSString stringWithFormat:@"%@", self];
}
@end
