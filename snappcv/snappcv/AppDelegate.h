//
//  AppDelegate.h
//  snappcv
//
//  Created by Vu Tiet on 2/10/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SnappCVWorkMode) {
   SnappCVWorkModeHiring,
   SnappCVWorkModeFinding
};

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) SnappCVWorkMode workMode;
@property (assign, nonatomic) BOOL isChangedCenter;
@property (assign) BOOL isLogin;
@property (assign) BOOL isOpenDropbox;
//methods
- (void)loadStoryboard:(NSString *)storyboardName;
@end

