//
//  main.m
//  snappcv
//
//  Created by Vu Tiet on 2/10/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SnappCVApplication.h"
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([SnappCVApplication class]), NSStringFromClass([AppDelegate class]));
    }
}
