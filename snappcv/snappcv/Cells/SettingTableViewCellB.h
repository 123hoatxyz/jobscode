//
//  SettingTableViewCellB.h
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewCellB : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *ibToggleButton;
@property (weak, nonatomic) IBOutlet UILabel *ibNotificationLabel;
@end

