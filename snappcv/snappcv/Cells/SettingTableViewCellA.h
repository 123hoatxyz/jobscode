//
//  SettingTableViewCellA.h
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewCellA : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;

@end
