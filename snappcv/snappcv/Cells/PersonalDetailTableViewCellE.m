//
//  PersonalDetailTableViewCellE.m
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "PersonalDetailTableViewCellE.h"
#import "Constants.h"

@implementation PersonalDetailTableViewCellE

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)pressOnButton:(id)sender {
    if ([_delegate respondsToSelector:@selector(didTapOnButtonAtPersonalDetailTableViewCell:)]) {
        [_delegate didTapOnButtonAtPersonalDetailTableViewCell:self];
    }
}

-(void)setCellInfo:(id)info{
    [_ibTitleButton setTitle:[info valueForKey:kDictTitle] forState:UIControlStateNormal];
}
@end
