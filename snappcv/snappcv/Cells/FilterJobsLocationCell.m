//
//  FilterJobsLocationCell.m
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "FilterJobsLocationCell.h"
#import "Constants.h"

@implementation FilterJobsLocationCell

- (void)awakeFromNib {
    // Initialization code
    _didTick = NO;
    _ibTitleLabel.textColor = kColorTitleLocationUnSelected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellInfo:(id)info{
    _ibTitleLabel.text = [info valueForKey:kDictTitle];
    BOOL checked = [[info valueForKey:kDictChecked] isEqual:@1]? YES : NO;
    [self setHiddenCheckedButton:checked];
}
-(void)setHiddenCheckedButton:(BOOL)checked{
    _ibTitleLabel.textColor = checked? kColorTitleLocationSelected:kColorTitleLocationUnSelected;
    _ibCheckedButton.hidden = checked? NO : YES;
}

@end
