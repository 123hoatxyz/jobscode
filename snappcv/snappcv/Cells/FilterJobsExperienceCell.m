//
//  FilterJobsExperienceCell.m
//  snappcv
//
//  Created by Hoat Ha Van on 7/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "FilterJobsExperienceCell.h"
#import "UIToolbar+Utils.h"
#import "Constants.h"

@implementation FilterJobsExperienceCell

- (void)awakeFromNib {
    // Initialization code
    _ibExperienceField.delegate = self;
    if ([_ibExperienceField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _ibExperienceField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleEnterYourExperience attributes:@{NSForegroundColorAttributeName:kColorPlaceHolder}];
    }
    [_ibExperienceField addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventEditingChanged];
    _ibExperienceField.inputAccessoryView = [UIToolbar keyboardToolbar:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellInfo:(id)info{
    _ibExperienceField.text = [info valueForKey:kDictExperienceYear];
}
#pragma mark - UITextField's Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_ibExperienceField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 2;
}

-(IBAction)didChangeValue:(id)sender{
    if ([_delegate respondsToSelector:@selector(didChangeTextAtCell:valueChanged:)]) {
        [_delegate didChangeTextAtCell:self valueChanged:_ibExperienceField.text];
    }
}

-(void)endEditing{
    [_ibExperienceField resignFirstResponder];
}

@end
