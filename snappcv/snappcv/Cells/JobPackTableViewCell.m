//
//  JobPackTableViewCell.m
//  snappcv
//
//  Created by Staff on 3/12/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "JobPackTableViewCell.h"
#import "Constants.h"

@implementation JobPackTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellInfo:(id)info{
    NSString *jobNumber1 = [info valueForKey:kDictJobsNumber1];
    NSString *jobNumber2 = [info valueForKey:kDictJobsNumber2];
    NSString *money1 = [info valueForKey:kDictMoney1];
    NSString *money2 = [info valueForKey:kDictMoney2];
    
    _ibJobsNumber1Label.font = kFontDefaultForJobsPack1;
    _ibJobsNumber2Label.font = kFontDefaultForJobsPack1;
    _ibMoney1Label.font = kFontDefaultForJobsPack2;
    _ibMoney2Label.font = kFontDefaultForJobsPack2;
    [_ibJobsNumber1Label setText:jobNumber1 afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
     {
         NSRange termsRange = [[mutableAttributedString string] rangeOfString:kDictJobs options:NSCaseInsensitiveSearch];
         
         UIFont *regularSystemFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:13.0];
         CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)regularSystemFont.fontName, regularSystemFont.pointSize, NULL);
         if (font) {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorJobsPack.CGColor range:termsRange];
             CFRelease(font);
         }
         
         return mutableAttributedString;
     }];
    
    [_ibJobsNumber2Label setText:jobNumber2 afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
     {
         NSRange termsRange = [[mutableAttributedString string] rangeOfString:kDictJobs options:NSCaseInsensitiveSearch];
         
         UIFont *regularSystemFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:13.0];
         CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)regularSystemFont.fontName, regularSystemFont.pointSize, NULL);
         if (font) {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorJobsPack.CGColor range:termsRange];
             CFRelease(font);
         }
         
         return mutableAttributedString;
     }];
    
    
    [_ibMoney1Label setText:money1 afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
     {
         NSRange termsRange = [[mutableAttributedString string] rangeOfString:kDictUSD options:NSCaseInsensitiveSearch];
         
         UIFont *regularSystemFont = [UIFont fontWithName:@"HelveticaNueue" size:13.0];
         CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)regularSystemFont.fontName, regularSystemFont.pointSize, NULL);
         if (font) {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)[UIColor whiteColor].CGColor range:termsRange];
             CFRelease(font);
         }
         
         return mutableAttributedString;
     }];
    
    [_ibMoney2Label setText:money2 afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
     {
         NSRange termsRange = [[mutableAttributedString string] rangeOfString:kDictUSD options:NSCaseInsensitiveSearch];
         
         UIFont *regularSystemFont = [UIFont fontWithName:@"HelveticaNueue" size:13.0];
         CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)regularSystemFont.fontName, regularSystemFont.pointSize, NULL);
         if (font) {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)[UIColor whiteColor].CGColor range:termsRange];
             CFRelease(font);
         }
         
         return mutableAttributedString;
     }];
}

@end
