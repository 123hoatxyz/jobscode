//
//  PersonalDetailTableViewCell.h
//  snappcv
//
//  Created by Staff on 2/27/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PersonalDetailTableViewCell;
@protocol PersonalDetailTableViewCellDelegate <NSObject>

@optional
-(void)willBeginEdit:(PersonalDetailTableViewCell*)cell;
-(void)willEndEdit:(PersonalDetailTableViewCell*)cell;
-(void)didChangeValueInPersonalDetailTableViewCell:(PersonalDetailTableViewCell*)cell value:(NSString*)value;
@end

@interface PersonalDetailTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *ibTitleField;
@property (weak, nonatomic) id <PersonalDetailTableViewCellDelegate>delegate;
-(void)setCellInfo:(id)info;
@end
