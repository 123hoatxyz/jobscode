//
//  PersonalDetailTableViewCellE.h
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PersonalDetailTableViewCellE;
@protocol PersonalDetailTableViewCellEDelegate <NSObject>

@optional
-(void)didTapOnButtonAtPersonalDetailTableViewCell:(PersonalDetailTableViewCellE*)cell;

@end
@interface PersonalDetailTableViewCellE : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *ibTitleButton;
@property (weak, nonatomic) id<PersonalDetailTableViewCellEDelegate>delegate;

-(void)setCellInfo:(id)info;
@end
