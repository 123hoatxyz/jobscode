//
//  PersonalDetailTableViewCellB.h
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PersonalDetailTableViewCellB;
@protocol PersonalDetailTableViewCellBDelegate<NSObject>
@optional
-(void)showPickerViewFromPersonalDetailCell:(PersonalDetailTableViewCellB*)cell withType:(NSString*)type;
@end

@interface PersonalDetailTableViewCellB : UITableViewCell


//@property (weak, nonatomic) IBOutlet UITextField *ibTitleField;
@property (weak, nonatomic) IBOutlet UIButton *ibGenderButton;
@property (weak, nonatomic) id<PersonalDetailTableViewCellBDelegate>delegate;
@property (strong, nonatomic) NSString *type;

-(void)setCellInfo:(id)info;
@end
