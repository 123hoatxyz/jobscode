//
//  PersonalDetailTableViewCellB.m
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "PersonalDetailTableViewCellB.h"
#import "Constants.h"

@implementation PersonalDetailTableViewCellB

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellInfo:(id)info{
    [_ibGenderButton setTitle:[info valueForKey:kDictTitle] forState:UIControlStateNormal];
}
- (IBAction)callSenderView:(id)sender {
    if ([_delegate respondsToSelector:@selector(showPickerViewFromPersonalDetailCell:withType:)]) {
        [_delegate showPickerViewFromPersonalDetailCell:self withType:_type];
    }
}

@end
