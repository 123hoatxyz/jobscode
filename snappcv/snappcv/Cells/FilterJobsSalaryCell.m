//
//  FilterJobsSalaryCell.m
//  snappcv
//
//  Created by Hoat Ha Van on 7/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "FilterJobsSalaryCell.h"
#import "Constants.h"
#import "NSString+Utils.h"
#import "NSMutableDictionary+FilterUtils.h"

@implementation FilterJobsSalaryCell

- (void)awakeFromNib {
   
    // Initialization code
    [_ibMinSalarySlider setThumbImage:[UIImage imageNamed:@"slider_thumb"] forState:UIControlStateNormal];
    [_ibMinSalarySlider setMinimumTrackImage:[[UIImage imageNamed:@"slider_min"] stretchableImageWithLeftCapWidth:4 topCapHeight:0] forState:UIControlStateNormal];
    [_ibMinSalarySlider setMaximumTrackImage:[UIImage imageNamed:@"slider_default"] forState:UIControlStateNormal];
    [_ibMaxSalarySlider setThumbImage:[UIImage imageNamed:@"slider_thumb"] forState:UIControlStateNormal];
    [_ibMaxSalarySlider setMinimumTrackImage:[[UIImage imageNamed:@"slider_max"] stretchableImageWithLeftCapWidth:4 topCapHeight:0] forState:UIControlStateNormal];
    [_ibMaxSalarySlider setMaximumTrackImage:[UIImage imageNamed:@"slider_default"] forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellInfo:(id)info{
    NSArray *arrSubTitles = [info valueForKey:kDictSubTitles];
    NSMutableDictionary *dictSubTitles = [arrSubTitles objectAtIndex:0];
    
    NSNumber *minValue = [dictSubTitles getMinSalary];
    _ibMinValueLabe.text = [NSString stringWithFormat:@"%@",minValue];
    NSNumber *maxValue = [dictSubTitles getMaxSalary];
    _ibMaxValueLabel.text = [NSString stringWithFormat:@"%.1f",[maxValue floatValue]/10.0];
    
    _ibMinSalarySlider.value = [minValue floatValue];
    
    _ibMaxSalarySlider.value = [maxValue floatValue];
}

- (IBAction)minChanged:(id)sender {
    NSString *value = [NSString stringWithFormat:@"%ld", (long)_ibMinSalarySlider.value];
    _ibMinValueLabe.text = value;
    
    long number = (long)[_ibMinSalarySlider value];
    if ([_delegate respondsToSelector:@selector(didChangeMinSalaryFromCell:minValue:)]) {
        [_delegate didChangeMinSalaryFromCell:self minValue:[NSNumber numberWithLong: number*1000]];
    }
}

- (IBAction)maxChanged:(id)sender {
    [_ibMaxSalarySlider setValue:((int)((_ibMaxSalarySlider.value + 2.5) / 5) * 5) animated:NO];
    NSString *value = [NSString stringWithFormat:@"%.1f", _ibMaxSalarySlider.value/10.0];
    _ibMaxValueLabel.text = value;
    
    float number = [_ibMaxSalarySlider value];
    if ([_delegate respondsToSelector:@selector(didChangeMaxSalaryFromCell:maxValue:)]) {
        [_delegate didChangeMaxSalaryFromCell:self maxValue:[NSNumber numberWithLong: number*100000]];
    }
}


@end
