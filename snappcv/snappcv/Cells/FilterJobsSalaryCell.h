//
//  FilterJobsSalaryCell.h
//  snappcv
//
//  Created by Hoat Ha Van on 7/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FilterJobsSalaryCell;
@protocol FilterJobsSalaryCellDelegate <NSObject>

@optional
-(void)didChangeMaxSalaryFromCell:(FilterJobsSalaryCell*)cell maxValue:(NSNumber*)maxValue;
-(void)didChangeMinSalaryFromCell:(FilterJobsSalaryCell*)cell minValue:(NSNumber*)minValue;

@end
@interface FilterJobsSalaryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISlider *ibMinSalarySlider;
@property (weak, nonatomic) IBOutlet UISlider *ibMaxSalarySlider;
@property (weak, nonatomic) IBOutlet UILabel *ibMinValueLabe;
@property (weak, nonatomic) IBOutlet UILabel *ibMaxValueLabel;
@property (weak, nonatomic) id<FilterJobsSalaryCellDelegate>delegate;
-(void)setCellInfo:(id)info;

@end
