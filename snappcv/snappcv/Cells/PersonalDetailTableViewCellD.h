//
//  PersonalDetailTableViewCellD.h
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalDetailTableViewCellD : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *ibTitleButton;
@property (weak, nonatomic) IBOutlet UIButton *ibTickButton;
-(void)setCellInfo:(id)info;

@end
