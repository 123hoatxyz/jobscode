//
//  PersonalDetailTableViewCell.m
//  snappcv
//
//  Created by Staff on 2/27/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "PersonalDetailTableViewCell.h"
#import "Constants.h"
#import "UIToolbar+Utils.h"

@implementation PersonalDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _ibTitleField.delegate = self;
    // Configure the view for the selected state
    [_ibTitleField addTarget:self action:@selector(valueEditChanged) forControlEvents:UIControlEventEditingChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];    
}
-(void)setCellInfo:(id)info{
   
    if ([_ibTitleField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        
        if ([info valueForKey:kDictTitle]) {
            _ibTitleField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[info valueForKey:kDictTitle] attributes:@{NSForegroundColorAttributeName: kColorPlaceHolder}];
        }else if([info valueForKey:kDictTitle1]){
            _ibTitleField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[info valueForKey:kDictTitle1] attributes:@{NSForegroundColorAttributeName: kColorPlaceHolder}];
        }
    }
    
    NSString *type = [info valueForKey:@"type"];
    if ([type isEqualToString:@"number"]) {
        _ibTitleField.inputAccessoryView = [UIToolbar keyboardToolbar:self];
       [_ibTitleField setKeyboardType:UIKeyboardTypeNumberPad];
        [_ibTitleField setTag:4];
    }else{
         [_ibTitleField setKeyboardType:UIKeyboardTypeDefault];
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([_delegate respondsToSelector:@selector(willBeginEdit:)]) {
        [_delegate willBeginEdit:self];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_ibTitleField resignFirstResponder];
    if ([_delegate respondsToSelector:@selector(willEndEdit:)]) {
        [_delegate willEndEdit:self];
    }
    return YES;
}
-(BOOL)endEditing:(BOOL)force{
    [_ibTitleField resignFirstResponder];
    if ([_delegate respondsToSelector:@selector(willEndEdit:)]) {
        [_delegate willEndEdit:self];
    }
    return YES;
}

-(void)valueEditChanged{
    if ([_delegate respondsToSelector:@selector(didChangeValueInPersonalDetailTableViewCell:value:)]) {
        [_delegate didChangeValueInPersonalDetailTableViewCell:self value:_ibTitleField.text];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    int allowedLength;
    switch(textField.tag) {
        case 4:
            allowedLength = 12;   // triggered for input fields with tag = 1
            break;
        default:
            allowedLength = 255;   // length default when no tag (=0) value =255
            break;
    }
    
    if (textField.text.length >= allowedLength && range.length == 0) {
        return NO; // Change not allowed
    } else {
        return YES; // Change allowed
    }
}


@end
