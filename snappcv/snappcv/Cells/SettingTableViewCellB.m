//
//  SettingTableViewCellB.m
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "SettingTableViewCellB.h"
#import "NSUserDefaults+Utils.h"

@implementation SettingTableViewCellB

- (void)awakeFromNib {
    // Initialization code
    [_ibToggleButton setImage:[UIImage imageNamed:@"notification_on"] forState:UIControlStateSelected];
    
    id value = [NSUserDefaults getNotificationState];
    
    if (value == nil)
    {
        _ibToggleButton.selected = NO;
    }
    else
    {
        NSNumber *val = value;
        BOOL notificationState = [val boolValue];
        _ibToggleButton.selected = (BOOL)notificationState;
    }
    
    if (_ibToggleButton.selected)
    {
        _ibNotificationLabel.text = @"ON";
    }
    else
    {
        _ibNotificationLabel.text = @"OFF";
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)toggle:(id)sender
{
    UIButton *button = (UIButton*)sender;
    button.selected  = !button.selected;
    
    [NSUserDefaults updateNotificationState:button.selected];
    
    if (button.selected)
    {
        _ibNotificationLabel.text = @"ON";
    }
    else
    {
         _ibNotificationLabel.text = @"OFF";
    }
}

@end
