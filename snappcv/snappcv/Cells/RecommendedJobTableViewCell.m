//
//  RecommendedJobTableViewCell.m
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RecommendedJobTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "TTTAttributedLabel.h"
#import "Constants.h"
#import "APIManager.h"
#import "NSDate+Utils.h"
#import "Job.h"
#import "NSString+Utils.h"

@implementation RecommendedJobTableViewCell
@synthesize job;
- (void)awakeFromNib {
    // Initialization code
    [_ibRatingButton setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
    [_ibRatingButton setImage:[UIImage imageNamed:@"star_fav"] forState:UIControlStateSelected];
    _didApplyJob = NO;
    [_ibJobImageView.layer setBorderColor: [kColorBorderImageView CGColor]];
    [_ibJobImageView.layer setBorderWidth: 1.5];
    _ibPostTimeLabel.text = @"";
}
-(void)setCellInfo:(Job*)info{
    job = info;
    _ibApplyButton.enabled = info.is_applied?NO:YES;
    _ibApplyButton.backgroundColor = info.is_applied?kColorRecommJobDidApply : kColorRecommJobNotApply;
    [_ibApplyButton setTitle:info.is_applied?@"Applied":@"Apply" forState:UIControlStateNormal];
    _ibRatingButton.selected = info.is_favorite;
    _ibJobTitleLabel.text = info.job_title;
    _ibCompanyNameLabel.text = info.company_name;
    _ibCompanyLocationLabel.text = info.company_address;
    [_ibJobImageView setImageWithURL:info.company_logo placeholderImage:nil];
    
    [self.ibExperienceYearLabel setText:[NSString stringWithFormat:@"%ld - %ld Years of Exp.", (long)info.exp_year_min, (long)info.exp_year_max]
    afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
        
         NSRange termsRange = [[mutableAttributedString string] rangeOfString:kTitleExperiencePart options:NSCaseInsensitiveSearch];
         
         UIFont *regularSystemFont = [UIFont fontWithName:@"Helvetica-Nueue" size:self.ibExperienceYearLabel.font.pointSize];
         CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)regularSystemFont.fontName, regularSystemFont.pointSize, NULL);
         if (font) {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorExperiencePart.CGColor range:termsRange];
             CFRelease(font);
         }
         return mutableAttributedString;
     }];
    
//    _ibApplyButton.selected = info.is_applied;
     _ibPostTimeLabel.text = [NSDate titleDateFromNumber:info.posted_date];
   
}

-(void)setCellFilterInfo:(id)info highlightedText:(NSString*)highlightedText{
    [self setCellInfo:info];
    if (![NSString isEmptyString:highlightedText]) {
        [self highlightText:highlightedText];
    }
}

-(void) highlightText:(NSString *)value {
    @try {
        NSInteger lengthValue = value.length;
        NSInteger idx = 0;
        while (idx < (_ibJobTitleLabel.text.length-lengthValue)) {
            NSRange stringRange = NSMakeRange(idx, lengthValue);
            
            NSString *nextString = [[_ibJobTitleLabel.text substringWithRange:stringRange] lowercaseString];
            if ([nextString isEqualToString:value]) {
                NSMutableAttributedString *tempString = [[NSMutableAttributedString alloc] initWithAttributedString:_ibJobTitleLabel.attributedText];
                [tempString addAttribute:NSBackgroundColorAttributeName value:[UIColor yellowColor] range:stringRange];
                _ibJobTitleLabel.attributedText = tempString;
                idx += lengthValue;
            } else {
                idx++;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)pressFavourite:(id)sender {
    
    BOOL isFavourite = !_ibRatingButton.selected;
    id params = @{kDictJobID :[NSNumber numberWithInteger:job.job_id], kDictJobIsFavourite:@(isFavourite)};
    [[APIManager sharedManager] favouriteJob:params completion:^(id result, BOOL success, NSString *message) {
        if (success) {
           
            dispatch_async(dispatch_get_main_queue(), ^{
                 _ibRatingButton.selected = !_ibRatingButton.selected;
                job.is_favorite = _ibRatingButton.selected;
            });
            
        }
    }];
    
}

- (IBAction)pressApplyJob:(id)sender {
//    _didApplyJob = !_didApplyJob;
//    _ibApplyButton.backgroundColor = _didApplyJob?kColorRecommJobDidApply:kColorRecommJobNotApply;
    [_ibApplyButton setTitle:_didApplyJob?kTitleDidApply:kTitleApply forState:UIControlStateNormal];
    if ([_delegate respondsToSelector:@selector(didTouchApplyButton:)]) {
        [_delegate didTouchApplyButton:self];
    }
}
@end
