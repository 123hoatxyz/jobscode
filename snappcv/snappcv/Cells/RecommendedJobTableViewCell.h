//
//  RecommendedJobTableViewCell.h
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TTTAttributedLabel, RecommendedJobTableViewCell;
@class Job;
@protocol RecommendedJobTableViewCellDelegate <NSObject>

@optional
-(void)didTouchApplyButton:(RecommendedJobTableViewCell*)cell;

@end
@interface RecommendedJobTableViewCell : UITableViewCell
@property (strong, nonatomic) Job *job;
@property (weak, nonatomic) IBOutlet UIImageView *ibJobImageView;
@property (weak, nonatomic) IBOutlet UILabel *ibJobTitleLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibExperienceYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibPostTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibCompanyLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibCompanyNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *ibApplyButton;
@property (weak, nonatomic) IBOutlet UIButton *ibRatingButton;

@property (weak, nonatomic) IBOutlet UIView *ibBorderImageView;
@property (assign,nonatomic) BOOL didApplyJob;
@property (weak, nonatomic) id <RecommendedJobTableViewCellDelegate>delegate;

-(void)setCellInfo:(Job*)info;

-(void)setCellFilterInfo:(id)info highlightedText:(NSString*)highlightedText;

@end
