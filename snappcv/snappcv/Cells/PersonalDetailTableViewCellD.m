//
//  PersonalDetailTableViewCellD.m
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "PersonalDetailTableViewCellD.h"
#import "Constants.h"

@implementation PersonalDetailTableViewCellD

- (void)awakeFromNib {
    // Initialization code
    [_ibTickButton setImage:[UIImage imageNamed:@"check_cir"] forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)checkCVUpload:(id)sender {
    _ibTickButton.selected = !_ibTickButton.selected;
}

-(void)setCellInfo:(id)info{
    [_ibTitleButton setTitle:[info valueForKey:kDictTitle1] forState:UIControlStateNormal];
}
@end
