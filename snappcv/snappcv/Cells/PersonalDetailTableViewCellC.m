//
//  PersonalDetailTableViewCellC.m
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "PersonalDetailTableViewCellC.h"
#import "Constants.h"
#import "UIToolbar+Utils.h"

@implementation PersonalDetailTableViewCellC

- (void)awakeFromNib {
   
    _ibTitle1Field.delegate = self;
    _ibTitle2Field.delegate = self;
    [_ibTitle1Field addTarget:self action:@selector(valueEditChanged) forControlEvents:UIControlEventEditingChanged];
    [_ibTitle2Field addTarget:self action:@selector(valueEditChanged) forControlEvents:UIControlEventEditingChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setCellInfo:(id)info withKeyPad:(int)type{
    if(type == 1){
        _ibTitle1Field.keyboardType = UIKeyboardTypeNumberPad;
        _ibTitle2Field.keyboardType = UIKeyboardTypeNumberPad;
    }else{
        _ibTitle1Field.keyboardType = UIKeyboardTypeDefault;
        _ibTitle2Field.keyboardType = UIKeyboardTypeDefault;
    }
    
    _ibTitle1Field.inputAccessoryView = [UIToolbar keyboardToolbar:self];
    _ibTitle2Field.inputAccessoryView = [UIToolbar keyboardToolbar:self];
    
    if ([_ibTitle1Field respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _ibTitle1Field.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:[info valueForKey:kDictTitle1] attributes:@{NSForegroundColorAttributeName:kColorPlaceHolder}];
        _ibTitle2Field.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:[info valueForKey:kDictTitle2] attributes:@{NSForegroundColorAttributeName:kColorPlaceHolder}];
    }
}

#pragma mark UITextField's Delegate
-(void)valueEditChanged{
    if ([_delegate respondsToSelector:@selector(didChangeValueInPersonalDetailTableViewCellC:expInYears:expInMonths:)]) {
        [_delegate didChangeValueInPersonalDetailTableViewCellC:self expInYears:_ibTitle1Field.text expInMonths:_ibTitle2Field.text];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_ibTitle1Field resignFirstResponder];
    [_ibTitle2Field resignFirstResponder];
    return YES;
}

@end
