//
//  FilterJobsExperienceCell.h
//  snappcv
//
//  Created by Hoat Ha Van on 7/7/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FilterJobsExperienceCell;
@protocol FilterJobsExperienceCellDelegate <NSObject>

@optional
-(void)didChangeTextAtCell:(FilterJobsExperienceCell*)cell valueChanged:(NSString*)valueChanged;

@end
@interface FilterJobsExperienceCell : UITableViewCell <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *ibExperienceField;
@property (weak, nonatomic) id <FilterJobsExperienceCellDelegate> delegate;
-(void)setCellInfo:(id)info;
@end
