//
//  FilterJobsLocationCell.h
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterJobsLocationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *ibCheckedButton;
@property (assign, nonatomic) BOOL didTick;
-(void)setHiddenCheckedButton:(BOOL)hidden;
-(void)setCellInfo:(id)info;
@end
