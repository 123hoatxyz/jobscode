//
//  PersonalDetailTableViewCellC.h
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PersonalDetailTableViewCellC;

@protocol PersonalDetailTableViewCellCDelegate <NSObject>

@optional
-(void)didChangeValueInPersonalDetailTableViewCellC:(PersonalDetailTableViewCellC*)cell expInYears:(NSString*)expInYears expInMonths:(NSString*)expInMonths;
@end
@interface PersonalDetailTableViewCellC : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *ibTitle1Field;
@property (weak, nonatomic) IBOutlet UITextField *ibTitle2Field;
@property (weak,nonatomic) id <PersonalDetailTableViewCellCDelegate>delegate;
-(void)setCellInfo:(id)info withKeyPad:(int)type;
@end
