//
//  JobPackTableViewCell.h
//  snappcv
//
//  Created by Staff on 3/12/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface JobPackTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibJobsNumber1Label;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibJobsNumber2Label;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibMoney1Label;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibMoney2Label;
- (void)setCellInfo:(id)info;
@end
