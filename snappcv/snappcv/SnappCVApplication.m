//
//  SnappCVApplication.m
//  snappcv
//
//  Created by Staff on 4/15/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "SnappCVApplication.h"
#import "AppDelegate.h"
@interface SnappCVApplication()
@property (assign, nonatomic) BOOL enableMultiTouches;
@end

@implementation SnappCVApplication

-(void)enableMultiTouches:(BOOL)enabled
{
    self.enableMultiTouches = enabled;
}
@end
