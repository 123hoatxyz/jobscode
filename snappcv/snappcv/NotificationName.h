//
//  NotificationName.h
//  Agento Floortime
//
//  Created by Staff on 2/3/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#ifndef Agento_Floortime_NotificationName_h
#define Agento_Floortime_NotificationName_h

//Jobs Searcg Result view controller

static NSString *const kNotifyGotoJobsSearchResultScreen = @"kNotifyGotoJobsSearchResultScreen";
static NSString *const kNotifyShowingJobsSearchResultScreen = @"kNotifyShowingJobsSearchResultScreen";
static NSString *const kNotifykNotifyShowingRecommendJobsDefault = @"kNotifyShowingRecommendJobsDefault";
#endif
