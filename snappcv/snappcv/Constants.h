//
//  Constants.h
//  snappcv
//
//  Created by Staff on 2/27/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#ifndef snappcv_Constants_h
#define snappcv_Constants_h

//----------------------------------View Controller-----------------------------------------
#pragma mark - ViewController
//Register
static NSString *const kTitleCompanyName = @"Company Name";
static NSString *const kTitleContactPerson = @"Contact Person";
static NSString *const kTitleZipcode = @"Zipcode";
static NSString *const kTitleCompanyURL = @"Company URL";
static NSString *const kTitleEmailAddress = @"Email Address";
static NSString *const kTitleUsername = @"Username";
static NSString *const kTitlePassword = @"Password";
static NSString *const kTitleConfirmPassword = @"Confirm Password";
static NSString *const kTitleTermConditionFull = @"I accept our Terms & Conditions";
static NSString *const kTitleTerms = @"Terms";
static NSString *const kTitleConditions = @"Conditions";
static NSString *const kTitleExperience = @"Enter Your Experience";

//Recommended Jobs
static NSString *const kTitleApply = @"Apply";
static NSString *const kTitleDidApply = @"Applied";

static NSString *const kTitleExperiencePart = @"of Exp.";

//Filter, Search
static NSString *const kTitleSearchKeyword = @"Search Keyword";

static NSString *const kTitleEnterYourExperience = @"Enter your Experience";

//Personal Detail
static NSString *const kProfessionalDetailViewController = @"ProfessionalDetailViewController";
static NSString *const kPersonalDetailViewController = @"PersonalDetailViewController";

//RecommededJobsViewController
static NSString *const kRecommededJobsViewController = @"RecommededJobsViewController";

//JobDetailViewController
static NSString *const kJobDetailViewController = @"JobDetailViewController";

//ApplyJobViewController
static NSString *const kApplyJobViewController =@"ApplyJobViewController";

//SearchJobViewController
static NSString *const kSearchJobViewController = @"SearchJobViewController";

//JobsPackViewController
static NSString *const kJobsPackViewController = @"JobsPackViewController";

//CompanyInfoViewController
static NSString *const kCompanyInfoViewController = @"CompanyInfoViewController";
//ResetPasswordViewController
static NSString *const kResetPasswordViewController = @"ResetPasswordViewController";
//PreferencesViewController
static NSString *const kPreferencesViewController = @"PreferencesViewController";
//FavoritesJobsViewController
static NSString *const kFavoritesJobsViewController = @"FavoritesJobsViewController";
//ApplicationHistoryViewController
static NSString *const kApplicationHistoryViewController = @"ApplicationHistoryViewController";
//WebViewController
static NSString *const kWebViewController = @"WebViewController";
//SettingViewController
static NSString *const kSettingViewController = @"SettingViewController";
//EditProfileViewController
static NSString *const kEditProfileViewController = @"EditProfileViewController";
static NSString *const kChangePasswordViewController = @"ChangePasswordViewController";

//----------------Key to identify for NSDictionary-------------------------
static NSString *const kDictTitle = @"title";
static NSString *const kDictTitle1 = @"title1";
static NSString *const kDictTitle2 = @"title2";
static NSString *const kDictType = @"type";
static NSString *const kDictSubTitles = @"subTitles";
static NSString *const kDictChecked = @"checked";
static NSString *const kDictValueId = @"valueId";
static NSString *const kDictIdentifier = @"identifier";
static NSString *const kDictJobs = @"Jobs";
static NSString *const kDictUSD = @"$";
static NSString *const kDictJobsNumber1 = @"jobsNumber1";
static NSString *const kDictJobsNumber2 = @"jobsNumber2";
static NSString *const kDictMoney1 = @"money1";
static NSString *const kDictMoney2 = @"money2";
static NSString *const kStoryBoardMain = @"Main";
static NSString *const kStoryBoardRegister = @"Register";
static NSString *const kDictError = @"error";
static NSString *const kDictMinSalary = @"min_salary";
static NSString *const kDictMaxSalary = @"max_salary";
static NSString *const kDictTopEmployers = @"top_employers";
static NSString *const kDictJobByLocation = @"jobs_by_location";
static NSString *const kDictJobIndustryId = @"industry_id";
//Reset forget password
static NSString *const kDictMessage = @"message";
static NSString *const kDictSuccess = @"Success";
//Sign-up
static NSString *const kDictEmail = @"email";
static NSString *const kDictPassword = @"password";
static NSString *const kDictPasswordConfirmation = @"password_confirmation";
static NSString *const kDictName = @"name";
static NSString *const kDictFirstName = @"first_name";
static NSString *const kDictLastName = @"last_name";
static NSString *const kDictAvatarURL = @"avatar";

//personal-detail
static NSString *const kDictProfile = @"profile";
static NSString *const kDictPersonalDetail =@"personal_detail";
static NSString *const kDictNationality =@"nationality_id";
static NSString *const kDictNationalityName =@"nationality_name";
static NSString *const kDictIndustryId = @"industry_id";
static NSString *const kDictIndustryName = @"industry_name";
static NSString *const kDictCurrentLocation =@"current_location";
static NSString *const kDictPhoneNumber = @"phone_number";
static NSString *const kDictDateOfBirth = @"date_of_birth";
static NSString *const kDictGender = @"gender";
static NSString *const kDictAvatarContentBase64String = @"avatar_content";
static NSString *const kDictAvatarName = @"avatar_name";
//profession-detail
static NSString *const kDictProfessionalDetail = @"professional_detail";
static NSString *const kDictJobType = @"job_type";
static NSString *const kDictExperienceYear = @"experience_year";
static NSString *const kDictExperienceMonth = @"experience_month";
static NSString *const kDictCurrentIndustry = @"current_industry";
static NSString *const kDictFunction = @"function";
static NSString *const kDictFunctionId = @"function_id";
static NSString *const kDictFunctionName = @"function_name";
static NSString *const kDictKeySkills = @"key_skills";
static NSString *const kDictFileMyCV = @"cv";
static NSString *const kDictFileMineType = @"mine_type";
static NSString *const kDictFileName = @"file_name";
static NSString *const kDictFilefileMyIntro = @"introduction_video";
//preference
static NSString *const kDictPreference = @"preference";
static NSString *const kDictNotice = @"notice";
static NSString *const kDictSubscribe = @"subscribe";
static NSString *const kDictCVTitle = @"cv_title";
static NSString *const kDictNotification = @"notification";
//Authentication
static NSString *const kDictAuthenticationToken = @"authentication_token";
static NSString *const kDictCandidateId = @"candidate_id";
static NSString *const kDictClientID = @"client_id";
static NSString *const kDictID = @"id";
static NSString *const kDictRole = @"role";
static NSString *const kDictSnappRole = @"snapp_role";
static NSString *const kDictUpdatedAt = @"updated_at";
//Token -- JSON Parse
static NSString *const kDictToken = @"token";
static NSString *const kDictData = @"data";
static NSString *const kDictResult = @"result";
static NSString *const kHeaderAuthorization = @"Authorization";
//Jobs
static NSString *const kDictListJobs = @"list_jobs";
static NSString *const kDictJobID =  @"job_id";
static NSString *const kDictJobIsApplied = @"is_applied";
static NSString *const kDictJobIsFavourite  =@"is_favorite";
static NSString *const kDictJobCompanyLogo = @"company_logo";
static NSString *const kDictJobCompanyAddress = @"company_address";
static NSString *const kDictJobCompanyName = @"company_name";
static NSString *const kDictJobCompanyId = @"company_id";
static NSString *const kDictJobExperience = @"experience_year";
static NSString *const kDictJobExperienceMin = @"exp_year_min";
static NSString *const kDictJobExperienceMax = @"exp_year_max";
static NSString *const kDictJobTitle = @"job_title";
static NSString *const kDictJobPostedDate = @"posted_date";
static NSString *const kDictJobOverview = @"job_overview";
static NSString *const kDictJobDescription = @"job_description";
static NSString *const kDictJobLocationId = @"location_id";
static NSString *const kDictJobLocationName = @"location_name";
//static NSString *const kDictJobLocationId = @"location_id";
//Master List
static NSString *const kDictAllFunctions = @"all_function";
static NSString *const kDictAllNations= @"all_nationality";
static NSString *const kDictAllIndustry = @"all_industry";
static NSString *const kDictAllLocation = @"all_location";
static NSString *const kDictFunctionKey = @"function_key";
static NSString *const kDictIndustryKey = @"industry_key";
static NSString *const kDictLocationKey = @"location_key";
static NSString *const kDictNationKey = @"nation_key";


// LINKEDIN
static NSString *const LINKEDIN_CLIENT_ID =  @"75dnkr1bkulhc0";
//@"75zja1a7mt8zbc";
static NSString *const LINKEDIN_CLIENT_SECRET = @"mnt2gvpIHEmJJPIx";
//@"YOjrrdQvpOimqP1E";

static NSString *const kDictParams = @"params";

static NSString *const kDictRegisterInfo= @"registerInfo";

static NSString *const kDictDeviceToken = @"deviceToken";

//------------------------------COLOR----------------------------------------------
#pragma mark - Color

#define kColorTermsCondition            [UIColor colorWithRed:45.0/255.0 green:194.0/255.0 blue:229.0/255.0 alpha:1.0f]
#define kColorPlaceHolder               [UIColor colorWithRed:161.0/255.0 green:160.0/255.0 blue:164.0/255.0 alpha:1.0f]

#define kColorRecommJobDidApply         [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0f]
#define kColorRecommJobNotApply         [UIColor colorWithRed:47.0/255.0 green:210.0/255.0 blue:248.0/255.0 alpha:1.0f]
#define kColorApplyJobNotApply         [UIColor colorWithRed:246.0/255.0 green:116.0/255.0 blue:35.0/255.0 alpha:1.0f]
#define kColorExperienceFull            [UIColor colorWithRed:94.0/255.0 green:94.0/255.0 blue:94.0/255.0 alpha:1.0f]
#define kColorExperiencePart            [UIColor colorWithRed:109.0/255.0 green:109.0/255.0 blue:109.0/255.0 alpha:1.0f]
#define kColorLocation                  [UIColor colorWithRed:109.0/255.0 green:109.0/255.0 blue:109.0/255.0 alpha:1.0f]

#define kColorFilterRelevanceHighLightView_Highlighted  [UIColor colorWithRed:239.0/255.0 green:143.0/255.0 blue:5.0/255.0 alpha:1.0]
#define kColorFilterFreshnessHighLightView              [UIColor colorWithRed:68.0/255.0 green:78.0/255.0 blue:85.0/255.0 alpha:1.0]
#define kColorFilterRelevanceHighLightView              [UIColor colorWithRed:68.0/255.0 green:78.0/255.0 blue:85.0/255.0 alpha:1.0]
#define kColorFilterFreshnessHighLightView_Highlighted  [UIColor colorWithRed:239.0/255.0 green:143.0/255.0 blue:5.0/255.0 alpha:1.0]

#define kColorMenuSelected                              [UIColor colorWithRed:64.0/255.0 green:76.0/255.0 blue:83.0/255.0 alpha:1.0]

#define kColorTitleLocationSelected                     [UIColor colorWithRed:208.0/255.0 green:117.0/255.0 blue:48.0/255.0 alpha:1.0]
#define kColorTitleLocationUnSelected                   [UIColor colorWithRed:215.0/255.0 green:217.0/255.0 blue:218.0/255.0 alpha:1.0]

#define kColorBorderImageView [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1.0]

//Circel Image
#define kColorCircleImageView [UIColor colorWithRed:48.0/255.0 green:59.0/255.0 blue:66.0/255.0 alpha:1.0]

//Jobs Pack
#define kColorJobsPack        [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0f]

//Edge
#define kEdgeInsetsImageMenu        UIEdgeInsetsMake(0,30,0,0)
#define kEdgeInsetsTitleMenu        UIEdgeInsetsMake(0,50,0,0)

//-----------------------------------FONT-----------------------------------------------
#pragma mark - FONT

#define kFontDefaultForTitle        [UIFont fontWithName:@"HelveticaNeue-Thin" size:20.0]
#define kFontDefaultForJobsPack1     [UIFont fontWithName:@"HelveticaNeue-Thin" size:30.0]
#define kFontDefaultForJobsPack2     [UIFont fontWithName:@"HelveticaNeue-Thin" size:22.0]

//-------------------------VIEW---------------------------
#define kTextFieldLeftView [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)]

///////////////////////////////////////////////////////////////////////
////-----------------API - Server configuration
//////////////////////////////////////////////////////////////////////
static NSString *const kServerAddress = @"http://snappcv.rubyspace.net/api/v1";
static NSString *const kLoginAPI = @"/sessions.json";
static NSString *const kLoginLinkedInAPI = @"/linkedin_sessions.json";
static NSString *const kLogoutAPI = @"/sessions/logout.json";
static NSString *const kResetPasswordAPI = @"/password/reset.json";
static NSString *const kChangePasswordAPI = @"/password/change.json";
static NSString *const kRegisterAPI = @"/candidates.json";
static NSString *const kUpdateCandatidateProfileAPI = @"/candidates/update.json";
static NSString *const kGetJobsAPI = @"/jobs.json";
static NSString *const kSubmitJobAPI = @"/applications/submit.json";

static NSString *const kURLHelpPage = @"http://snappcv.rubyspace.net/statics/help";
static NSString *const kURLRatePage = @"http://snappcv.rubyspace.net/statics/rate";
static NSString *const kURLAboutPage = @"http://snappcv.rubyspace.net/statics/about";
static NSString *const kURLCVPage = @"http://snappcv.rubyspace.net/statics/cv";
#endif
