//
//  GenderPickerView.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//
@import UIKit;
@class LocationPickerView;
@protocol LocationPickerViewDelegate <NSObject>

@optional
-(void)willDismissLocationPickerView:(LocationPickerView*)view;
-(void)didSelectComponentInLocationPickerView:(LocationPickerView*)view value:(NSDictionary*)value;
@end

@interface LocationPickerView : UIView<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *ibLocationPickerView;
@property (weak, nonatomic) id<LocationPickerViewDelegate>delegate;
@property (strong, nonatomic) NSString *locationIdSelected;
@property (strong, nonatomic) NSMutableArray *datas;
@end
