//
//  NationPickerView.m
//  snappcv
//
//  Created by Staff on 7/6/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "NationPickerView.h"
#import "NSUserDefaults+Utils.h"
#import "Constants.h"

@implementation NationPickerView

-(void)awakeFromNib{
    _datas = [self convertDictToArray:[NSUserDefaults getNationality:kDictNationKey]];
    
    _ibNationPickerView.dataSource = self;
    _ibNationPickerView.delegate = self;
    [UIView animateWithDuration:0.0 animations:^{
        [_ibNationPickerView reloadAllComponents];
    }completion:^(BOOL finished) {
        if (_nationIdSelected != nil)
        {
            NSInteger index = [self getSelectedIndex:_nationIdSelected];
            [_ibNationPickerView selectRow:index inComponent:0 animated:NO];
        }
    }];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSDictionary *nation = [_datas objectAtIndex:row];
    return [nation objectForKey:@"nationality_name"];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_datas count];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if ([_delegate respondsToSelector:@selector(didSelectComponentInNationPickerView:value:)]) {
        [_delegate didSelectComponentInNationPickerView:self value:_datas[row]];
    }
}
- (IBAction)dismissView:(id)sender {
    if ( [_delegate respondsToSelector:@selector(willDismissNationPickerView:)]) {
        [_delegate willDismissNationPickerView:self];
    }
}

- (NSInteger)getSelectedIndex:(NSString*)selectedId{
    NSInteger index = 0;
    for (int i = 0; i < [_datas count]; i++)
    {
        NSDictionary *dict = [_datas objectAtIndex:i];
        if ([[dict objectForKey:@"nationality_id"] isEqualToString:selectedId])
        {
            index = i;
            break;
        }
    }
    return index;
}

-(NSMutableArray*)convertDictToArray:(NSDictionary*)dictArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in dictArray)
    {
        [array addObject:dict];
    }
    
    return array;
}
@end
