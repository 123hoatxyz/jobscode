//
//  TermDialogView.h
//  snappcv
//
//  Created by TuongNguyen on 7/16/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@class TermDialogView;
@protocol TermConditionViewDelegate <NSObject>

@optional
- (void)willDismissTermConditionView:(TermDialogView*)termConditionView;
- (void)willCheckTermConditionView:(TermDialogView*)termConditionView;
@end

@interface TermDialogView : UIView

@property (weak, nonatomic) IBOutlet UIView *ibContentView;
@property (weak, nonatomic) IBOutlet UIButton *ibOkButton;
@property (weak, nonatomic) IBOutlet UIButton *ibAcceptTermButton;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibTermConditionLabel;

@property (weak, nonatomic) id<TermConditionViewDelegate>delegate;

-(void)setCheckTerm:(BOOL)checked;

@end
