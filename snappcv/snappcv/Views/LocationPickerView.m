//
//  GenderPickerView.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "LocationPickerView.h"
#import "NSUserDefaults+Utils.h"
#import "Constants.h"

@implementation LocationPickerView

-(void)awakeFromNib{
    _datas = [self convertDictToArray:[NSUserDefaults getFunctionList:kDictLocationKey]];
    
    _ibLocationPickerView.dataSource = self;
    _ibLocationPickerView.delegate = self;
   [UIView animateWithDuration:0.0 animations:^{
       [_ibLocationPickerView reloadAllComponents];
   }completion:^(BOOL finished) {
       if (_locationIdSelected != nil)
       {
           NSInteger index = [self getSelectedIndex:_locationIdSelected];
           [_ibLocationPickerView selectRow:index inComponent:0 animated:NO];
       }
   }];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSDictionary *industry = [_datas objectAtIndex:row];
    return [industry objectForKey:@"location_name"];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_datas count];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if ([_delegate respondsToSelector:@selector(didSelectComponentInLocationPickerView:value:)]) {
        [_delegate didSelectComponentInLocationPickerView:self value:_datas[row]];
    }
}
- (IBAction)dismissView:(id)sender {
    if ( [_delegate respondsToSelector:@selector(willDismissLocationPickerView:)]) {
        [_delegate willDismissLocationPickerView:self];
    }
}

- (NSInteger)getSelectedIndex:(NSString*)selectedId{
    NSInteger index = 0;
    for (int i = 0; i <= [_datas count]; i++)
    {
        NSDictionary *dict = [_datas objectAtIndex:i];
        if ([[dict objectForKey:@"location_id"] integerValue] == [selectedId integerValue])
        {
            index = i;
            break;
        }
    }
    return index;
}

-(NSMutableArray*)convertDictToArray:(NSDictionary*)dictArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in dictArray)
    {
        [array addObject:dict];
    }
    
    return array;
}
@end
