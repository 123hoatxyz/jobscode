//
//  FunctionPickerView.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//
@import UIKit;
@class FunctionPickerView;
@protocol FunctionPickerViewDelegate <NSObject>

@optional
-(void)willDismissFunctionPickerView:(FunctionPickerView*)view;
-(void)didSelectComponentInFunctionPickerView:(FunctionPickerView*)view value:(NSDictionary*)value;
@end

@interface FunctionPickerView : UIView<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *ibFunctionPickerView;
@property (weak, nonatomic) id<FunctionPickerViewDelegate>delegate;
@property (strong, nonatomic) NSString *functionIdSelected;
@property (strong, nonatomic) NSMutableArray *datas;
@end
