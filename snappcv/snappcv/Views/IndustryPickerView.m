//
//  GenderPickerView.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "IndustryPickerView.h"
#import "NSUserDefaults+Utils.h"
#import "Constants.h"

@implementation IndustryPickerView

-(void)awakeFromNib{
    _datas = [self convertDictToArray:[NSUserDefaults getFunctionList:kDictIndustryKey]];
    
    _ibIndustryPickerView.dataSource = self;
    _ibIndustryPickerView.delegate = self;
   [UIView animateWithDuration:0.0 animations:^{
       [_ibIndustryPickerView reloadAllComponents];
   }completion:^(BOOL finished) {
       if (_industryIdSelected != nil)
       {
           NSInteger index = [self getSelectedIndex:_industryIdSelected];
           [_ibIndustryPickerView selectRow:index inComponent:0 animated:NO];
       }
   }];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSDictionary *industry = [_datas objectAtIndex:row];
    return [industry objectForKey:@"industry_name"];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_datas count];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if ([_delegate respondsToSelector:@selector(didSelectComponentInIndustryPickerView:value:)]) {
        [_delegate didSelectComponentInIndustryPickerView:self value:_datas[row]];
    }
}
- (IBAction)dismissView:(id)sender {
    if ( [_delegate respondsToSelector:@selector(willDismissIndustryPickerView:)]) {
        [_delegate willDismissIndustryPickerView:self];
    }
}

- (NSInteger)getSelectedIndex:(NSString*)selectedId{
    NSInteger index = 0;
    for (int i = 0; i < [_datas count]; i++)
    {
        NSDictionary *dict = [_datas objectAtIndex:i];
        if ([[dict objectForKey:@"industry_id"] integerValue] == [selectedId integerValue])
        {
            index = i;
            break;
        }
    }
    return index;
}

-(NSMutableArray*)convertDictToArray:(NSDictionary*)dictArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in dictArray)
    {
        [array addObject:dict];
    }
    
    return array;
}
@end
