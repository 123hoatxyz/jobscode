//
//  GenderPickerView.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//
@import UIKit;
@class IndustryPickerView;
@protocol IndustryPickerViewDelegate <NSObject>

@optional
-(void)willDismissIndustryPickerView:(IndustryPickerView*)view;
-(void)didSelectComponentInIndustryPickerView:(IndustryPickerView*)view value:(NSDictionary*)value;
@end

@interface IndustryPickerView : UIView<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *ibIndustryPickerView;
@property (weak, nonatomic) id<IndustryPickerViewDelegate>delegate;
@property (strong, nonatomic) NSString *industryIdSelected;
@property (strong, nonatomic) NSMutableArray *datas;
@end
