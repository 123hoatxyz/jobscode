//
//  HeaderInfo.h
//  Agento
//
//  Created by Staff on 2/10/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//


#import <Foundation/Foundation.h>

@class HeaderFilterView;

@interface HeaderInfo : NSObject

@property (getter = isOpen) BOOL open;
@property (strong, nonatomic) NSMutableDictionary *infos;
@property (assign, nonatomic) NSInteger rows;
@property (strong, nonatomic) NSMutableArray *rowHeights;
@property (weak, nonatomic) HeaderFilterView *headerFilterView;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;

@end
