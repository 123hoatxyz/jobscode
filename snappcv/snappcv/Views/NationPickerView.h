//
//  NationPickerView.h
//  snappcv
//
//  Created by Staff on 7/6/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

//
@import UIKit;
@class NationPickerView;
@protocol NationPickerViewDelegate <NSObject>

@optional
-(void)willDismissNationPickerView:(NationPickerView*)view;
-(void)didSelectComponentInNationPickerView:(NationPickerView*)view value:(NSDictionary*)value;
@end

@interface NationPickerView : UIView<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *ibNationPickerView;
@property (weak, nonatomic) id<NationPickerViewDelegate>delegate;
@property (strong, nonatomic) NSString *nationIdSelected;
@property (strong, nonatomic) NSMutableArray *datas;
@end
