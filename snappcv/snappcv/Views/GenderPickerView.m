//
//  GenderPickerView.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "GenderPickerView.h"

@implementation GenderPickerView

-(void)awakeFromNib{
    _datas = @[@"Select Gender", @"Male", @"Female"];
    _ibGenderPickerView.dataSource = self;
    _ibGenderPickerView.delegate = self;
   [UIView animateWithDuration:0.0 animations:^{
       [_ibGenderPickerView reloadAllComponents];
   }completion:^(BOOL finished) {
       NSInteger index = [_datas indexOfObject:_valueSelected];
       [_ibGenderPickerView selectRow:index inComponent:0 animated:NO];
   }];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [_datas objectAtIndex:row];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_datas count];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if ([_delegate respondsToSelector:@selector(didSelectComponentInGenderPickerView:value:)]) {
        [_delegate didSelectComponentInGenderPickerView:self value:_datas[row]];
    }
}
- (IBAction)dismissView:(id)sender {
    if ( [_delegate respondsToSelector:@selector(willDismissGenderPickerView:)]) {
        [_delegate willDismissGenderPickerView:self];
    }
}
@end
