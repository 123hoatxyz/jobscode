//
//  GenderPickerView.h
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//
@import UIKit;
@class GenderPickerView;
@protocol GenderPickerViewDelegate <NSObject>

@optional
-(void)willDismissGenderPickerView:(GenderPickerView*)view;
-(void)didSelectComponentInGenderPickerView:(GenderPickerView*)view value:(NSString*)value;
@end
@interface GenderPickerView : UIView<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *ibGenderPickerView;
@property (weak, nonatomic) id<GenderPickerViewDelegate>delegate;
@property (strong, nonatomic) NSString *valueSelected;
@property (strong, nonatomic) NSArray *datas;
@end
