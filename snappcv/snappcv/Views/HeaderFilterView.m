//
//  HeaderFilterView.m
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "HeaderFilterView.h"
#import "Constants.h"

@implementation HeaderFilterView

-(void)awakeFromNib{
    [_ibExpandCollapseButton setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateSelected];
    // set up the tap gesture recognizer
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressExpandCollapseMenu:)];
    [self addGestureRecognizer:tapGesture];
}
- (IBAction)pressExpandCollapseMenu:(id)sender {
    [self toggleOpenWithUserAction:YES];
}

- (void)toggleOpenWithUserAction:(BOOL)userAction {
    
    _ibExpandCollapseButton.selected = !_ibExpandCollapseButton.selected;
    
    if (userAction) {
        if (_ibExpandCollapseButton.selected) {
            if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                [self.delegate sectionHeaderView:self sectionOpened:self.section];
            }
        }
        else {
            if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                [self.delegate sectionHeaderView:self sectionClosed:self.section];
            }
        }
    }
    [self changedBackground: _ibExpandCollapseButton.selected];
}

-(void)changedBackground:(BOOL)userAction{
    _ibHeaderFilterView.backgroundColor = userAction?kColorMenuSelected:[UIColor clearColor];
}
-(void)setHeaderInfo:(id)info{
    [_ibTitleLabel setText:info];
}
@end
