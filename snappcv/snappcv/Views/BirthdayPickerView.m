
//
//  BirthdayPickerView.m
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "BirthdayPickerView.h"

@implementation BirthdayPickerView


- (IBAction)updateBirthday:(id)sender {
    if ([_delegate respondsToSelector:@selector(didSelectInBirthdayPickerView:date:)]) {
        [_delegate didSelectInBirthdayPickerView:self date:_ibBirthdayPicker.date];
    }
}

-(void)setDateSelect:(NSDate *)dateSelect{
    if (dateSelect) {
        _ibBirthdayPicker.date = dateSelect;
    }
}
- (IBAction)dismissView:(id)sender {
    if ([_delegate respondsToSelector:@selector(willDismissBirthdayPickerView:)]) {
        [_delegate willDismissBirthdayPickerView:self];
    }
}
@end
