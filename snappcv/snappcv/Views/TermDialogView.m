//
//  TermDialogView.m
//  snappcv
//
//  Created by TuongNguyen on 7/16/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "TermDialogView.h"
#import "Constants.h"
#import "ErrorMessage.h"

@implementation TermDialogView

- (void)awakeFromNib{
    _ibOkButton.layer.cornerRadius = 5;
    _ibContentView.layer.cornerRadius = 5;
    [self setUp];
}
- (IBAction)acceptTerm:(id)sender {
    _ibAcceptTermButton.selected = !_ibAcceptTermButton.selected;
}
- (IBAction)closeDialog:(id)sender {
    if(_ibAcceptTermButton.selected){
        if([_delegate respondsToSelector:@selector(willCheckTermConditionView:)]){
            [_delegate willCheckTermConditionView:self];
        }
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageTermsAndCondition delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }

}

-(void)setUp{
    [_ibTermConditionLabel setText:kTitleTermConditionFull afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
     {
         NSRange termsRange = [[mutableAttributedString string] rangeOfString:kTitleTerms options:NSCaseInsensitiveSearch];
         NSRange conditionsRange = [[mutableAttributedString string] rangeOfString:kTitleConditions options:NSCaseInsensitiveSearch];
         
         
         
         UIFont *boldSystemFont = _ibTermConditionLabel.font;
         CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, 11, NULL);
         if (font) {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorTermsCondition.CGColor range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTUnderlineStyleAttributeName value:@(YES) range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTFontAttributeName value:(__bridge id)font range:conditionsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorTermsCondition.CGColor range:conditionsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTUnderlineStyleAttributeName value:@(YES) range:conditionsRange];
             CFRelease(font);
         }
         
         return mutableAttributedString;
     }];
}

-(void)setCheckTerm:(BOOL)checked{
    _ibAcceptTermButton.selected = checked;
}

@end
