//
//  HeaderFilterView.h
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HeaderFilterView;

@protocol HeaderFilterViewDelegate <NSObject>

@optional
- (void)sectionHeaderView:(HeaderFilterView *)sectionHeaderView sectionOpened:(NSInteger)section;
- (void)sectionHeaderView:(HeaderFilterView *)sectionHeaderView sectionClosed:(NSInteger)section;
@end

@interface HeaderFilterView : UITableViewHeaderFooterView

@property (unsafe_unretained, nonatomic) IBOutlet UIView *ibHeaderFilterView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *ibExpandCollapseButton;
@property (nonatomic) NSInteger section;
@property (nonatomic, weak)  id <HeaderFilterViewDelegate> delegate;

- (void)toggleOpenWithUserAction:(BOOL)userAction;
- (void)changedBackground:(BOOL)userAction;
- (void)setHeaderInfo:(id)info;
@end
