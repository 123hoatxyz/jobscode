//
//  BirthdayPickerView.h
//  snappcv
//
//  Created by Staff on 3/31/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BirthdayPickerView;

@protocol BirthdayPickerViewDelegate <NSObject>

@optional
-(void)didSelectInBirthdayPickerView:(BirthdayPickerView*)view date:(NSDate*)date;
-(void)willDismissBirthdayPickerView:(BirthdayPickerView*)view;

@end
@interface BirthdayPickerView : UIView

@property (weak, nonatomic) IBOutlet UIDatePicker *ibBirthdayPicker;
@property (weak, nonatomic) id<BirthdayPickerViewDelegate> delegate;
@property (strong, nonatomic) NSDate *dateSelect;
@end
