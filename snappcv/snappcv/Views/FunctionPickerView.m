//
//  FunctionPickerView.m
//  snappcv
//
//  Created by Staff on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "FunctionPickerView.h"
#import "NSUserDefaults+Utils.h"
#import "Constants.h"

@implementation FunctionPickerView

-(void)awakeFromNib{
    _datas = [self convertDictToArray:[NSUserDefaults getFunctionList:kDictFunctionKey]];
    
    _ibFunctionPickerView.dataSource = self;
    _ibFunctionPickerView.delegate = self;
   [UIView animateWithDuration:0.0 animations:^{
       [_ibFunctionPickerView reloadAllComponents];
   }completion:^(BOOL finished) {
       if (_functionIdSelected != nil)
       {
           NSInteger index = [self getSelectedIndex:_functionIdSelected];
           [_ibFunctionPickerView selectRow:index inComponent:0 animated:NO];
       }
   }];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSDictionary *function = [_datas objectAtIndex:row];
    return [function objectForKey:@"function_name"];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_datas count];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if ([_delegate respondsToSelector:@selector(didSelectComponentInFunctionPickerView:value:)]) {
        [_delegate didSelectComponentInFunctionPickerView:self value:_datas[row]];
    }
}
- (IBAction)dismissView:(id)sender {
    if ( [_delegate respondsToSelector:@selector(willDismissFunctionPickerView:)]) {
        [_delegate willDismissFunctionPickerView:self];
    }
}

- (NSInteger)getSelectedIndex:(NSString*)selectedId{
    NSInteger index = 0;
    for (int i = 0; i < [_datas count]; i++)
    {
        NSDictionary *dict = [_datas objectAtIndex:i];
        if ([[dict objectForKey:@"function_id"] integerValue] == [selectedId integerValue])
        {
            index = i;
            break;
        }
    }
    return index;
}

-(NSMutableArray*)convertDictToArray:(NSDictionary*)dictArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in dictArray)
    {
        [array addObject:dict];
    }
    
    return array;
}
@end
