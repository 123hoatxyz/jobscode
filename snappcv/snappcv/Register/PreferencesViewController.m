//
//  PreferencesViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "PreferencesViewController.h"
#import "AppDelegate.h"
#import "Profile.h"
#import "Constants.h"
#import "UIApplication+Utils.h"
#import "UIStoryboard+Utils.h"
#import "NSUserDefaults+Utils.h"
#import "APIManager.h"
#import "Preference.h"
#import "WebViewController.h"
#import "IndicatorViewHelper.h"

@interface PreferencesViewController ()
{
    BOOL isPublic;
    Preference *preference;
}
@end

@implementation PreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       [_ibNotificationButton setImage:[UIImage imageNamed:@"check_cir_dis"] forState:UIControlStateNormal];
    [_ibNotificationButton setImage:[UIImage imageNamed:@"check_cir"] forState:UIControlStateSelected];
    [_ibSubscribeButton setImage:[UIImage imageNamed:@"check_cir"] forState:UIControlStateSelected];
    
    _ibTitleLabel.font = kFontDefaultForTitle;
    isPublic = YES;
    preference = [[Preference alloc] init];
}

- (IBAction)pressCheckOnNotification:(id)sender {
    _ibNotificationButton.selected = !_ibNotificationButton.selected;
    preference.notice = _ibNotificationButton.selected;
}
- (IBAction)pressCheckOnSubscribe:(id)sender {
    _ibSubscribeButton.selected = !_ibSubscribeButton.selected;
    preference.subscribe = _ibSubscribeButton.selected;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (IBAction)submitButtonDidTouch:(id)sender {
    if ([UIApplication isWorkMode] == SnappCVWorkModeFinding) {
        //                                 kDictPersonalDetail:
        //                                     @{
        //                                         kDictNationality: (_personalDetail.nationality?_personalDetail.nationality : @""),
        //                                         kDictCurrentLocation: (_personalDetail.currentLocation?_personalDetail.currentLocation : @""),
        //                                         kDictPhoneNumber:(_personalDetail.mobileNumber?_personalDetail.mobileNumber : @""),
        //                                         kDictDateOfBirth:(_personalDetail.dateOfBirth?_personalDetail.dateOfBirth : @""),
        //                                         kDictGender:(_personalDetail.gender? @1 : @0)
        //                                         },
        //                                 kDictProfessionalDetail:
        //                                     @{
        //                                         kDictJobType: (_professionDetail.jobType ? _professionDetail.jobType : @""),
        //                                         kDictCurrentIndustry:(_professionDetail.currentIndustry? _professionDetail.currentIndustry : @""),
        //                                         kDictFunction:(_professionDetail.function? _professionDetail.function : @""),
        //                                         kDictKeySkills:(_professionDetail.keySkills ? _professionDetail.keySkills : @""),
        //                                         kDictExperienceYear:(_professionDetail.experienceYears?_professionDetail.experienceYears : @"")
        //                                         },
        //                                 kDictPreference :
        //                                     @{
        //                                         kDictNotice : @(preference.notice),
        //                                         kDictSubscribe : @(preference.subscribe)
        //                                         }

        Profile *profile = [[Profile alloc] init];
        profile.personalDetail = _personalDetail;
        profile.basicInfo = _basicInfo;
        profile.professionDetail = _professionDetail;
        profile.preference = preference;
        
        id avarta = _personalDetail.avatarBase64String;
        NSDictionary *params = nil;
        if (avarta) {
            params =  @{
                        kDictEmail:(_basicInfo.email?_basicInfo.email : @""),
                        kDictFirstName:(_basicInfo.first_name?_basicInfo.first_name : @""),
                        kDictLastName:(_basicInfo.last_name?_basicInfo.last_name : @""),
                        kDictNationality: (_personalDetail.nationality?_personalDetail.nationality : @""),
                        kDictCurrentLocation: (_personalDetail.currentLocation?_personalDetail.currentLocation : @""),
                        kDictPhoneNumber:(_personalDetail.phoneNumber?_personalDetail.phoneNumber : @""),
                        kDictDateOfBirth:(_personalDetail.dateOfBirth?_personalDetail.dateOfBirth : @""),
                        kDictGender:(_personalDetail.gender? @1 : @0),
                        kDictJobType: (_professionDetail.jobType ? _professionDetail.jobType : @""),
                        kDictCurrentIndustry:(_professionDetail.currentIndustry? _professionDetail.currentIndustry : @""),
                        kDictFunctionId:(_professionDetail.function? _professionDetail.function : @""),
                        kDictKeySkills:(_professionDetail.keySkills ? _professionDetail.keySkills : @""),
                        kDictExperienceYear:(_professionDetail.experienceYears?_professionDetail.experienceYears : @"0"),
                        kDictExperienceMonth:(_professionDetail.experienceMonths?_professionDetail.experienceMonths : @"0"),
                        kDictNotice : @(preference.notice),
                        kDictSubscribe : @(preference.subscribe),
                        kDictAvatarContentBase64String:_personalDetail.avatarBase64String,
                        kDictAvatarName:@"MyAvarta"
                        };
        }else{
            params =  @{
                        kDictEmail:(_basicInfo.email?_basicInfo.email : @""),
                        kDictFirstName:(_basicInfo.first_name?_basicInfo.first_name : @""),
                        kDictLastName:(_basicInfo.last_name?_basicInfo.last_name : @""),
                        kDictNationality: (_personalDetail.nationality?_personalDetail.nationality : @""),
                        kDictCurrentLocation: (_personalDetail.currentLocation?_personalDetail.currentLocation : @""),
                        kDictPhoneNumber:(_personalDetail.phoneNumber?_personalDetail.phoneNumber : @""),
                        kDictDateOfBirth:(_personalDetail.dateOfBirth?_personalDetail.dateOfBirth : @""),
                        kDictGender:(_personalDetail.gender? @1 : @0),
                        kDictJobType: (_professionDetail.jobType ? _professionDetail.jobType : @""),
                        kDictCurrentIndustry:(_professionDetail.currentIndustry? _professionDetail.currentIndustry : @""),
                        kDictFunctionId:(_professionDetail.function? _professionDetail.function : @""),
                        kDictKeySkills:(_professionDetail.keySkills ? _professionDetail.keySkills : @""),
                        kDictExperienceYear:(_professionDetail.experienceYears?_professionDetail.experienceYears : @"0"),
                        kDictExperienceMonth:(_professionDetail.experienceMonths?_professionDetail.experienceMonths : @"0"),
                        kDictNotice : @(preference.notice),
                        kDictSubscribe : @(preference.subscribe)
                        
                        };
        }
        
        NSLog(@"updateCandidateProfile params: %@",params);
        [IndicatorViewHelper showHUDInWindow:YES];
        [[APIManager sharedManager] updateCandidateProfile:params completion:^(BOOL success, NSString *message, NSString *avatarURL) {
            if (!success) {
                [IndicatorViewHelper hideAllHUDForWindow:YES];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }else {
                if (![avatarURL isEqual:[NSNull null]] && avatarURL!=nil) {
                    // Update Avatar
                    profile.personalDetail.avatarURL = [NSURL URLWithString:avatarURL];
                }
                
                // Update Profile on NSDefaults
                [NSUserDefaults updateProfile:profile key:kDictProfile];
                
                [IndicatorViewHelper hideAllHUDForWindow:YES];
                AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
                [delegate loadStoryboard:@"Main"];
            }
        }];
        
    }else{
        
        id jobPack = [UIStoryboard viewControllerWithStoryBoardName:kStoryBoardMain identifier:kJobsPackViewController];
        [UIApplication setRootViewController:jobPack];
    }
}

- (IBAction)backButtonDidTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
