//
//  LoginViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UIView+Utils.h"
#import "UIStoryboard+Utils.h"
#import "UIApplication+Utils.h"
#import "APIManager.h"
#import "NSString+Utils.h"
#import "ErrorMessage.h"
#import "LinkedInManager.h"
#import "IndicatorViewHelper.h"

@interface LoginViewController ()<UITextFieldDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([_ibPasswordField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _ibPasswordField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:kTitlePassword attributes:@{NSForegroundColorAttributeName:kColorPlaceHolder}];
        _ibUsernameField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:kTitleUsername attributes:@{NSForegroundColorAttributeName:kColorPlaceHolder}];
        
    }
    _ibPasswordField.textColor = kColorPlaceHolder;
    _ibUsernameField.textColor = kColorPlaceHolder;
    _ibUsernameField.delegate = self;
    _ibPasswordField.delegate = self;
#pragma mark - HardCode for Login
//    _ibUsernameField.text = @"tuongnt@elarion.com";
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backButtonDidTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)forgetPassword:(id)sender {
    id resetPassword = [UIStoryboard viewControllerWithStoryBoardName:kStoryBoardRegister identifier:kResetPasswordViewController];
    [self.navigationController pushViewController:resetPassword animated:YES];
}

-(IBAction)loginWithLinkedIn:(id)sender
{
    [[LinkedInManager sharedInstanced] loginWithLinkedInSuccess:^(NSString *accessToken) {
        NSLog(@"%@", accessToken);
        
        [IndicatorViewHelper showHUDInWindow:YES];
        
        [[APIManager sharedManager] loginWithLinkedIn:accessToken accessSecret:LINKEDIN_CLIENT_SECRET completion:^(NSDictionary *data, BOOL success, NSString *error) {
            [IndicatorViewHelper hideAllHUDForWindow:YES];
            
            if (success) {
                [UIApplication setLogin:YES];
                AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
                [delegate loadStoryboard:@"Main"];
                
            }else{
                UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertError show];
            }
            
        }];
        
    } cancel:^{
        
    } failure:^(NSError *error) {
        
    }];
}

- (IBAction)loginButtonDidTouch:(id)sender {
    
    
    if (![NSString isValidEmail:_ibUsernameField.text] && [NSString isEmptyString:_ibUsernameField.text]) {
        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageUsernameIsNotCorrect delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertError show];
    }else{
        if ([NSString isEmptyString:_ibPasswordField.text]) {
            UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessagePasswordIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertError show];
        }else{
            [IndicatorViewHelper showHUDInWindow:YES];
            [[APIManager sharedManager] loginWithEmail:_ibUsernameField.text password:_ibPasswordField.text completion:^(NSDictionary *data, BOOL success, NSString *error) {
                [IndicatorViewHelper hideAllHUDForWindow:YES];
                if (success) {
                    //NSLog(@"Login Data: %@", data);
                    [UIApplication setLogin:YES];
                    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
                    [delegate loadStoryboard:@"Main"];                   
                }else{
                    UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertError show];
                }
            }];
        }
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    _ibContentView.top = -50;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    _ibContentView.top = 0;
}
@end
