//
//  RegisterViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "RegisterViewController.h"
#import "Constants.h"
#import "UIView+Utils.h"
#import "UIApplication+Utils.h"
#import "UIStoryboard+Utils.h"
#import "PersonalDetailViewController.h"
#import "CompanyInfoViewController.h"
#import "APIManager.h"
#import "BasicInfo.h"
#import "NSString+Utils.h"
#import "ErrorMessage.h"
#import "NSUserDefaults+Utils.h"
#import "UIDevice+Utils.h"
#import "IndicatorViewHelper.h"

#define kDEFAULT_POSITION_TOP_1     50
#define kDEFAULT_POSITION_TOP_2     90
#define kHEIGHT_SCROLLVIEW          185

@interface RegisterViewController (){
    UITextField *activeField;
    BasicInfo *basicInfo;
}
@property (strong, nonatomic) UIImageView *ibBackgroundTemp;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUp];
    [self registerForKeyboardNotifications];
    
    _isSelectedHire = NO;

    [_ibHireButton setImage:[UIImage imageNamed:@"radio_disable"] forState:UIControlStateNormal];
    [_ibFindButton setImage:[UIImage imageNamed:@"radio_enable"] forState:UIControlStateNormal];
    
    //update a view mode
    [self showOrHideTheFindJobs];
    //Update variable isFindJobs
    [UIApplication setWorkMode:_isSelectedHire ? SnappCVWorkModeHiring : SnappCVWorkModeFinding];

}

#pragma mark - Event of View Controller

- (IBAction)backButtonDidTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pressHireOrFind:(UIButton*)sender {
    
    [self.view endEditing:YES];
    if (_isSelectedHire && sender.tag == 1) {
        
        [_ibHireButton setImage:[UIImage imageNamed:@"radio_disable"] forState:UIControlStateNormal];
        [_ibFindButton setImage:[UIImage imageNamed:@"radio_enable"] forState:UIControlStateNormal];
        
        _isSelectedHire = NO;
    }else if(!_isSelectedHire && sender.tag == 0){
        
        [_ibFindButton setImage:[UIImage imageNamed:@"radio_disable"] forState:UIControlStateNormal];
        [_ibHireButton setImage:[UIImage imageNamed:@"radio_enable"] forState:UIControlStateNormal];
        _isSelectedHire = YES;
    }
    //update a view mode
    [self showOrHideTheFindJobs];
    //Update variable isFindJobs
    [UIApplication setWorkMode:_isSelectedHire ? SnappCVWorkModeHiring : SnappCVWorkModeFinding];
}

- (IBAction)acceptTerms:(id)sender {
    _ibAcceptTermsButton.selected = !_ibAcceptTermsButton.selected;
}

- (IBAction)registerInformation:(id)sender {
    [self.view endEditing:YES];
    
    if ([UIApplication isWorkMode] == SnappCVWorkModeFinding) {
//#pragma mark - [TODO] Tesk
//        PersonalDetailViewController *personDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:kPersonalDetailViewController];
//        personDetailVC.basicInfo = basicInfo;
//        [self.navigationController pushViewController:personDetailVC animated:YES];
//        return;
        
        if ([NSString isEmptyString:_ibEmailJobField.text] || ![NSString isValidEmail:_ibEmailJobField.text]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageEmailIsNotCorrect delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }else{
            
            basicInfo = [[BasicInfo alloc] init];
            basicInfo.email = _ibEmailJobField.text;
            basicInfo.password = _ibPasswordField.text;
            basicInfo.password_confirmation = _ibConfirmPasswordField.text;
            
            
            if (![NSString isEmptyString:basicInfo.password] || !(basicInfo.password.length > 7)) {
                if ([basicInfo.password isEqualToString:basicInfo.password_confirmation]) {
                        if (_ibAcceptTermsButton.selected) {
                            NSDictionary *params = @{kDictEmail:basicInfo.email, kDictPassword: basicInfo.password, @"device_token":[NSUserDefaults getDeviceToken], @"device_type":[UIDevice stringNameDeviceType]};
#pragma mark - Register email
                            [IndicatorViewHelper showHUDInWindow:YES];
                            [[APIManager sharedManager] registerUser:params completion:^(BOOL success, NSString *message) {
                                [IndicatorViewHelper hideAllHUDForWindow:YES];
                                if (success) {
                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                    alertView.tag = 1000;
                                    [alertView show];
                                    
                                }else{
                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                    [alertView show];
                                }
                            }];
                            
                        }else{
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageTermsAndCondition delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alertView show];
                            
                        }
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessagePasswordIsNotMatch delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessagePasswordIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
        
    }else{
        NSLog(@"Go to Hire Jobs");
        CompanyInfoViewController *companyInfoVC = [self.storyboard instantiateViewControllerWithIdentifier:kCompanyInfoViewController];
        [self.navigationController pushViewController:companyInfoVC animated:YES];
    }
}



#pragma mark UITextField's Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    activeField = textField;
}


-(void)textFieldShouldResign{
    if (_isSelectedHire) {
        [_ibCompanyField resignFirstResponder];
        [_ibEmailField resignFirstResponder];
        [_ibContactField resignFirstResponder];
    }else{
        [_ibEmailJobField resignFirstResponder];
        [_ibPasswordField resignFirstResponder];
        [_ibConfirmPasswordField resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//show or hide a view option for Find Jobs or Hire
-(void)showOrHideTheFindJobs{
    if (_isSelectedHire) {
        _ibCompanyField.hidden = NO;
        _ibContactField.hidden = NO;
        _ibEmailField.hidden = NO;
        _ibEmailJobField.hidden = YES;
        _ibPasswordField.hidden = YES;
        _ibConfirmPasswordField.hidden = YES;
    }else{
        _ibCompanyField.hidden = YES;
        _ibContactField.hidden = YES;
        _ibEmailField.hidden = YES;
        _ibEmailJobField.hidden = NO;
        _ibPasswordField.hidden = NO;
        _ibConfirmPasswordField.hidden = NO;
    }
}

#pragma mark - Methods
- (void)setUp {
    [_ibHireButton setImage:[UIImage imageNamed:@"radio_enable"] forState:UIControlStateNormal];
    [_ibFindButton setImage:[UIImage imageNamed:@"radio_disable"] forState:UIControlStateNormal];
    
    //Assign Title
    _ibContactField.placeholder = kTitleContactPerson;
    _ibCompanyField.placeholder = kTitleCompanyName;
    _ibEmailField.placeholder = kTitleEmailAddress;
    
    //Configure color of Placeholde's Text field
    if ([_ibContactField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = kColorPlaceHolder;
        _ibContactField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleContactPerson attributes:@{NSForegroundColorAttributeName: color}];
        _ibCompanyField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleCompanyName attributes:@{NSForegroundColorAttributeName: color}];
        _ibEmailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleEmailAddress attributes:@{NSForegroundColorAttributeName: color}];
        _ibEmailJobField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleEmailAddress attributes:@{NSForegroundColorAttributeName: color}];
        _ibPasswordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitlePassword attributes:@{NSForegroundColorAttributeName: color}];
        _ibConfirmPasswordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleConfirmPassword attributes:@{NSForegroundColorAttributeName: color}];
    }
    _ibContactField.textColor = kColorPlaceHolder;
    _ibCompanyField.textColor = kColorPlaceHolder;
    _ibEmailField.textColor = kColorPlaceHolder;
    _ibEmailJobField.textColor = kColorPlaceHolder;
    _ibPasswordField.textColor = kColorPlaceHolder;
    _ibConfirmPasswordField.textColor = kColorPlaceHolder;
    
    
    //TextField Delegate
    _ibCompanyField.delegate = self;
    _ibEmailField.delegate = self;
    _ibContactField.delegate = self;
    _ibEmailJobField.delegate = self;
    _ibPasswordField.delegate = self;
    _ibConfirmPasswordField.delegate = self;
    
   
    [_ibTermsConditonLabel setText:kTitleTermConditionFull afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
     {
         NSRange termsRange = [[mutableAttributedString string] rangeOfString:kTitleTerms options:NSCaseInsensitiveSearch];
         NSRange conditionsRange = [[mutableAttributedString string] rangeOfString:kTitleConditions options:NSCaseInsensitiveSearch];

       
         
         UIFont *boldSystemFont = _ibTermsConditonLabel.font;
         CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, 11, NULL);
         if (font) {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorTermsCondition.CGColor range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTUnderlineStyleAttributeName value:@(YES) range:termsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTFontAttributeName value:(__bridge id)font range:conditionsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorTermsCondition.CGColor range:conditionsRange];
             [mutableAttributedString addAttribute:(NSString*)kCTUnderlineStyleAttributeName value:@(YES) range:conditionsRange];
             CFRelease(font);
         }
         
         return mutableAttributedString;
     }];
    
    //pre-setting content size for Scrollview if Device has screensize is 3.5 inch
    if (isDeviceiPhone35Inch) {
        _ibContentScrollView.contentSize = CGSizeMake([UIView widthDevice], kHEIGHT_SCROLLVIEW);
        _ibContentView.height = kHEIGHT_SCROLLVIEW;
    }
    
    //
    _ibTermsConditonLabel.linkAttributes = @{ NSForegroundColorAttributeName: (__bridge id)kColorTermsCondition.CGColor,
                                              NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle] };
    _ibTermsConditonLabel.delegate = self;
    _ibTermsConditonLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    NSRange rangeTerms = [_ibTermsConditonLabel.text rangeOfString:kTitleTerms];
    NSRange rangeCondition = [_ibTermsConditonLabel.text rangeOfString:kTitleConditions];
    [_ibTermsConditonLabel addLinkToURL:[NSURL URLWithString:@"action://show-term-condition"] withRange:rangeTerms];
    [_ibTermsConditonLabel addLinkToURL:[NSURL URLWithString:@"action://show-term-condition"] withRange:rangeCondition];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    if ([[url scheme] hasPrefix:@"action"]) {
        if ([[url host] hasPrefix:@"show-term-condition"]) {
            TermDialogView *term = (id)[UIView viewWithNibName:@"TermDialogView"];
            term.delegate = self;
            [term setCheckTerm:_ibAcceptTermsButton.selected];
            [self.view addSubviewWithFullSize:term];
            
            //TermDialogView *reminder = [[TermDialogView alloc]init];
            //reminder.delegate = self;
            //[self.view addSubviewWithFullSize:reminder];
        }
    }
}

-(void)willCheckTermConditionView:(TermDialogView *)termConditionView{
    [termConditionView removeFromSuperview];
    _ibAcceptTermsButton.selected = YES;
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    [UIView animateWithDuration:0.5 animations:^{
        _ibParentView.top = -kbSize.height;
        [_ibContentScrollView scrollRectToVisible:activeField.frame animated:YES];
    }];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _ibContentScrollView.contentInset = contentInsets;
    _ibContentScrollView.scrollIndicatorInsets = contentInsets;
    _ibParentView.top = 0;
}

#pragma mark - UIAlertView's Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1000) {
        if (buttonIndex == 0) {
            PersonalDetailViewController *personDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:kPersonalDetailViewController];
            personDetailVC.basicInfo = basicInfo;
            [self.navigationController pushViewController:personDetailVC animated:YES];
        }
    }
}
@end
