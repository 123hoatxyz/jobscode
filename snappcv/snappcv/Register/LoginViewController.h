//
//  LoginViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *ibUsernameField;
@property (weak, nonatomic) IBOutlet UITextField *ibPasswordField;
@property (weak, nonatomic) IBOutlet UIView *ibContentView;
@end
