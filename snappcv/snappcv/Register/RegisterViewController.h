//
//  RegisterViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"
#import "TermDialogView.h"

@interface RegisterViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate, TTTAttributedLabelDelegate, TermConditionViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *ibHireButton;
@property (weak, nonatomic) IBOutlet UIButton *ibFindButton;
@property (weak, nonatomic) IBOutlet UIButton *ibAcceptTermsButton;
@property (weak, nonatomic) IBOutlet UITextField *ibCompanyField;
@property (weak, nonatomic) IBOutlet UITextField *ibContactField;
@property (weak, nonatomic) IBOutlet UITextField *ibEmailField;
@property (weak, nonatomic) IBOutlet UIView *ibContentView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibTermsConditonLabel;
@property (weak, nonatomic) IBOutlet UITextField *ibEmailJobField;
@property (weak, nonatomic) IBOutlet UITextField *ibPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *ibConfirmPasswordField;
@property (weak, nonatomic) IBOutlet UIScrollView *ibContentScrollView;
@property (weak, nonatomic) IBOutlet UIView *ibParentView;


@property (assign, nonatomic) BOOL isSelectedHire;
@end
