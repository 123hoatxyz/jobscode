//
//  PersonalDetailViewController.m
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "PersonalDetailViewController.h"
#import "PersonalDetailTableViewCell.h"
#import "PersonalDetailTableViewCellB.h"
#import "PersonalDetailTableViewCellC.h"
#import "UIView+Utils.h"
#import "Constants.h"
#import "GenderPickerView.h"
#import "ErrorMessage.h"
#import "NSDate+Utils.h"
#import "PersonalDetail.h"
#import "ProfessionalDetailViewController.h"
#import "UIStoryboard+Utils.h"
#import "PersonalDetailTableViewCellE.h"
#import "BirthdayPickerView.h"
#import "NSDate+Utils.h"
#import "NSString+Utils.h"
#import "NSUserDefaults+Utils.h"
#import "UIImage+Addition.h"
#import "NationPickerView.h"

@import QuartzCore;
@import AssetsLibrary;
@import MobileCoreServices;


#define kHEIGHT_TABLEVIEW           54.0
#define kHEIGHT_TABLEVIEW_MOVE_UP_A   120.0
#define kHEIGHT_TABLEVIEW_MOVE_UP_B   150.0
@implementation NSMutableDictionary (Utils)

+(id)dictWithTitle:(NSString*)title subTitles:(NSArray*)subTitles type:(NSString*)type{
    if (!subTitles) {
        subTitles = [NSArray array];
    }
    return [NSMutableDictionary dictionaryWithObjects:@[title, subTitles, type] forKeys:@[kDictTitle, kDictSubTitles, kDictType]];
}

+(id)dictWithTitle1:(NSString*)title1 title2:(NSString*)title2{
    return [NSMutableDictionary dictionaryWithObjects:@[title1, title2] forKeys:@[@"title1", @"title2"]];
}

-(void)setTitle:(NSString*)title{
    [self setValue:title forKey:kDictTitle];
}
@end


//constants
static NSString *kPersonalDetailTableViewCell = @"PersonalDetailTableViewCell";
static NSString *kPersonalDetailTableViewCellB = @"PersonalDetailTableViewCellB";
static NSString *kPersonalDetailTableViewCellC  = @"PersonalDetailTableViewCellC";
static NSString *kPersonalDetailTableViewCellE = @"PersonalDetailTableViewCellE";
static NSString *kPersonalDetailTableViewCellTypeGender = @"kPersonalDetailTableViewCellTypeGender";
static NSString *kPersonalDetailTableViewCellTypeNation = @"kPersonalDetailTableViewCellTypeNation";


@interface PersonalDetailViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, PersonalDetailTableViewCellDelegate, PersonalDetailTableViewCellBDelegate, GenderPickerViewDelegate, PersonalDetailTableViewCellCDelegate, PersonalDetailTableViewCellEDelegate, NationPickerViewDelegate, BirthdayPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    CGFloat heightTableView;
    NSString *selectedGender;
    PersonalDetail *personalDetail;
    NSDate *dateSelect;
}
@property (strong,nonatomic) NSMutableArray *arrPersonals;

@end

@implementation PersonalDetailViewController

- (void)setUp {
    //Prepare data
    dateSelect = nil;
    personalDetail = [[PersonalDetail alloc] init];
    _arrPersonals = [NSMutableArray arrayWithCapacity:6];
    selectedGender = @"Select Gender";

    [_arrPersonals addObject:[NSMutableDictionary  dictWithTitle1:@"First Name" title2:@"Last Name" ]];
    [_arrPersonals addObject:[NSMutableDictionary  dictWithTitle:@"Nationality" subTitles:nil type:@""]];
    [_arrPersonals addObject:[NSMutableDictionary  dictWithTitle:@"Current Location" subTitles:nil type:@""]];
    [_arrPersonals addObject:[NSMutableDictionary  dictWithTitle:@"Mobile Number" subTitles:nil type:@"number"]];
    [_arrPersonals addObject:[NSMutableDictionary  dictWithTitle:@"Date of Birth" subTitles:nil type:@""]];
    [_arrPersonals addObject:[NSMutableDictionary  dictWithTitle:@"Select Gender" subTitles:[NSArray arrayWithObjects:@"Male",@"Female", nil] type:@""]];
    
    // Do any additional setup after loading the view.
    [_ibPersonalDetailTable registerNib:[UINib nibWithNibName:kPersonalDetailTableViewCell bundle:nil] forCellReuseIdentifier:kPersonalDetailTableViewCell];
    [_ibPersonalDetailTable registerNib:[UINib nibWithNibName:kPersonalDetailTableViewCellB bundle:nil] forCellReuseIdentifier:kPersonalDetailTableViewCellB];
    [_ibPersonalDetailTable registerNib:[UINib nibWithNibName:kPersonalDetailTableViewCellC bundle:nil] forCellReuseIdentifier:kPersonalDetailTableViewCellC];
    [_ibPersonalDetailTable registerNib:[UINib nibWithNibName:kPersonalDetailTableViewCellE bundle:nil] forCellReuseIdentifier:kPersonalDetailTableViewCellE];
    _ibPersonalDetailTable.dataSource = self;
    _ibPersonalDetailTable.delegate = self;
    //_ibPersonalDetailTable.scrollEnabled = (isDeviceiPhone35Inch? YES : NO);
    
    _ibTitleLabel.font = kFontDefaultForTitle;
    
    
    //add a tap on image view
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPhotoMenu)];
    singleTap.numberOfTapsRequired = 1;
    [_ibPersonalImageView setUserInteractionEnabled:YES];
    [_ibPersonalImageView addGestureRecognizer:singleTap];
}

- (void)showPhotoMenu{
    NSString *actionSheetTitle = @"Add Avatar!"; //Action Sheet Title
    NSString *option1 = @"Take Photo";
    NSString *option2 = @"Choose from Gallery";
    NSString *cancelTitle = @"Cancel";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:option1, option2, nil];
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Take photo
    if (buttonIndex == 0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeCamera] == NO){
            return;
        }
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    // Choose from Gallery
    else if (buttonIndex == 1)
    {
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO){
            return;
        }
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        picker.allowsEditing = NO;
        picker.delegate = self;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}
- (void)openPhotoGalary{
    
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO){
        return;
    }
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    mediaUI.allowsEditing = NO;
    mediaUI.delegate = self;
    
    [self presentViewController:mediaUI animated:YES completion:NULL];
    
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    UIImage *newImage = [UIImage compressImage:[info objectForKey:UIImagePickerControllerOriginalImage] size:CGSizeMake(_ibPersonalImageView.width, _ibPersonalImageView.height)];
    
    personalDetail.avatarBase64String = [NSString encodeToBase64String:newImage];
    [self dismissViewControllerAnimated:YES completion:^{
        //UIImageView
        
        _ibPersonalImageView.image = newImage;
        _ibPersonalImageView.layer.cornerRadius = _ibPersonalImageView.width/2;
        _ibPersonalImageView.layer.masksToBounds = YES;
        _ibPersonalImageView.layer.borderWidth = 10.0f;
        _ibPersonalImageView.layer.borderColor = kColorCircleImageView.CGColor;
        _ibPersonalImageView.backgroundColor = [UIColor clearColor];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUp];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    heightTableView = _ibPersonalDetailTable.height;
}

#pragma mark UITableView datasource and Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kHEIGHT_TABLEVIEW;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrPersonals count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        PersonalDetailTableViewCellC *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCellC];
        cell.delegate = self;
        [cell setCellInfo:[_arrPersonals objectAtIndex:indexPath.row]withKeyPad:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == 1) {
        PersonalDetailTableViewCellB *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCellB];
        cell.delegate = self;
        [cell setCellInfo:[_arrPersonals objectAtIndex:indexPath.row]];
        cell.type = kPersonalDetailTableViewCellTypeNation;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == 5) {
        PersonalDetailTableViewCellB *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCellB];
        cell.delegate = self;
        [cell setCellInfo:[_arrPersonals objectAtIndex:indexPath.row]];
        cell.type = kPersonalDetailTableViewCellTypeGender;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == 4) {
        PersonalDetailTableViewCellE *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCellE];
        cell.delegate = self;
        [cell setCellInfo:[_arrPersonals objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        PersonalDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCell];
        [cell setCellInfo:[_arrPersonals objectAtIndex:indexPath.row]];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kNoCellIdentifier"];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self tableViewWillChangeHeight:heightTableView scrollEnabled:NO];
    NSLog(@"Person detail:%@", personalDetail);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)willBeginEdit:(PersonalDetailTableViewCell *)cell{
    NSIndexPath *indexPath = [_ibPersonalDetailTable indexPathForCell:cell];
    
    switch (indexPath.row) {
        case 3:
            [self tableViewWillChangeHeight:(isDeviceiPhone35Inch?kHEIGHT_TABLEVIEW_MOVE_UP_A-88:kHEIGHT_TABLEVIEW_MOVE_UP_A) scrollEnabled:YES];
            break;
        case 4:
            [self tableViewWillChangeHeight:(isDeviceiPhone35Inch?kHEIGHT_TABLEVIEW_MOVE_UP_B-88:kHEIGHT_TABLEVIEW_MOVE_UP_B) scrollEnabled:YES];
            break;
        default:
            break;
    }
}

-(void)willEndEdit:(PersonalDetailTableViewCell *)cell{
    [self.view endEditing:YES];
    [self tableViewWillChangeHeight:heightTableView scrollEnabled:NO];
    
    NSIndexPath *indexPath = [_ibPersonalDetailTable indexPathForCell:cell];
    switch (indexPath.row) {
        case 2:
            personalDetail.currentLocation = cell.ibTitleField.text;
            break;
        case 3:
            personalDetail.phoneNumber = cell.ibTitleField.text;
            break;
        case 4:
            personalDetail.dateOfBirth  = cell.ibTitleField.text;
            break;
        default:
            break;
    }
}

-(void)didChangeValueInPersonalDetailTableViewCell:(PersonalDetailTableViewCell *)cell value:(NSString *)value{
    NSIndexPath *indexPath = [_ibPersonalDetailTable indexPathForCell:cell];
    switch (indexPath.row) {
        case 0:
            personalDetail.nationality = cell.ibTitleField.text;
            break;
        case 2:
            personalDetail.currentLocation = cell.ibTitleField.text;
            break;
        case 3:
            personalDetail.phoneNumber = cell.ibTitleField.text;
            break;
        case 4:
            personalDetail.dateOfBirth  = cell.ibTitleField.text;
            break;
        default:
            break;
    }
    
}

-(void)tableViewWillChangeHeight:(CGFloat)height scrollEnabled:(BOOL)scrollEnabled{
//    if (!isDeviceiPhone35Inch) {
//        _ibPersonalDetailTable.scrollEnabled = scrollEnabled;
//    }
//    _ibPersonalDetailTable.height = height;
//    _ibPersonalDetailTable.contentSize =  CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds), ([_arrPersonals count]*kHEIGHT_TABLEVIEW));
}

#pragma mark - PersonalDetailTableViewCellB's Delegate
-(void)showPickerViewFromPersonalDetailCell:(PersonalDetailTableViewCellB *)cell withType:(NSString *)type{

    if ([type isEqualToString:kPersonalDetailTableViewCellTypeGender]) {
        GenderPickerView *pickerView = (id)[GenderPickerView viewWithNibName:@"GenderPickerView"];
        pickerView.delegate = self;
        pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height,
                                      [UIView widthDevice], pickerView.frame.size.height);
        pickerView.valueSelected = selectedGender;
        [self.view addSubview:pickerView];
    }
    else if([type isEqualToString:kPersonalDetailTableViewCellTypeNation]){
        NationPickerView *pickerView = (id)[NationPickerView viewWithNibName:@"NationPickerView"];
        pickerView.delegate = self;
        pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
        // pickerView.nationIdSelected = _candidateProfile.personalDetail.nationality;
        [self.view addSubview:pickerView];
    }
    [self.ibPersonalDetailTable setUserInteractionEnabled:NO];
}

#pragma mark - GenderPickerView's Delegate
-(void)willDismissGenderPickerView:(GenderPickerView *)view{
    [view removeFromSuperview];
    [self.ibPersonalDetailTable setUserInteractionEnabled:YES];
}

-(void)didSelectComponentInGenderPickerView:(GenderPickerView *)view value:(NSString *)value{
    NSLog(@"Gender Value: %@", value);
    NSMutableDictionary *dictGender = [_arrPersonals objectAtIndex:5]; [dictGender setTitle:value];
    selectedGender = value;
    //personalDetail.gender = [NSString isMale:value];
    personalDetail.gender = [self getGenderValue:value];
    [self.ibPersonalDetailTable reloadData];
}

#pragma mark - NationPickerView's Delegate
-(void)willDismissNationPickerView:(NationPickerView *)view{
    [view removeFromSuperview];
    [self.ibPersonalDetailTable setUserInteractionEnabled:YES];
}

-(void)didSelectComponentInNationPickerView:(NationPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Nationality Value: %@", [value objectForKey:kDictNationalityName]);
    NSMutableDictionary *dictGender = [_arrPersonals objectAtIndex:1];
    [dictGender setTitle:[value objectForKey:kDictNationalityName]];
    personalDetail.nationality = [value objectForKey:kDictNationality];
    [self.ibPersonalDetailTable reloadData];
}

#pragma mark - PersonalDetailTableViewCellC's Delegate
-(void)didChangeValueInPersonalDetailTableViewCellC:(PersonalDetailTableViewCellC *)cell expInYears:(NSString *)expInYears expInMonths:(NSString *)expInMonths{
    _basicInfo.first_name = expInYears;
    _basicInfo.last_name = expInMonths;
}


#pragma mark - PersonalDetailTableViewCellE's Delegate
-(void)didTapOnButtonAtPersonalDetailTableViewCell:(PersonalDetailTableViewCellE *)cell{
    BirthdayPickerView *pickerView = (id)[BirthdayPickerView viewWithNibName:@"BirthdayPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height,
                                  [UIView widthDevice], pickerView.frame.size.height);
    pickerView.dateSelect = dateSelect;
    [self.view addSubview:pickerView];
    [self.ibPersonalDetailTable setUserInteractionEnabled:NO];
}

-(void)willDismissBirthdayPickerView:(BirthdayPickerView *)view{
    _ibPersonalDetailTable.userInteractionEnabled = YES;
    [view removeFromSuperview];
}

-(void)didSelectInBirthdayPickerView:(BirthdayPickerView *)view date:(NSDate *)date{
    dateSelect = date;
    personalDetail.dateOfBirth = [NSDate dateFormatDefault:date];
    
    NSMutableDictionary *dict = [_arrPersonals objectAtIndex:4];
    [dict setTitle:[NSDate birthdayFormat:date]];
    //update table view
    [_ibPersonalDetailTable reloadData];
    _ibPersonalDetailTable.userInteractionEnabled = YES;
    [view removeFromSuperview];
}

-(NSString*)getGender:(NSString*)value{
    NSString *gender = @"";
    
    if ([personalDetail.gender isEqual:@"0"])
    {
        gender = @"Male";
    }
    else if ([personalDetail.gender isEqual:@"1"])
    {
        gender = @"Female";
    }
    else
    {
        gender = @"Select Gender";
    }
    
    return gender;
}

-(NSString*)getGenderValue:(NSString*)name{
    NSString *gender = @"";
    
    if ([name isEqual:@"Male"])
    {
        gender = @"0";
    }
    else if ([name isEqual:@"Female"])
    {
        gender = @"1";
    }
    else
    {
        gender = @"-1";
    }
    
    return gender;
}

#pragma mark - Event
- (IBAction)next:(id)sender {
    // first name
    if ([NSString isEmptyString:_basicInfo.first_name]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageFirstNameIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // last name
    if ([NSString isEmptyString:_basicInfo.last_name]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageLastNameIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // nation
    if ([NSString isEmptyString:personalDetail.nationality]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageNationIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // location
    if ([NSString isEmptyString:personalDetail.currentLocation]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageLocationIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // phone
    if ([NSString isEmptyString:personalDetail.phoneNumber] || ![NSString validPhone:personalDetail.phoneNumber]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageMobileInvalid delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // day of birth
    if ( [NSString isEmptyString:personalDetail.dateOfBirth] || ![NSDate isValidDateOfBirth:personalDetail.dateOfBirth]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageBirthInvalid delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // gender
    if ([NSString isEmptyString:personalDetail.gender]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageGenderInvalid delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    ProfessionalDetailViewController *professionVC = [UIStoryboard viewControllerWithStoryBoardName:kStoryBoardRegister identifier:kProfessionalDetailViewController];
    professionVC.basicInfo = _basicInfo;
    professionVC.personalDetail = personalDetail;
    if (personalDetail.avatarBase64String) {
        [NSUserDefaults avatarBase64String:personalDetail.avatarBase64String];
    }
    [self.navigationController pushViewController:professionVC animated:YES];
    
}

@end
