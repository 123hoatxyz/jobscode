//
//  ProfessionalDetailViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicInfo.h"
#import "PersonalDetail.h"

@interface ProfessionalDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *ibContentView;
@property (weak, nonatomic) IBOutlet UITableView *ibProfessionTable;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (strong, nonatomic) BasicInfo *basicInfo;
@property (strong, nonatomic) PersonalDetail *personalDetail;
@end
