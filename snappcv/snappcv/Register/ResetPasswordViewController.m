//
//  ResetPasswordViewController.m
//  snappcv
//
//  Created by Hoat Ha Van on 3/30/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "Constants.h"
#import "APIManager.h"
#import "NSString+Utils.h"
#import "ErrorMessage.h"

@interface ResetPasswordViewController ()<UITextFieldDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *ibEmailField;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([_ibEmailField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _ibEmailField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:kTitleEmailAddress attributes:@{NSForegroundColorAttributeName:kColorPlaceHolder}];
        
    }
    _ibEmailField.textColor = kColorPlaceHolder;
    _ibEmailField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}
- (IBAction)resetPassword:(id)sender {
    if (![NSString isEmptyString:_ibEmailField.text]) {
        
        if (![NSString isValidEmail:_ibEmailField.text] ) {
            UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:nil message: kErrorMessageEmailIsNotCorrect delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertError show];
        }else{
            
            [[APIManager sharedManager] resetPasswordWithAnEmail:_ibEmailField.text completion:^(BOOL success, NSString* message) {
                if (success) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alertView.tag = 1000;
                    [alertView show];
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                }
            }];
        }
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageEmailIsEmpty delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
- (IBAction)backLoginView:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIAlertview's Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1000) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
