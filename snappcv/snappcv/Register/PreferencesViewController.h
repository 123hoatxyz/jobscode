//
//  PreferencesViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfessionDetail.h"
#import "PersonalDetail.h"
#import "BasicInfo.h"

@interface PreferencesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *ibNotificationButton;
@property (weak, nonatomic) IBOutlet UIButton *ibSubscribeButton;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (strong, nonatomic) ProfessionDetail *professionDetail;
@property (strong, nonatomic) BasicInfo *basicInfo;
@property (strong, nonatomic) PersonalDetail *personalDetail;
@end
