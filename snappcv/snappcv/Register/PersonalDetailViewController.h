//
//  PersonalDetailViewController.h
//  snappcv
//
//  Created by Staff on 2/28/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicInfo.h"
@interface PersonalDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *ibPersonalImageView;
@property (weak, nonatomic) IBOutlet UITableView *ibPersonalDetailTable;
@property (weak, nonatomic) IBOutlet UIView *ibContentView;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (strong, nonatomic) BasicInfo *basicInfo;
@end
