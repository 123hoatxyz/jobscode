//
//  ProfessionalDetailViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "ProfessionalDetailViewController.h"
#import "PersonalDetailTableViewCellB.h"
#import "PersonalDetailTableViewCellC.h"
#import "PersonalDetailTableViewCellD.h"
#import "PersonalDetailTableViewCell.h"
#import "UIView+Utils.h"
#import "ErrorMessage.h"
#import "Constants.h"
#import "ProfessionDetail.h"
#import "PreferencesViewController.h"
#import "UIStoryboard+Utils.h"
#import "IndustryPickerView.h"
#import "FunctionPickerView.h"
#import "NSString+Utils.h"

#define kHEIGHT_TABLEVIEW_MOVE_UP       255.0
#define kHEIGHT_TABLEVIEW               54.0

static NSString *kPersonalDetailTableViewCell = @"PersonalDetailTableViewCell";
static NSString *kPersonalDetailTableViewCellB = @"PersonalDetailTableViewCellB";
static NSString *kPersonalDetailTableViewCellC  = @"PersonalDetailTableViewCellC";
static NSString *kPersonalDetailTableViewCellD  = @"PersonalDetailTableViewCellD";
static NSString *kProfessionalDetailTableViewCellTypeIndustry = @"kProfessionalDetailTableViewCellTypeIndustry";
static NSString *kProfessionalDetailTableViewCellTypeFunction = @"kProfessionalDetailTableViewCellTypeFunction";

@implementation NSMutableDictionary (Utils)

+(id)dictWithTitle:(NSString*)title subTitles:(NSArray*)subTitles type:(NSString*)type{
    if (!subTitles) {
        subTitles = [NSArray array];
    }
    return [NSMutableDictionary dictionaryWithObjects:@[title, subTitles, type] forKeys:@[kDictTitle, kDictSubTitles, kDictType]];
}

+(id)dictWithTitle1:(NSString*)title1 title2:(NSString*)title2{
    return [NSMutableDictionary dictionaryWithObjects:@[title1, title2] forKeys:@[@"title1", @"title2"]];
}

-(void)setTitle:(NSString*)title{
    [self setValue:title forKey:kDictTitle];
}

@end


@interface ProfessionalDetailViewController ()<UITableViewDataSource, UITableViewDelegate, PersonalDetailTableViewCellCDelegate, PersonalDetailTableViewCellBDelegate, IndustryPickerViewDelegate, FunctionPickerViewDelegate,
PersonalDetailTableViewCellDelegate>{
    CGFloat heightTableView;
    ProfessionDetail *professionDetail;
}
@property (strong, nonatomic) NSMutableArray *arrProfessions;
@end

@implementation ProfessionalDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    professionDetail = [[ProfessionDetail alloc] init];
    _arrProfessions = [NSMutableArray arrayWithCapacity:5];
    [_arrProfessions addObject:[NSMutableDictionary dictWithTitle1:@"Job Type" title2:@""]];
    [_arrProfessions addObject:[NSMutableDictionary dictWithTitle1:@"Exp. in Years" title2:@"Exp.in Months"]];
    [_arrProfessions addObject:[NSMutableDictionary dictWithTitle:@"Current Industry" subTitles:nil type:@""]];
    [_arrProfessions addObject:[NSMutableDictionary dictWithTitle:@"Function" subTitles:nil type:@""]];
    [_arrProfessions addObject:[NSMutableDictionary dictWithTitle1:@"Key Skills" title2:@""]];
    //set up profession table
    [_ibProfessionTable registerNib:[UINib nibWithNibName:kPersonalDetailTableViewCell bundle:nil] forCellReuseIdentifier:kPersonalDetailTableViewCell];
    [_ibProfessionTable registerNib:[UINib nibWithNibName:kPersonalDetailTableViewCellB bundle:nil] forCellReuseIdentifier:kPersonalDetailTableViewCellB];
    [_ibProfessionTable registerNib:[UINib nibWithNibName:kPersonalDetailTableViewCellC bundle:nil] forCellReuseIdentifier:kPersonalDetailTableViewCellC];
    _ibProfessionTable.dataSource = self;
    _ibProfessionTable.delegate = self;
    _ibProfessionTable.scrollEnabled = isDeviceiPhone35Inch? YES: NO;
    
    _ibTitleLabel.font = kFontDefaultForTitle;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    heightTableView = _ibProfessionTable.height;
}

- (IBAction)backButtonDidTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark UITableView datasource and Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kHEIGHT_TABLEVIEW;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrProfessions count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        PersonalDetailTableViewCellC *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCellC];
        cell.delegate = self;
        [cell setCellInfo:[_arrProfessions objectAtIndex:indexPath.row]withKeyPad:1];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if (indexPath.row == 2) {
        PersonalDetailTableViewCellB *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCellB];
        cell.delegate = self;
        [cell setCellInfo:[_arrProfessions objectAtIndex:indexPath.row]];
        cell.type = kProfessionalDetailTableViewCellTypeIndustry;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if (indexPath.row == 3) {
        PersonalDetailTableViewCellB *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCellB];
        cell.delegate = self;
        [cell setCellInfo:[_arrProfessions objectAtIndex:indexPath.row]];
        cell.type = kProfessionalDetailTableViewCellTypeFunction;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        PersonalDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalDetailTableViewCell];
        [cell setCellInfo:[_arrProfessions objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }
    
    return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"kNoCellIdentifier"];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self tableViewWillChangeHeight:heightTableView scrollEnabled:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark PersonDetailCell's Delegate
-(void)willBeginEdit:(PersonalDetailTableViewCell *)cell{
    NSIndexPath *indexPath = [_ibProfessionTable indexPathForCell:cell];
    if (indexPath.row == 6 || indexPath.row == 4) {
        [self tableViewWillChangeHeight:(isDeviceiPhone35Inch?kHEIGHT_TABLEVIEW_MOVE_UP-88:kHEIGHT_TABLEVIEW_MOVE_UP) scrollEnabled:YES];
    }
    
}

-(void)willEndEdit:(PersonalDetailTableViewCell *)cell{
    [self.view endEditing:YES];
    [self tableViewWillChangeHeight:heightTableView scrollEnabled:NO];
}

-(void)didChangeValueInPersonalDetailTableViewCell:(PersonalDetailTableViewCell*)cell value:(NSString*)value{
    NSIndexPath *indexPath = [_ibProfessionTable indexPathForCell:cell];
    NSLog(@"Value: %@%ld", value, (long)indexPath.row);
    switch (indexPath.row) {
        case 0:
            professionDetail.jobType = cell.ibTitleField.text;
            break;
        case 4:
            professionDetail.keySkills = cell.ibTitleField.text;
            break;
        default:
            break;
    }
}

-(void)tableViewWillChangeHeight:(CGFloat)height scrollEnabled:(BOOL)scrollEnabled{
    if (!isDeviceiPhone35Inch) {
        _ibProfessionTable.scrollEnabled = scrollEnabled;
    }
    _ibProfessionTable.height = height;
    _ibProfessionTable.contentSize =  CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds), ([_arrProfessions count]*kHEIGHT_TABLEVIEW)+ (scrollEnabled?10:0));
}

#pragma mark - PersonalDetailTableViewCellB's Delegate
-(void)showPickerViewFromPersonalDetailCell:(PersonalDetailTableViewCellB *)cell withType:(NSString *)type{
    
    if ([type isEqualToString:kProfessionalDetailTableViewCellTypeIndustry]) {
        IndustryPickerView *pickerView = (id)[IndustryPickerView viewWithNibName:@"IndustryPickerView"];
        pickerView.delegate = self;
        pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
        pickerView.industryIdSelected = professionDetail.currentIndustry;
        [self.view addSubview:pickerView];
    }
    else if([type isEqualToString:kProfessionalDetailTableViewCellTypeFunction]){
        FunctionPickerView *pickerView = (id)[FunctionPickerView viewWithNibName:@"FunctionPickerView"];
        pickerView.delegate = self;
        pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
        pickerView.functionIdSelected = professionDetail.function;
        [self.view addSubview:pickerView];
    }
    [self.ibProfessionTable setUserInteractionEnabled:NO];
}

#pragma mark - PersonalDetailTableViewCellC's Delegate
-(void)didChangeValueInPersonalDetailTableViewCellC:(PersonalDetailTableViewCellC *)cell expInYears:(NSString *)expInYears expInMonths:(NSString *)expInMonths{
    professionDetail.experienceYears = expInYears;
    professionDetail.experienceMonths = expInMonths;
}

#pragma mark - IndustryPickerView's Delegate
-(void)willDismissIndustryPickerView:(IndustryPickerView *)view{
    [view removeFromSuperview];
    [self.ibProfessionTable setUserInteractionEnabled:YES];
}

-(void)didSelectComponentInIndustryPickerView:(IndustryPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Industry Value: %@ - key: %@", [value objectForKey:kDictIndustryName], [value objectForKey:kDictIndustryId]);
    professionDetail.currentIndustry = [[value objectForKey:kDictIndustryId] stringValue];
    
    NSMutableDictionary *dictIndustry = [_arrProfessions objectAtIndex:2];
    [dictIndustry setTitle:[value objectForKey:kDictIndustryName]];
    [self.ibProfessionTable reloadData];
}

#pragma mark - FunctionPickerView's Delegate
-(void)willDismissFunctionPickerView:(FunctionPickerView *)view{
    [view removeFromSuperview];
    [self.ibProfessionTable setUserInteractionEnabled:YES];
}
-(void)didSelectComponentInFunctionPickerView:(FunctionPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Function Value: %@ - key: %@", [value objectForKey:kDictFunctionName], [value objectForKey:kDictFunctionId]);
    professionDetail.function = [[value objectForKey:kDictFunctionId] stringValue];
    
    NSMutableDictionary *dictFunction = [_arrProfessions objectAtIndex:3];
    [dictFunction setTitle:[value objectForKey:kDictFunctionName]];
    [self.ibProfessionTable reloadData];
}

#pragma mark - Event
- (IBAction)gotoPreference:(id)sender {
    // job type
    if ([NSString isEmptyString:professionDetail.jobType]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageJobtypeIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // exp year
    if ([NSString isEmptyString:professionDetail.experienceYears] || ![NSString validExYear:professionDetail.experienceYears]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageExpYearIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // exp month
    if ([NSString isEmptyString:professionDetail.experienceMonths] || ![NSString validExMonth:professionDetail.experienceMonths]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageExpMonthIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // currentIndustry
    if ([NSString isEmptyString:professionDetail.currentIndustry]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageIndustryIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // function
    if ([NSString isEmptyString:professionDetail.function]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageFunctionIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    // keySkills
    if ([NSString isEmptyString:professionDetail.keySkills]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageKeySkillIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    PreferencesViewController *preferenceVC = [UIStoryboard viewControllerWithStoryBoardName:kStoryBoardRegister identifier:kPreferencesViewController];
    preferenceVC.personalDetail = _personalDetail;
    preferenceVC.professionDetail = professionDetail;
    preferenceVC.basicInfo = _basicInfo;
    [self.navigationController pushViewController:preferenceVC animated:YES];
}

@end
