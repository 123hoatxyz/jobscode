//
//  ErrorMessage.h
//  snappcv
//
//  Created by Staff on 4/4/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#ifndef snappcv_ErrorMessage_h
#define snappcv_ErrorMessage_h

//Network
static NSString *const kErrorMessageNetworkConnect = @"Cannot connect to carrier network";

//Sign-in
static NSString *const kErrorMessageEmailIsNotCorrect = @"Email is empty or invalid.";
static NSString *const kErrorMessageUsernameIsNotCorrect = @"Username is empty or invalid.";
static NSString *const kErrorMessageTermsAndCondition = @"You must agree with the Terms and Conditions.";
static NSString *const kErrorMessagePasswordIsNotMatch = @"Password and Confirm Password do not match.";
static NSString *const KErrorMessagePasswordIsEmpty = @"Password is empty or too short. Try one with at least 8 characters.";
static NSString *const kErrorMessageEmailIsEmpty = @"Your email is not empty";
static NSString *const kErrorMessageFullnameIsEmpty = @"Full name can not be empty";
static NSString *const KErrorMessageFirstNameIsEmpty = @"First name can not be empty";
static NSString *const KErrorMessageLastNameIsEmpty = @"Last name can not be empty";
static NSString *const KErrorMessageMobileInvalid = @"Mobile Number is empty or invalid.";
static NSString *const KErrorMessageBirthInvalid = @"Date of Birth is invalid.";
static NSString *const KErrorMessageGenderInvalid = @"Gender can not be empty";
static NSString *const KErrorMessageNationIsEmpty = @"Nation can not be empty";
static NSString *const KErrorMessageLocationIsEmpty = @"Current Location can not be empty";
static NSString *const KErrorMessageJobtypeIsEmpty = @"Job Type can not be empty";
static NSString *const KErrorMessageExpYearIsEmpty = @"Exp. in Years is invalid";
static NSString *const KErrorMessageExpMonthIsEmpty = @"Exp. in Months is invalid";
static NSString *const KErrorMessageIndustryIsEmpty = @"Current Industry can not be empty";
static NSString *const KErrorMessageFunctionIsEmpty = @"Function can not be empty";
static NSString *const KErrorMessageKeySkillIsEmpty = @"Key Skills can not be empty";
static NSString *const KErrorMessageCVTitleIsEmpty = @"Cv Title can not be empty";


//Save
static NSString *const kErrorMessageSaveVideo = @"Failed to save your video";
static NSString *const kAlertMessageRecordVideoSuccessfully = @"Record video successfully";
static NSString *const KErrorMessageUpdateProfile = @"Update profile failed.";
static NSString *const KSuccessMessageUpdateProfile = @"Update profile successful.";

// Apply job
static NSString *const KSuccessMessageSubmitApplication = @"Submit Application was successful.";
static NSString *const KErrorMessageSubmitApplication = @"Submit Application unsuccessful.";
static NSString *const KErrorMessageCvRequire = @"CV is required.";
static NSString *const KErrorMessageCv5Mb = @"CV attached size must less than 5 MB.";
static NSString *const KErrorMessageCvChooseFailed = @"CV does not exists or could not open. Please choose another one.";


#endif
