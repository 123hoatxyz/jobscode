//
//  SearchJobViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchJobViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *ibPickLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *ibPickIndustryButton;
@property (weak, nonatomic) IBOutlet UIButton *ibPickFunctionButton;
@property (weak, nonatomic) IBOutlet UISlider *ibMinSalarySlider;
@property (weak, nonatomic) IBOutlet UISlider *ibMaxSalarySlider;
@property (weak, nonatomic) IBOutlet UITextField *ibExperienceField;
@property (weak, nonatomic) IBOutlet UIButton *ibSearchButton;
@property (weak, nonatomic) IBOutlet UILabel *ibMinValueLabe;
@property (weak, nonatomic) IBOutlet UILabel *ibMaxValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@end
