//
//  FilterTableViewController.h
//  snappcv
//
//  Created by Hoat Ha Van on 7/8/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterTableViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *ibRecommJobTableView;
@property (strong, nonatomic) id params;
@property (strong, nonatomic) NSString *stringHighlighted;
@property (weak, nonatomic) UITextField *ibSearchField;
-(void)reloadDataTable;
-(void)clearDataTable;
-(void)highlightedText:(NSString*)text;
@end
