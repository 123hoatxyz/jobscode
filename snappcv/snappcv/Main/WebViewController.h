//
//  WebViewController.h
//  snappcv
//
//  Created by Staff on 4/10/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UIWebView *ibWebView;
@property (strong, nonatomic) NSString *urlAddress;
@property (strong, nonatomic) NSString *textTitle;
@end
