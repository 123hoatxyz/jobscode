//
//  SearchJobViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "SearchJobViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "Constants.h"
#import "UIView+Utils.h"
#import "NSNotificationCenter+Utils.h"
#import "NotificationName.h"
#import "RecommededJobsViewController.h"
#import "UIToolbar+Utils.h"
#import "IndustryPickerView.h"
#import "FunctionPickerView.h"
#import "LocationPickerView.h"
#import "NSUserDefaults+Utils.h"
#import "NSString+Utils.h"


@interface SearchJobViewController ()<UITextFieldDelegate, IndustryPickerViewDelegate, FunctionPickerViewDelegate, LocationPickerViewDelegate>

@end

NSMutableDictionary *params;
@implementation SearchJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_ibMinSalarySlider setThumbImage:[UIImage imageNamed:@"slider_thumb"] forState:UIControlStateNormal];
    [_ibMinSalarySlider setMinimumTrackImage:[[UIImage imageNamed:@"slider_min"] stretchableImageWithLeftCapWidth:4 topCapHeight:0] forState:UIControlStateNormal];
    [_ibMinSalarySlider setMaximumTrackImage:[UIImage imageNamed:@"slider_default"] forState:UIControlStateNormal];
    [_ibMaxSalarySlider setThumbImage:[UIImage imageNamed:@"slider_thumb"] forState:UIControlStateNormal];
    [_ibMaxSalarySlider setMinimumTrackImage:[[UIImage imageNamed:@"slider_max"] stretchableImageWithLeftCapWidth:4 topCapHeight:0] forState:UIControlStateNormal];
    [_ibMaxSalarySlider setMaximumTrackImage:[UIImage imageNamed:@"slider_default"] forState:UIControlStateNormal];
    
    [_ibPickFunctionButton setImageEdgeInsets:UIEdgeInsetsMake(0, _ibPickFunctionButton.width-10, 0, 0)];
    [_ibPickIndustryButton setImageEdgeInsets:UIEdgeInsetsMake(0, _ibPickIndustryButton.width-10, 0, 0)];
    [_ibPickLocationButton setImageEdgeInsets:UIEdgeInsetsMake(0, _ibPickLocationButton.width-10, 0, 0)];
    if ([_ibExperienceField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _ibExperienceField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:kTitleEnterYourExperience attributes:@{NSForegroundColorAttributeName: kColorPlaceHolder}];
    }
    
    _ibTitleLabel.font = kFontDefaultForTitle;
    _ibExperienceField.delegate = self;
    _ibExperienceField.inputAccessoryView = [UIToolbar keyboardToolbar:self.view];
    
    /*params = @{@"location_id":[NSNull null], @"industry_id":[NSNull null],
               @"function_id":[NSNull null], @"experience_year":[NSNull null],
               @"min_salary":[NSNull null], @"max_salary":[NSNull null],
               @"is_favorite":@YES, @"is_applied":@YES};*/

    params = [[NSMutableDictionary alloc] init];
    
//    [params setValue:@YES forKey:@"is_favorite"];
//    [params setValue:@YES forKey:@"is_applied"];
//    
//    [params setValue:[NSNull null] forKey:@"location_id"];
//    [params setValue:[NSNull null] forKey:@"industry_id"];
//    [params setValue:[NSNull null] forKey:@"function_id"];
//    
//    [params setValue:[NSNull null] forKey:@"experience_year"];
//    [params setValue:[NSNull null] forKey:@"min_salary"];
//    [params setValue:[NSNull null] forKey:@"max_salary"];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonDidTouch:(id)sender {
    [NSUserDefaults deleteSearchParams];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchJobs:(id)sender {
    if (DEBUG) {
        NSLog(@"Search Jobs");
    }

    
    if(![NSString isEmptyString:_ibExperienceField.text]){
        [params setValue:_ibExperienceField.text forKey:kDictJobExperience];
    }
    
    [NSUserDefaults saveSearchParams:params];
    
    [self.mm_drawerController closeDrawerAnimated:YES completion:NULL];
    [NSNotificationCenter postNotificationName:kNotifyShowingJobsSearchResultScreen];

}

-(IBAction)showLocationPickerView:(id)sender{
    LocationPickerView *pickerView = (id)[LocationPickerView viewWithNibName:@"LocationPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
    pickerView.locationIdSelected = nil;
    [self.view addSubview:pickerView];
}

-(IBAction)showIndustryPickerView:(id)sender{
    IndustryPickerView *pickerView = (id)[IndustryPickerView viewWithNibName:@"IndustryPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
    pickerView.industryIdSelected = nil;
    [self.view addSubview:pickerView];
}

-(IBAction)showFunctionPickerView:(id)sender{
    FunctionPickerView *pickerView = (id)[FunctionPickerView viewWithNibName:@"FunctionPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
    pickerView.functionIdSelected = nil;
    [self.view addSubview:pickerView];
}

#pragma mark - IndustryPickerView's Delegate
-(void)willDismissIndustryPickerView:(IndustryPickerView *)view{
    [view removeFromSuperview];
}

-(void)didSelectComponentInIndustryPickerView:(IndustryPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Value: %@", [value objectForKey:@"industry_name"]);
    
    id industryId = [value objectForKey:kDictJobIndustryId];
    
    if (![industryId isKindOfClass:[NSNull class]] ) {
         [params setValue:[value objectForKey:kDictJobIndustryId] forKey:kDictJobIndustryId];
    }

    [_ibPickIndustryButton setTitle:[value objectForKey:@"industry_name"] forState:UIControlStateNormal];
}

#pragma mark - FunctionPickerView's Delegate
-(void)willDismissFunctionPickerView:(FunctionPickerView *)view{
    [view removeFromSuperview];
}

-(void)didSelectComponentInFunctionPickerView:(FunctionPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Value: %@", [value objectForKey:@"function_name"]);
    
    id function_id = [value objectForKey:kDictFunctionId];
    
    if (![function_id isKindOfClass:[NSNull class]]) {
        [params setValue:function_id forKey:kDictFunctionId];
    }
    
    [_ibPickFunctionButton setTitle:[value objectForKey:@"function_name"] forState:UIControlStateNormal];
}

#pragma mark - LocationPickerView's Delegate
-(void)willDismissLocationPickerView:(LocationPickerView *)view{
    [view removeFromSuperview];
}

-(void)didSelectComponentInLocationPickerView:(LocationPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Value: %@", [value objectForKey:@"location_name"]);
    id location_id = [value objectForKey:kDictJobLocationId];
    if (![location_id isKindOfClass:[NSNull class]]) {
        [params setValue:location_id forKey:kDictJobLocationId];
    }
    
    [_ibPickLocationButton setTitle:[value objectForKey:@"location_name"] forState:UIControlStateNormal];
}

- (IBAction)maxChanged:(id)sender {
    [_ibMaxSalarySlider setValue:((int)((_ibMaxSalarySlider.value + 2.5) / 5) * 5) animated:NO];
    
    _ibMaxValueLabel.text = [NSString stringWithFormat:@"%.1f", _ibMaxSalarySlider.value/10.0];

    float number = [_ibMaxSalarySlider value];
    if(number != 0){
        [params setValue:[NSNumber numberWithFloat:number*100000] forKey:kDictMaxSalary];
    }else{
        [params removeObjectForKey:kDictMaxSalary];
    }
}
- (IBAction)minChanged:(id)sender {
    _ibMinValueLabe.text = [NSString stringWithFormat:@"%ld", (long)_ibMinSalarySlider.value];
    long number = (long)[_ibMinSalarySlider value];
    if(number != 0){
        [params setValue:[NSNumber numberWithLong: number*1000] forKey:kDictMinSalary];
    }else{
        [params removeObjectForKey:kDictMinSalary];
    }
}
@end
