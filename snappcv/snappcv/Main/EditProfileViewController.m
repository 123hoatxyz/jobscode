//
//  EditProfileViewController.m
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "EditProfileViewController.h"
#import "Constants.h"
#import "NSUserDefaults+Utils.h"
#import "Profile.h"
#import "UIView+Utils.h"
#import "NSDate+Utils.h"
#import "EditProfileTableViewController.h"
#import "UIImageView+AFNetworking.h"
#import "APIManager.h"
#import "UIImage+Addition.h"
#import "NSString+Utils.h"
#import "IndicatorViewHelper.h"
#import "ErrorMessage.h"

@import QuartzCore;
@import AssetsLibrary;
@import MobileCoreServices;

@interface EditProfileViewController ()<UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

EditProfileTableViewController *profileController;

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPhotoMenu)];
    singleTap.numberOfTapsRequired = 1;
    [_ibPersonalImageView setUserInteractionEnabled:YES];
    [_ibPersonalImageView addGestureRecognizer:singleTap];
    
    if (profileController != nil)
    {
        if (profileController.candidateProfile.personalDetail.avatarURL) {
            _ibPersonalImageView.layer.cornerRadius = _ibPersonalImageView.width/2;
            _ibPersonalImageView.layer.masksToBounds = YES;
            _ibPersonalImageView.layer.borderWidth = 5.0f;
            _ibPersonalImageView.layer.borderColor = kColorCircleImageView.CGColor;
        
            [_ibPersonalImageView setImageWithURL:profileController.candidateProfile.personalDetail.avatarURL];
        }else{
            _ibPersonalImageView.image = [UIImage imageNamed:@"avatar"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showPhotoMenu{
    NSString *actionSheetTitle = @"Add Avatar!"; //Action Sheet Title
    NSString *option1 = @"Take Photo";
    NSString *option2 = @"Choose from Gallery";
    NSString *cancelTitle = @"Cancel";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:option1, option2, nil];
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {   
    // Take photo
    if (buttonIndex == 0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeCamera] == NO){
            return;
        }
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    // Choose from Gallery
    else if (buttonIndex == 1)
    {
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO){
            return;
        }
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        picker.allowsEditing = NO;
        picker.delegate = self;
        
        [self presentViewController:picker animated:YES completion:NULL];

    }
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    UIImage *newImage = [UIImage compressImage:[info objectForKey:UIImagePickerControllerOriginalImage] size:CGSizeMake(_ibPersonalImageView.width, _ibPersonalImageView.height)];
    
    profileController.candidateProfile.personalDetail.avatarBase64String = [NSString encodeToBase64String:newImage];
    
    [self dismissViewControllerAnimated:YES completion:^{
        //UIImageView
        _ibPersonalImageView.image = newImage;
        _ibPersonalImageView.layer.cornerRadius = _ibPersonalImageView.width/2;
        _ibPersonalImageView.layer.masksToBounds = YES;
        _ibPersonalImageView.layer.borderWidth = 10.0f;
        _ibPersonalImageView.layer.borderColor = kColorCircleImageView.CGColor;
        _ibPersonalImageView.backgroundColor = [UIColor clearColor];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ProfileDetails"]) {
        profileController = [segue destinationViewController];        
    }
}

- (IBAction)saveProfile:(id)sender {
    NSLog(@"Save Profile");
    Profile *profile = [profileController getUpdatedProfile];
    
    if (profile != nil)
    {
        if ([NSString isEmptyString:profile.basicInfo.email] || ![NSString isValidEmail:profile.basicInfo.email]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageEmailIsNotCorrect delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        profile.basicInfo.first_name = [profile.basicInfo.first_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        profile.basicInfo.last_name = [profile.basicInfo.last_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        // full name
        if ([NSString isEmptyString:profile.basicInfo.first_name]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageFullnameIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // location
        if ([NSString isEmptyString:profile.personalDetail.currentLocation]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageLocationIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // phone
        if ([NSString isEmptyString:profile.personalDetail.phoneNumber] || ![NSString validPhone:profile.personalDetail.phoneNumber]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageMobileInvalid delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // day of birth
        if ( [NSString isEmptyString:profile.personalDetail.dateOfBirth] || ![NSDate isValidDateOfBirth:profile.personalDetail.dateOfBirth]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageBirthInvalid delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // gender
        if ([NSString isEmptyString:profile.personalDetail.gender]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageGenderInvalid delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // job type
        if ([NSString isEmptyString:profile.professionDetail.jobType]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageJobtypeIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // exp year
        if ([NSString isEmptyString:profile.professionDetail.experienceYears] || ![NSString validExYear:profile.professionDetail.experienceYears]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageExpYearIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // exp month
        if ([NSString isEmptyString:profile.professionDetail.experienceMonths] || ![NSString validExMonth:profile.professionDetail.experienceMonths]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageExpMonthIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // currentIndustry
        if ([NSString isEmptyString:profile.professionDetail.currentIndustry]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageIndustryIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // function
        if ([NSString isEmptyString:profile.professionDetail.function]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageFunctionIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        
        // keySkills
        if ([NSString isEmptyString:profile.professionDetail.keySkills]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageKeySkillIsEmpty delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }

        
        id avarta = profile.personalDetail.avatarBase64String;
        
        NSDictionary *params = nil;
        if (avarta) {
            params =  @{
                        kDictEmail:(profile.basicInfo.email?profile.basicInfo.email : @""),
                        kDictFirstName:(profile.basicInfo.first_name?profile.basicInfo.first_name : @""),
                        kDictLastName:(profile.basicInfo.last_name?profile.basicInfo.last_name : @""),
                        kDictNationality: (profile.personalDetail.nationality?profile.personalDetail.nationality : @""),
                        kDictCurrentLocation: (profile.personalDetail.currentLocation?profile.personalDetail.currentLocation : @""),
                        kDictPhoneNumber:(profile.personalDetail.phoneNumber?profile.personalDetail.phoneNumber : @""),
                        kDictDateOfBirth:(profile.personalDetail.dateOfBirth?profile.personalDetail.dateOfBirth : @""),
                        kDictGender: ([profile.personalDetail.gender isEqual:@"0"] ? @NO : @YES),
                        kDictJobType: (profile.professionDetail.jobType ? profile.professionDetail.jobType : @""),
                        kDictExperienceYear:(profile.professionDetail.experienceYears?profile.professionDetail.experienceYears : @"0"),
                        kDictExperienceMonth:(profile.professionDetail.experienceMonths?profile.professionDetail.experienceMonths : @"0"),
                        kDictCurrentIndustry:(profile.professionDetail.currentIndustry? profile.professionDetail.currentIndustry : @"0"),
                        kDictFunctionId:(profile.professionDetail.function? profile.professionDetail.function : @"0"),
                        kDictKeySkills:(profile.professionDetail.keySkills ? profile.professionDetail.keySkills : @""),
                        kDictNotice : @(profile.preference.notice),
                        kDictSubscribe : @(profile.preference.subscribe),
                        kDictAvatarContentBase64String:profile.personalDetail.avatarBase64String,
                        kDictAvatarName:@"MyAvarta"
                        };
        }else{
            params =  @{
                        kDictEmail:(profile.basicInfo.email?profile.basicInfo.email : @""),
                        kDictFirstName:(profile.basicInfo.first_name?profile.basicInfo.first_name : @""),
                        kDictLastName:(profile.basicInfo.last_name?profile.basicInfo.last_name : @""),
                        kDictNationality: (profile.personalDetail.nationality?profile.personalDetail.nationality : @""),
                        kDictCurrentLocation: (profile.personalDetail.currentLocation?profile.personalDetail.currentLocation : @""),
                        kDictPhoneNumber:(profile.personalDetail.phoneNumber?profile.personalDetail.phoneNumber : @""),
                        kDictDateOfBirth:(profile.personalDetail.dateOfBirth?profile.personalDetail.dateOfBirth : @""),
                        kDictGender: ([profile.personalDetail.gender isEqual:@"0"] ? @NO : @YES),
                        kDictJobType: (profile.professionDetail.jobType ? profile.professionDetail.jobType : @""),
                        kDictExperienceYear:(profile.professionDetail.experienceYears?profile.professionDetail.experienceYears : @"0"),
                        kDictExperienceMonth:(profile.professionDetail.experienceMonths?profile.professionDetail.experienceMonths : @"0"),
                        kDictCurrentIndustry:(profile.professionDetail.currentIndustry? profile.professionDetail.currentIndustry : @"0"),
                        kDictFunctionId:(profile.professionDetail.function? profile.professionDetail.function : @"0"),
                        kDictKeySkills:(profile.professionDetail.keySkills ? profile.professionDetail.keySkills : @""),
                        kDictNotice : @(profile.preference.notice),
                        kDictSubscribe : @(profile.preference.subscribe),
                        };
        }
        
        [IndicatorViewHelper showHUDInWindow:YES];
        
        NSLog(@">>>>>>>>>>>> updateCandidateProfile Begin: %@",params);
        
        [[APIManager sharedManager] updateCandidateProfile:params completion:^(BOOL success, NSString *message, NSString *avatarURL) {
            if (!success) {
                // Fail
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageUpdateProfile delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                [IndicatorViewHelper hideAllHUDForWindow:YES];
            }else {
                // Success
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KSuccessMessageUpdateProfile delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                
                if (![avatarURL isEqual:[NSNull null]] && avatarURL!=nil) {
                    // Update Avatar
                    profile.personalDetail.avatarURL = [NSURL URLWithString:avatarURL];
                }
                
                // Update Profile on NSDefaults
                [NSUserDefaults updateProfile:profile key:kDictProfile];
                
                [IndicatorViewHelper hideAllHUDForWindow:YES];
            }
        }];
    }
}

@end
