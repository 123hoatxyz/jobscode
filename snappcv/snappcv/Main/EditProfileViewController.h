//
//  EditProfileViewController.h
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *ibSaveButton;
@property (weak, nonatomic) IBOutlet UIImageView *ibPersonalImageView;


@end
