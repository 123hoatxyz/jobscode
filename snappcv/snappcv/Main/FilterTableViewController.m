//
//  FilterTableViewController.m
//  snappcv
//
//  Created by Hoat Ha Van on 7/8/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "FilterTableViewController.h"
#import "RecommendedJobTableViewCell.h"
#import "JobDetailViewController.h"
#import "ApplyJobViewController.h"
#import "IndicatorViewHelper.h"
#import "Constants.h"
#import "APIManager.h"

@interface FilterTableViewController()<UITableViewDataSource, UITableViewDelegate, RecommendedJobTableViewCellDelegate>
@property (strong, nonatomic) NSArray *arrRecommJobs;
@end

static NSString *kRecommendedJobTableViewCell =@"RecommendedJobTableViewCell";
#define kHEIGHT_TABLE_CELL  100.0

@implementation FilterTableViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    _arrRecommJobs = [[NSArray alloc] init];
    [_ibRecommJobTableView registerNib:[UINib nibWithNibName:kRecommendedJobTableViewCell bundle:nil] forCellReuseIdentifier:kRecommendedJobTableViewCell];
}
-(void)clearDataTable{
    _arrRecommJobs =  nil;
    _arrRecommJobs = [[NSMutableArray alloc] init];
    [_ibRecommJobTableView reloadData];
}
-(void)reloadDataTable{
    NSLog(@"Reload data table search");
    _ibRecommJobTableView.dataSource = self;
    _ibRecommJobTableView.delegate = self;
    [IndicatorViewHelper showHUDInWindow:YES];

    
    [[APIManager sharedManager] getListJobs:_params completion:^(id result, BOOL success, NSString *message) {
        if (success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _arrRecommJobs =  [NSArray arrayWithArray:result];
                
                [_ibRecommJobTableView reloadData];
                [IndicatorViewHelper hideAllHUDForWindow:YES];
            });
        }
    }];
}

#pragma mark - UITableView's Datasource and Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrRecommJobs count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kHEIGHT_TABLE_CELL;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommendedJobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRecommendedJobTableViewCell];
    [cell setCellFilterInfo:[_arrRecommJobs objectAtIndex:indexPath.row] highlightedText:_stringHighlighted];
   
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    JobDetailViewController *jobDetailsVC = (id)[self.storyboard instantiateViewControllerWithIdentifier:kJobDetailViewController];
    jobDetailsVC.aJob =  [_arrRecommJobs objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:jobDetailsVC animated:YES];
    //[self.mm_drawerController setCenterViewController:jobDetailsVC withFullCloseAnimation:YES completion:nil];
}

#pragma mark RecommendedJobTableViewCellDelegate
-(void)didTouchApplyButton:(RecommendedJobTableViewCell *)cell{
    
    //[UIApplication setChangedCenter:YES];
    NSIndexPath *indexPath = [_ibRecommJobTableView indexPathForCell:cell];
    ApplyJobViewController* applyJobVC = [self.storyboard instantiateViewControllerWithIdentifier:kApplyJobViewController];
    applyJobVC.aJob = [_arrRecommJobs objectAtIndex:indexPath.row];
    //    [self.mm_drawerController setCenterViewController:applyJobVC withFullCloseAnimation:YES completion:nil];
    [self.navigationController pushViewController:applyJobVC animated:YES];
}

-(void)highlightedText:(NSString *)text{
    _stringHighlighted = text;
    [self.ibRecommJobTableView reloadData];
}

#pragma mark - UITextField Search's Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
   // [self.view endEditing:YES];
    [_ibSearchField resignFirstResponder];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        //[self.view endEditing:YES];
         [_ibSearchField resignFirstResponder];
    }
}
@end
