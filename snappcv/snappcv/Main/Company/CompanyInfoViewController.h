//
//  CompanyInfoViewController.h
//  snappcv
//
//  Created by Staff on 3/20/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *ibZipCodeField;
@property (weak, nonatomic) IBOutlet UITextField *ibCompanyURL;
@property (weak, nonatomic) IBOutlet UIScrollView *ibContentScrollView;
@property (weak, nonatomic) IBOutlet UIView *ibContentView;

@end
