

//
//  CompanyInfoViewController.m
//  snappcv
//
//  Created by Staff on 3/20/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "CompanyInfoViewController.h"
#import "Constants.h"
#import "UIView+Utils.h"

#define kHEIGHT_SCROLLVIEW      460
@interface CompanyInfoViewController ()<UITextFieldDelegate>{
    UITextField *activeField;
}
@end

@implementation CompanyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma - Methods
-(void)setUp{
    //Placeholder's color
    if ([_ibZipCodeField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = kColorPlaceHolder;
        _ibZipCodeField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleZipcode attributes:@{NSForegroundColorAttributeName: color}];
        _ibCompanyURL.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleCompanyURL attributes:@{NSForegroundColorAttributeName: color}];
        
    }
    _ibZipCodeField.textColor = kColorPlaceHolder;
    _ibCompanyURL.textColor = kColorPlaceHolder;
    
    //Pointer left
    _ibZipCodeField.leftView = kTextFieldLeftView;
    _ibZipCodeField.leftViewMode = UITextFieldViewModeAlways;
    _ibCompanyURL.leftView = kTextFieldLeftView;
    _ibCompanyURL.leftViewMode = UITextFieldViewModeAlways;
    _ibCompanyURL.delegate = self;
    _ibZipCodeField.delegate = self;
    //Scrollviwe
    if (isDeviceiPhone35Inch) {
        _ibContentView.height = kHEIGHT_SCROLLVIEW;
        _ibContentScrollView.contentSize =CGSizeMake([UIView widthDevice], kHEIGHT_SCROLLVIEW);
    }
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _ibContentScrollView.contentInset = contentInsets;
    _ibContentScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [_ibContentScrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _ibContentScrollView.contentInset = contentInsets;
    _ibContentScrollView.scrollIndicatorInsets = contentInsets;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - UITextField's Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    activeField = textField;
}

@end
