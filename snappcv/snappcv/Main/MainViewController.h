//
//  MainViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"

@interface MainViewController : MMDrawerController

@end
