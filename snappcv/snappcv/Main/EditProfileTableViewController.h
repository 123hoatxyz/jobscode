//
//  EditProfileViewController.h
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"

@interface EditProfileTableViewController : UITableViewController
@property (nonatomic) Profile *candidateProfile;
@property (weak, nonatomic) IBOutlet UITextField *ibFullnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibEmailTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibCurrentLocationTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibMobileNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibJobTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibExpYearsTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibExpMonthsTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibKeySkillsTextField;

@property (weak, nonatomic) IBOutlet UIButton *ibPickDateOfBirthButton;
@property (weak, nonatomic) IBOutlet UIButton *ibPickGenderButton;
@property (weak, nonatomic) IBOutlet UIButton *ibPickIndustryButton;
@property (weak, nonatomic) IBOutlet UIButton *ibPickFunctionButton;
@property (weak, nonatomic) IBOutlet UIButton *ibPickNationButton;
@property (weak, nonatomic) IBOutlet UIButton *ibNotice;
@property (weak, nonatomic) IBOutlet UIButton *ibSubscribe;

-(Profile *)getUpdatedProfile;
@end
