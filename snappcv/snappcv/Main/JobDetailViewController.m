//
//  JobDetailViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "JobDetailViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "Constants.h"
#import "TTTAttributedLabel.h"
#import "UIApplication+Utils.h"
#import "UIImageView+AFNetworking.h"
#import "ApplyJobViewController.h"
#import "APIManager.h"
#import "NSDate+Utils.h"


@interface JobDetailViewController ()

@end

@implementation JobDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_ibJobImageView.layer setBorderColor:[kColorBorderImageView CGColor]];
    [_ibJobImageView.layer setBorderWidth:1.5];
    [_ibRatingButton setImage:[UIImage imageNamed:@"star_fav"] forState:UIControlStateSelected];
     _ibTitleLabel.font = kFontDefaultForTitle;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setCellInfo:_aJob];
    
    //    NSString *format = @"<html><body><p align=\"justify\"> <font size=\"4\" color=\"#494949\" face=\"HelveticaNeue\">%@ </font> </p> </body></html>";
    //    NSString *htmlString = [NSString stringWithFormat:format,_ibTopTextView.text];
    //    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:
    //                                            [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
    //                                                                            options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    //    _ibTopTextView.attributedText = attributedString;
    //
    //    htmlString = [NSString stringWithFormat:format,_ibBottomTextView.text];
    //    attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    //    _ibBottomTextView.attributedText = attributedString;
    
}


- (IBAction)pressFavourite:(UIButton*)sender {
    BOOL isFavourite = !_ibRatingButton.selected;
    id params = @{kDictJobID :[NSNumber numberWithInteger:_aJob.job_id], kDictJobIsFavourite:@(isFavourite)};
    [[APIManager sharedManager] favouriteJob:params completion:^(id result, BOOL success, NSString *message) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _ibRatingButton.selected = !_ibRatingButton.selected;
            });
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backButtonDidTouch:(id)sender {
    /*
    [UIApplication setChangedCenter:NO];
    
    id centerDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CenterNavControllerID"];
    self.mm_drawerController.centerViewController = centerDrawerViewController;
    */
    if ([UIApplication isChangedCenter]) {
        [UIApplication setChangedCenter:NO];
        [UIView animateWithDuration:0.1 delay:0.1 options:UIViewAnimationOptionCurveLinear animations:^{
            id centerDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CenterNavControllerID"];
            self.mm_drawerController.centerViewController = centerDrawerViewController;
        } completion:^(BOOL finished) {
            
        }];
        
    }else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)setCellInfo:(Job*)info{
    
    _ibApplyJobButton.enabled = info.is_applied?NO:YES;
    [_ibApplyJobButton setTitle:_aJob.is_applied?@"Applied":@"Apply for this job" forState:UIControlStateNormal];
    _ibApplyJobButton.backgroundColor = info.is_applied?kColorRecommJobDidApply : kColorRecommJobNotApply;
    
    [_ibJobImageView setImageWithURL:info.company_logo placeholderImage:nil];
    
    [self.ibExperienceYearLabel setText:[NSString stringWithFormat:@"%ld Years of Exp.", (long)info.experience]
afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
    
    NSRange termsRange = [[mutableAttributedString string] rangeOfString:kTitleExperiencePart options:NSCaseInsensitiveSearch];
    
    UIFont *regularSystemFont = [UIFont fontWithName:@"Helvetica-Nueue" size:self.ibExperienceYearLabel.font.pointSize];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)regularSystemFont.fontName, regularSystemFont.pointSize, NULL);
    if (font) {
        [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorExperiencePart.CGColor range:termsRange];
        CFRelease(font);
    }
    return mutableAttributedString;
}];
    
    _ibRatingButton.selected = info.is_favorite;
    _ibJobTitleLabel.text = info.job_title;
    _ibCompanyNameLabel.text = info.company_name;
    _ibCompanyLocationLabel.text = info.company_address;
    _ibBottomTextView.text = info.job_description;
    _ibTopTextView.text = info.job_overview;
    _ibPostTimeLabel.text = [NSDate titleDateFromNumber:info.posted_date];
    
}

- (IBAction)gotoApplyJob:(id)sender {
    [UIApplication setChangedCenter:NO];
    ApplyJobViewController *applyJob = [self.storyboard instantiateViewControllerWithIdentifier:kApplyJobViewController];
    applyJob.aJob = _aJob;
    [self.navigationController pushViewController:applyJob animated:YES];
}
@end
