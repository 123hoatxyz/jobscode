//
//  MenuViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *ibSearchJobButton;
@property (weak, nonatomic) IBOutlet UIButton *ibRecommendJobButton;
@property (weak, nonatomic) IBOutlet UIButton *ibFavouriteButton;
@property (weak, nonatomic) IBOutlet UIButton *ibApplicationHistoryButton;
@property (weak, nonatomic) IBOutlet UIButton *ibSettingButton;
@property (weak, nonatomic) IBOutlet UIButton *ibCVWritingButton;
@property (weak, nonatomic) IBOutlet UIButton *ibHelpButton;
@property (weak, nonatomic) IBOutlet UIButton *ibAboutButton;
@property (weak, nonatomic) IBOutlet UIButton *ibRateButton;
@property (weak, nonatomic) IBOutlet UIButton *ibLogoutButton;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *ibMenuScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *ibProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *ibCVTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibFullnameLabel;
@end
