//
//  JobsPackViewController.h
//  snappcv
//
//  Created by Staff on 3/12/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobsPackViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *ibJobsPackTableView;
@end
