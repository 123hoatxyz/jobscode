//
//  RecommededJobsViewController.h
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum{
    RecommededJobsNone  = 0,
    RecommededJobsShowingResults = 1
}RecommededJobsType;

@interface RecommededJobsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *ibRecommJobTableView;
@property (assign,nonatomic) RecommededJobsType recommendJobsType;
@property (weak, nonatomic) IBOutlet UILabel *ibNoResultLabel;


@end
