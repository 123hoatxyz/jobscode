//
//  FavoritesJobsViewController.m
//  snappcv
//
//  Created by Staff on 4/10/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "FavoritesJobsViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "JobDetailViewController.h"
#import "RecommendedJobTableViewCell.h"
#import "UIApplication+Utils.h"
#import "ApplyJobViewController.h"
#import "APIManager.h"
#import "Constants.h"
#import "UIView+Utils.h"

static NSString *kRecommendedJobTableViewCell =@"RecommendedJobTableViewCell";
#define kHEIGHT_TABLE_CELL  100.0

@interface FavoritesJobsViewController()<RecommendedJobTableViewCellDelegate>
@property (strong, nonatomic) NSArray *arrFavoritesJobs;
@end

@implementation FavoritesJobsViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    _ibTitleLabel.font = kFontDefaultForTitle;
    [_ibFavoritesTableView registerNib:[UINib nibWithNibName:kRecommendedJobTableViewCell bundle:nil] forCellReuseIdentifier:kRecommendedJobTableViewCell];
    _ibFavoritesTableView.dataSource = self;
    _ibFavoritesTableView.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [[APIManager sharedManager] getListJobs:@{kDictJobIsFavourite:@(YES)} completion:^(id result, BOOL success, NSString *message) {
        if (success) {
            _arrFavoritesJobs =  [NSArray arrayWithArray:result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                BOOL isScrollOver = ([_arrFavoritesJobs count]*kHEIGHT_TABLE_CELL) > _ibFavoritesTableView.height ? YES : NO;
                _ibFavoritesTableView.scrollEnabled = isScrollOver;
                [_ibFavoritesTableView reloadData];
            });
        }
    }];
    
}


#pragma mark UITableview Datasource and Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrFavoritesJobs count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kHEIGHT_TABLE_CELL;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommendedJobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRecommendedJobTableViewCell];
    [cell setCellInfo:[_arrFavoritesJobs objectAtIndex:indexPath.row]];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JobDetailViewController *jobDetailsVC = (id)[self.storyboard instantiateViewControllerWithIdentifier:kJobDetailViewController];
    jobDetailsVC.aJob =  [_arrFavoritesJobs objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:jobDetailsVC animated:YES];
}

- (IBAction)gotoMenu:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark RecommendedJobTableViewCellDelegate
-(void)didTouchApplyButton:(RecommendedJobTableViewCell *)cell{
    NSLog(@"Apply from Favorite");
    [UIApplication setChangedCenter:FALSE];
    NSIndexPath *indexPath = [_ibFavoritesTableView indexPathForCell:cell];
    ApplyJobViewController* applyJobVC = [self.storyboard instantiateViewControllerWithIdentifier:kApplyJobViewController];
    applyJobVC.aJob = [_arrFavoritesJobs objectAtIndex:indexPath.row];
    //    [self.mm_drawerController setCenterViewController:applyJobVC withFullCloseAnimation:YES completion:nil];
    [self.navigationController pushViewController:applyJobVC animated:YES];
}

@end
