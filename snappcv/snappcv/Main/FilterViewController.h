//
//  FilterViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterViewController : UIViewController
@property (assign, nonatomic) NSInteger openSectionIndex;
@property (weak, nonatomic) IBOutlet UITableView *ibJobsFilterTableView;
@property (weak, nonatomic) IBOutlet UITextField *ibSearchField;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *ibTableSearch;
@property (weak, nonatomic) IBOutlet UIView *ibOptionsFilterView;
@end
