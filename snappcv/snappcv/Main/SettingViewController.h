//
//  SettingViewController.h
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *ibSettingTableView;

@end
