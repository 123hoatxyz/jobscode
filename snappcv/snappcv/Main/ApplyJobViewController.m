//
//  ApplyJobViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "ApplyJobViewController.h"
#import "JobDetailViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "Constants.h"
#import "TTTAttributedLabel.h"
#import "ErrorMessage.h"
#import "UIApplication+Utils.h"
#import "UIImageView+AFNetworking.h"
#import "APIManager.h"
#import "Job.h"
#import "NSString+Utils.h"
#import "UIView+Utils.h"
#import "IndicatorViewHelper.h"
#import "NSDate+Utils.h"

#import <DBChooser/DBChooser.h>
#import <DBChooser/DBChooserResult.h>

@import QuartzCore;
@import AssetsLibrary;
@import MediaPlayer;
@import MobileCoreServices;
@class JobDetailViewController;

@interface ApplyJobViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIAlertViewDelegate>{
    NSData *resumeData;
    NSData *videoData;
    NSString *videoPath;
    NSURL *resumeUrl;
}

@property (nonatomic, assign) NSUInteger timeLeft;
@property (nonatomic, assign) BOOL isMultipleTouch;
@end

enum {
    DBCLinkTypeRowIndexPreview = 0,
    DBCLinkTypeRowIndexDirect,
    DBCLinkTypeRowCount,
} DBCLinkTypeRowIndex;

@implementation ApplyJobViewController
{
    DBChooserResult *_result; // result received from last CHooser call
    NSUInteger _linkTypeIndex;
}
@synthesize isMultipleTouch;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIApplication setOpenDropbox:NO];
    [_ibCVCheckedIcon setHidden:YES];
    [_ibCVLabel setHidden:YES];
    
    [_ibVideoCheckedIcon setHidden:YES];
    [_ibVideoIntroduceLabel setHidden:YES];
    
    [_ibJobImageView.layer setBorderColor:[kColorBorderImageView CGColor]];
    [_ibJobImageView.layer setBorderWidth:1.5];
    [_ibRatingButton setImage:[UIImage imageNamed:@"star_fav"] forState:UIControlStateSelected];
    _ibTitleLabel.font = kFontDefaultForTitle;
    _ibSubmitButton.backgroundColor = _aJob.is_applied?kColorRecommJobDidApply : kColorApplyJobNotApply;
    if (isDeviceiPhone35Inch) {
        _ibContentView.height = 387.0;
        _ibContentScrollView.contentSize = CGSizeMake(_ibContentScrollView.width, _ibContentView.height);
        
    }    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [UIView view:_ibCVLabel updateCenter:_ibCVCheckedIcon.center];
    [UIView view:_ibVideoIntroduceLabel updateCenter:_ibVideoCheckedIcon.center];
    [self setCellInfo:_aJob];
    isMultipleTouch = NO;
    [self setExclusiveTouchForButtons:self.view];
}

-(void)setExclusiveTouchForButtons:(UIView *)myView
{
    for (UIView * v in [myView subviews]) {
        if([v isKindOfClass:[UIButton class]])
            [((UIButton *)v) setExclusiveTouch:YES];
        else if ([v isKindOfClass:[UIView class]]){
            [self setExclusiveTouchForButtons:v];
        }
    }
}

-(void)setCellInfo:(Job*)info{
    
    [_ibJobImageView setImageWithURL:info.company_logo placeholderImage:nil];
    
    [self.ibExperienceYearLabel setText:[NSString stringWithFormat:@"%ld Years of Exp.", (long)info.experience]
afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString){
    
    NSRange termsRange = [[mutableAttributedString string] rangeOfString:kTitleExperiencePart options:NSCaseInsensitiveSearch];
    
    UIFont *regularSystemFont = [UIFont fontWithName:@"Helvetica-Nueue" size:self.ibExperienceYearLabel.font.pointSize];
    CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)regularSystemFont.fontName, regularSystemFont.pointSize, NULL);
    if (font) {
        [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)font range:termsRange];
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(__bridge id)kColorExperiencePart.CGColor range:termsRange];
        CFRelease(font);
    }
    return mutableAttributedString;
}];
    
    _ibRatingButton.selected = info.is_favorite;
    _ibJobTitleLabel.text = info.job_title;
    _ibCompanyNameLabel.text = info.company_name;
    _ibCompanyLocationLabel.text = info.company_address;
    _ibPostTimeLabel.text = [NSDate titleDateFromNumber:info.posted_date];

    
}


- (IBAction)pressFavourite:(UIButton*)sender {
    BOOL isFavourite = !_ibRatingButton.selected;
    id params = @{kDictJobID :[NSNumber numberWithInteger:_aJob.job_id], kDictJobIsFavourite:@(isFavourite)};
    [[APIManager sharedManager] favouriteJob:params completion:^(id result, BOOL success, NSString *message) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _ibRatingButton.selected = !_ibRatingButton.selected;
                _aJob.is_favorite = _ibRatingButton.selected;
            });
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backButtonDidTouch:(id)sender {
    
    [self goBackPreviousScreen];
}

-(void)goBackPreviousScreen{
    if ([UIApplication isChangedCenter]) {
        [UIApplication setChangedCenter:NO];
        [UIView animateWithDuration:0.1 delay:0.1 options:UIViewAnimationOptionCurveLinear animations:^{
            id centerDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CenterNavControllerID"];
            self.mm_drawerController.centerViewController = centerDrawerViewController;
        } completion:^(BOOL finished) {
            
        }];
        
    }else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)recordIntroduceVideo:(id)sender {
    if (isMultipleTouch) {
        return;
    }
    isMultipleTouch = YES;
    [self startCameraControllerFromViewController: self
                                    usingDelegate: self];
}

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose movie capture
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    
    cameraUI.delegate = delegate;
    cameraUI.videoMaximumDuration = 60.0f;
    
    [controller presentViewController:cameraUI animated:YES completion:^{
        isMultipleTouch = NO;
    }];
    
    return YES;
}

// For responding to the user accepting a newly-captured picture or movie
- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:YES completion:^{
        // Handle a movie capture
        if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0)
            == kCFCompareEqualTo) {
            
            videoPath = (id)[[info objectForKey:UIImagePickerControllerMediaURL] path];
            [_ibVideoCheckedIcon setHidden:NO];
            [_ibVideoIntroduceLabel setHidden:NO];
            _ibVideoIntroduceLabel.text = [videoPath lastPathComponent];
            [_ibVideoIntroduceLabel sizeToFit];
            
            [UIView view:_ibVideoIntroduceLabel updateCenter:_ibVideoCheckedIcon.center];
            _ibVideoCheckedIcon.right = _ibVideoIntroduceLabel.left-5.0;
            //            CGPoint center = _ibVideoIntroduceLabel.center;
            //            center.y = _ibVideoCheckedIcon.center.y;
            //            _ibVideoIntroduceLabel.center = center;
            //            NSString *moviePath = (id)[[info objectForKey:UIImagePickerControllerMediaURL] path];
            //            videoPath = moviePath;
            //            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
            //                UISaveVideoAtPathToSavedPhotosAlbum (moviePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
            //            }
            
            videoData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:videoPath]];
            
            
        }
    }];
    
}

- (void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:kErrorMessageSaveVideo  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:kAlertMessageRecordVideoSuccessfully  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}
- (IBAction)submitApplication:(id)sender {
    if (isMultipleTouch) {
        [IndicatorViewHelper hideAllHUDForWindow:YES];
        return;
    }
    if (resumeUrl == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageCvRequire delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [IndicatorViewHelper showHUDInWindow:YES];
    NSDictionary* params = @{ kDictJobID:@(_aJob.job_id), kDictJobIsApplied: @(YES) };
    //NSString *paths = [[NSBundle mainBundle] pathForResource:@"cv-template" ofType:@"pdf"];
    resumeData = [NSData dataWithContentsOfURL:resumeUrl];
    
    __block NSDictionary* dictResume = nil;
    if (resumeData) {
        dictResume = @{kDictFileMyCV:resumeData,kDictFileName:[[resumeUrl lastPathComponent] lowercaseString], kDictFileMineType:[NSString mimeTypeFromString:[resumeUrl pathExtension]]};
    }
    
    if (![NSString isEmptyString:videoPath]) {
        
        NSDictionary *dictVideo = nil;
        if (videoData.length) {
            dictVideo = @{kDictFilefileMyIntro:videoData,kDictFileName:[videoPath lastPathComponent], kDictFileMineType:[NSString mimeTypeFromString:[videoPath lastPathComponent]]};
        }
        [[APIManager sharedManager] submitApplicationJob:params dataCV:dictResume dataVideo:dictVideo completion:^(id result, BOOL success, NSString *message) {
            if (success) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KSuccessMessageSubmitApplication delegate:self cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil, nil];
                alertView.tag = 9999;
                [alertView show];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageSubmitApplication delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [IndicatorViewHelper hideAllHUDForWindow:YES];
            });
        }];
        
    }else{
        
        [[APIManager sharedManager] submitApplicationJob:params dataCV:dictResume dataVideo:nil completion:^(id result, BOOL success, NSString *message) {
            if (success) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KSuccessMessageSubmitApplication delegate:self cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil, nil];
                alertView.tag = 9999;
                [alertView show];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageSubmitApplication delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [IndicatorViewHelper hideAllHUDForWindow:YES];
            });
        }];
    }
}

- (IBAction)doAttachCV:(id)sender {
    DBChooserLinkType linkType = (_linkTypeIndex == DBCLinkTypeRowIndexDirect) ?
    DBChooserLinkTypeDirect : DBChooserLinkTypePreview;
    [UIApplication setOpenDropbox:YES];
    NSLog(@"Open Dropbox");
    [[DBChooser defaultChooser] openChooserForLinkType:linkType fromViewController:self
                                            completion:^(NSArray *results)
     {
         NSLog(@"Open Dropbox >>>>>>>>>>>>>> Call back");
         [UIApplication setOpenDropbox:NO];
         if ([results count]) {
             _result = results[0];
             //if (![[[_result name] pathExtension] isEqualToString:@"doc"]) {
                 
             //}
             NSLog(@"link: %@",[[_result link] absoluteString]);
             NSLog(@"name: %@",[_result name]);
             NSLog(@"size: %lld", [_result size]);
             
             // Check CV size
             if ([_result size] >= 5000) {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageCv5Mb delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
                 return;
             }
             
             [_ibCVCheckedIcon setHidden:NO];
             [_ibCVLabel setHidden:NO];
             _ibCVLabel.text = [[_result name] lowercaseString];

             [IndicatorViewHelper showHUDInWindow:YES];
             [[APIManager sharedManager] downloadFile:[[_result link] absoluteString] completion:^(BOOL success, NSURL *url) {
                 if (success) {
                    NSLog(@"File downloaded to: %@", url);
                     resumeUrl = url;
                 }else{
                     NSLog(@"File download failed");
                     resumeUrl = nil;
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[[UIAlertView alloc] initWithTitle:nil message:KErrorMessageCvChooseFailed delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                     });

                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [IndicatorViewHelper hideAllHUDForWindow:YES];
                 });
             }];
         } else {
             _result = nil;
             [_ibCVCheckedIcon setHidden:YES];
             [_ibCVLabel setHidden:YES];
         }
         //[[self tableView] reloadData];
     }];
}

-(void)prepareForMediaUpload:(NSString*)path completion:(void(^)(NSData *avideoData))completion{
    
    NSURL *fileURL = [NSURL URLWithString:path];
    __block NSData* mediaData;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
        [assetsLibrary assetForURL:fileURL resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *rep = [asset defaultRepresentation];
            if (rep) {
                @try {
                    Byte *buffer =(Byte*)malloc((size_t)rep.size);
                    NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:(NSUInteger)rep.size error:nil];
                    mediaData = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
                }
                @catch (NSException *exception) {
                    mediaData = nil;
                    NSLog(@"Exception:%@", exception);
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (mediaData.length) {
                        completion(mediaData);
                    }else{
                        completion(nil);
                    }
                });
            }
        } failureBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                completion(nil);
                
            });
        }];
    });
}

#pragma mark - UIAlertView's Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 9999) {
        _aJob.is_applied = YES;
        
        _ibSubmitButton.enabled = _aJob.is_applied?NO:YES;
        _ibSubmitButton.backgroundColor = _aJob.is_applied?kColorRecommJobDidApply : kColorLocation;
        [_ibSubmitButton setTitle:_aJob.is_applied?@"Application Applied":@"Submit Application" forState:UIControlStateNormal];
        
        // delete resume file
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtURL:resumeUrl error: &error];
        if (error){
            NSLog(@"Delete temp resume file error: %@", error);
        }else{
            resumeUrl = nil;
            NSLog(@"Delete temp resume complete");
        }
        
        
        //[self goBackPreviousScreen];
    }
}

@end
