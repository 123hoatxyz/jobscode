//
//  FavoritesJobsViewController.h
//  snappcv
//
//  Created by Staff on 4/10/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesJobsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *ibFavoritesTableView;

@end
