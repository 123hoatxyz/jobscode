//
//  RecommededJobsViewController.m
//  snappcv
//
//  Created by Staff on 3/2/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "RecommededJobsViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "RecommendedJobTableViewCell.h"
#import "JobDetailViewController.h"
#import "NotificationName.h"
#import "Constants.h"
#import "UIApplication+Utils.h"
#import "APIManager.h"
#import "UIView+Utils.h"
#import "ApplyJobViewController.h"
#import "IndicatorViewHelper.h"
#import "NSUserDefaults+Utils.h"
#import "RecommendJobsShared.h"

static NSString *kRecommendedJobTableViewCell =@"RecommendedJobTableViewCell";

#define kHEIGHT_TABLE_CELL  100.0
@interface RecommededJobsViewController ()<UITableViewDataSource, UITableViewDelegate, RecommendedJobTableViewCellDelegate>
@property (strong, nonatomic) NSArray *arrRecommJobs;
@end

@implementation RecommededJobsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_ibRecommJobTableView registerNib:[UINib nibWithNibName:kRecommendedJobTableViewCell bundle:nil] forCellReuseIdentifier:kRecommendedJobTableViewCell];
    //Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showingJobsSearchResult:) name:kNotifyShowingJobsSearchResultScreen object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showingRecommendedJobsDefault) name:kNotifykNotifyShowingRecommendJobsDefault object:nil];
    
    _recommendJobsType = RecommededJobsNone;
    _ibTitleLabel.font = kFontDefaultForTitle;
    //hidden
    _ibNoResultLabel.hidden = YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IndicatorViewHelper showHUDInWindow:YES];
    [[APIManager sharedManager] getListJobs:nil completion:^(id result, BOOL success, NSString *message) {
        if (success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _arrRecommJobs =  [NSArray arrayWithArray:result];
                [RecommendJobsShared defaultShared].arrRecommendJobs = [NSArray arrayWithArray:_arrRecommJobs];
                [[RecommendJobsShared defaultShared] arrayDictionaryMatchedLocation];
                BOOL isScrollOver = ([_arrRecommJobs count]*kHEIGHT_TABLE_CELL) > _ibRecommJobTableView.height ? YES : NO;
                _ibRecommJobTableView.scrollEnabled = isScrollOver;
                [_ibRecommJobTableView reloadData];
                [IndicatorViewHelper hideAllHUDForWindow:YES];
                
                 _ibNoResultLabel.hidden = [_arrRecommJobs count] == 0? NO :YES;
            });
        }
    }];
}

-(void)showingRecommendedJobsDefault{
    _ibTitleLabel.text = @"Recommended Jobs";
}
-(void)showingJobsSearchResult:(NSNotification*)notify{
    NSDictionary *params = [NSUserDefaults getSearchParams];
    
    NSLog(@"getSearchParams: %@", params);
    
    [IndicatorViewHelper showHUDInWindow:YES];
    [[APIManager sharedManager] getListJobs:params completion:^(id result, BOOL success, NSString *message) {
        if (success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _arrRecommJobs =  [NSArray arrayWithArray:result];
                
                [RecommendJobsShared defaultShared].arrRecommendJobs = [NSArray arrayWithArray:_arrRecommJobs];
                
                BOOL isScrollOver = ([_arrRecommJobs count]*kHEIGHT_TABLE_CELL) > _ibRecommJobTableView.height ? YES : NO;
                
                _ibRecommJobTableView.scrollEnabled = isScrollOver;
                
                [_ibRecommJobTableView reloadData];
                
                _ibTitleLabel.text = [NSString stringWithFormat:@"%ld Jobs Found", (long)_arrRecommJobs.count];
                
                _ibNoResultLabel.hidden = [_arrRecommJobs count] == 0? NO :YES;
            });
        }
        
        [IndicatorViewHelper hideAllHUDForWindow:YES];
    }];
}


#pragma mark event
- (IBAction)leftDrawerButtonDidTouch:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (IBAction)rightDrawerButtonDidTouch:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}


#pragma mark UITableview Datasource and Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrRecommJobs count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kHEIGHT_TABLE_CELL;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommendedJobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRecommendedJobTableViewCell];
    [cell setCellInfo:[_arrRecommJobs objectAtIndex:indexPath.row]];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"fun:%s,line:%d,row:%ld", __FUNCTION__, __LINE__, (long)indexPath.row);

    JobDetailViewController *jobDetailsVC = (id)[self.storyboard instantiateViewControllerWithIdentifier:kJobDetailViewController];
    jobDetailsVC.aJob =  [_arrRecommJobs objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:jobDetailsVC animated:YES];
    //[self.mm_drawerController setCenterViewController:jobDetailsVC withFullCloseAnimation:YES completion:nil];

}
#pragma mark RecommendedJobTableViewCellDelegate
-(void)didTouchApplyButton:(RecommendedJobTableViewCell *)cell{
    
    [UIApplication setChangedCenter:YES];
    NSIndexPath *indexPath = [_ibRecommJobTableView indexPathForCell:cell];
    ApplyJobViewController* applyJobVC = [self.storyboard instantiateViewControllerWithIdentifier:kApplyJobViewController];
    applyJobVC.aJob = [_arrRecommJobs objectAtIndex:indexPath.row];
//    [self.mm_drawerController setCenterViewController:applyJobVC withFullCloseAnimation:YES completion:nil];
    [self.navigationController pushViewController:applyJobVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
