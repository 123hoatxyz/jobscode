//
//  MenuViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "MenuViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "Constants.h"
#import "NSNotificationCenter+Utils.h"
#import "NotificationName.h"
#import "RecommededJobsViewController.h"
#import "UIView+Utils.h"
#import "UIApplication+Utils.h"
#import "Profile.h"
#import "APIManager.h"
#import "NSUserDefaults+Utils.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+Addition.h"
#import "NSString+Utils.h"
#import "WebViewController.h"
#import "EditProfileViewController.h"

#define kHEIGHT_MENU_SCROLLVIEW     391

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpProfile];
    
    [self setUpButtonInset];
    
    _ibTitleLabel.font = kFontDefaultForTitle;
    
    UITapGestureRecognizer *tapGestureForProfile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProfile)];
    tapGestureForProfile.numberOfTapsRequired = 1;
    [_ibProfileImageView setUserInteractionEnabled:YES];
    [_ibProfileImageView addGestureRecognizer:tapGestureForProfile];
}

-(void) viewWillAppear: (BOOL) animated {
    [self setUpProfile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setUpProfile{
    
    
    Profile *profile = [NSUserDefaults getProfile:kDictProfile];
    if (profile.personalDetail.avatarURL || ![UIApplication getLogin]) {
        _ibProfileImageView.layer.cornerRadius = _ibProfileImageView.width/2;
        _ibProfileImageView.layer.masksToBounds = YES;
        _ibProfileImageView.layer.borderWidth = 5.0f;
        _ibProfileImageView.layer.borderColor = kColorCircleImageView.CGColor;
        if (![UIApplication getLogin]) {
            NSString *stringBase64Image = [NSUserDefaults avatarBase64String];
            
            _ibProfileImageView.image = [NSString isEmptyString:stringBase64Image]? _ibProfileImageView.image = [UIImage imageNamed:@"avatar"]:[UIImage decodeBase64StringToImage:stringBase64Image];
            
        }else {
            [_ibProfileImageView setImageWithURL:profile.personalDetail.avatarURL];
        }
    }else{
        _ibProfileImageView.image = [UIImage imageNamed:@"avatar"];
    }
    
    _ibFullnameLabel.text = [NSString stringWithFormat:@"%@ %@", profile.basicInfo.first_name, profile.basicInfo.last_name];
    _ibCVTitleLabel.text = [NSString getFunctionName:profile.professionDetail.function dict: [NSUserDefaults getFunctionList:kDictFunctionKey]];
}

- (void)setUpButtonInset{
    
    [_ibCVWritingButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibCVWritingButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibLogoutButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibLogoutButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];

    [_ibFavouriteButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibFavouriteButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibHelpButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibHelpButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibRateButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibRateButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibRecommendJobButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibRecommendJobButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibSearchJobButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibSearchJobButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibSettingButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibSettingButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibApplicationHistoryButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibApplicationHistoryButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    [_ibAboutButton setImageEdgeInsets:kEdgeInsetsImageMenu];
    [_ibAboutButton setTitleEdgeInsets:kEdgeInsetsTitleMenu];
    
    //set content size of scrollview for Device 3.5 inch
    if (isDeviceiPhone35Inch) {
        _ibMenuScrollView.contentSize = CGSizeMake([UIView widthDevice], kHEIGHT_MENU_SCROLLVIEW);
    }
    
}
- (IBAction)closeMenu:(id)sender {
    [self.mm_drawerController closeDrawerAnimated:YES completion:NULL];
    [NSNotificationCenter postNotificationName:kNotifykNotifyShowingRecommendJobsDefault];
}

- (IBAction)gotoRecomendJobs:(id)sender {
    
    [UIApplication setChangedCenter:NO];
    
    [self.mm_drawerController closeDrawerAnimated:YES completion:^(BOOL finished) {
        id centerDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CenterNavControllerID"];
        self.mm_drawerController.centerViewController = centerDrawerViewController;
        [NSNotificationCenter postNotificationName:kNotifykNotifyShowingRecommendJobsDefault];
    }];

}
- (IBAction)gotoSetupSearchJobs:(id)sender {
    id setupSearchJob = [self.storyboard instantiateViewControllerWithIdentifier:kSearchJobViewController];
    [self.navigationController pushViewController:setupSearchJob animated:YES];
}
- (IBAction)gotoFavoriteJobs:(id)sender {
    id favoriteJobs = [self.storyboard instantiateViewControllerWithIdentifier:kFavoritesJobsViewController];
    [ self.mm_drawerController.navigationController pushViewController:favoriteJobs animated:YES];
}

- (IBAction)gotoHistoryJob:(id)sender {
    id applicationHistory = [self.storyboard instantiateViewControllerWithIdentifier:kApplicationHistoryViewController];
    [self.mm_drawerController.navigationController pushViewController:applicationHistory animated:YES];
}
- (IBAction)gotoHelp:(id)sender {
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewController];
    webView.textTitle = @"Help";
    webView.urlAddress = kURLHelpPage;
    [self.mm_drawerController.navigationController pushViewController:webView animated:YES];
   
}
- (IBAction)gotoAboutUS:(id)sender {
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewController];
     webView.textTitle = @"About Us";
    webView.urlAddress = kURLAboutPage;
    [self.mm_drawerController.navigationController pushViewController:webView animated:YES];
}
- (IBAction)gotoRating:(id)sender {
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewController];
    webView.textTitle = @"Rate this App";
    webView.urlAddress = kURLRatePage;
   
    [self.mm_drawerController.navigationController pushViewController:webView animated:YES];
}
- (IBAction)gotoSetting:(id)sender {
    id setting= [self.storyboard instantiateViewControllerWithIdentifier:kSettingViewController];
   
    [self.mm_drawerController.navigationController pushViewController:setting animated:YES];
}
- (IBAction)gotoCVWritingHelp:(id)sender {
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:kWebViewController];
    webView.textTitle = @"CV Writing Help";
    webView.urlAddress = kURLCVPage;
    
    [self.mm_drawerController.navigationController pushViewController:webView animated:YES];
}
- (IBAction)doLogout:(id)sender {
    [[APIManager sharedManager] logoutUserWithCompletion:^(BOOL success, NSString *error) {
        [UIApplication setLogin:NO];
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        [delegate loadStoryboard:@"Register"];
    }];
}

-(void)openProfile{
    EditProfileViewController *editProfile = [self.storyboard instantiateViewControllerWithIdentifier:kEditProfileViewController];
    [self.mm_drawerController.navigationController pushViewController:editProfile animated:YES];
}

@end
