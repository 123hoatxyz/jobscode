//
//  ChangePasswordViewController.h
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *ibCloseButton;
@property (weak, nonatomic) IBOutlet UIButton *ibSendButton;
@property (weak, nonatomic) IBOutlet UITextField *ibCurrentPasswordText;
@property (weak, nonatomic) IBOutlet UITextField *ibNewPasswordText;
@property (weak, nonatomic) IBOutlet UITextField *ibConfirmPasswordText;
@property (weak, nonatomic) IBOutlet UIButton *bnabnmba;
@end
