//
//  EditProfileViewController.m
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "EditProfileTableViewController.h"
#import "Constants.h"
#import "NSUserDefaults+Utils.h"
#import "UIView+Utils.h"
#import "NSString+Utils.h"
#import "NSDate+Utils.h"
#import "GenderPickerView.h"
#import "BirthdayPickerView.h"
#import "IndustryPickerView.h"
#import "FunctionPickerView.h"
#import "NationPickerView.h"
#import "ErrorMessage.h"
#import "UIToolbar+Utils.h"

@interface EditProfileTableViewController ()<GenderPickerViewDelegate, BirthdayPickerViewDelegate, IndustryPickerViewDelegate, FunctionPickerViewDelegate, NationPickerViewDelegate, UITextFieldDelegate>

@end

NSDictionary *functionDict;
NSDictionary *industryDict;
NSDictionary *genderDict;
NSDictionary *nationlityDict;

@implementation EditProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _candidateProfile = [NSUserDefaults getProfile:kDictProfile];
    industryDict = [NSUserDefaults getFunctionList:kDictIndustryKey];
    functionDict = [NSUserDefaults getFunctionList:kDictFunctionKey];
    nationlityDict = [NSUserDefaults getNationality:kDictNationKey];
    
    [_ibEmailTextField setEnabled:YES];
    
    // Make tableView transparent
    [self.tableView setBackgroundView:nil];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    // Setup checkbox
    [_ibNotice setImage:[UIImage imageNamed:@"check_cir_dis"] forState:UIControlStateNormal];
    [_ibNotice setImage:[UIImage imageNamed:@"check_cir"] forState:UIControlStateSelected];
    [_ibSubscribe setImage:[UIImage imageNamed:@"check_cir_dis"] forState:UIControlStateNormal];
    [_ibSubscribe setImage:[UIImage imageNamed:@"check_cir"] forState:UIControlStateSelected];
    
    // Setup Picker Button
    [_ibPickGenderButton setImageEdgeInsets:UIEdgeInsetsMake(0, _ibPickGenderButton.width-5, 0, 0)];
    [_ibPickIndustryButton setImageEdgeInsets:UIEdgeInsetsMake(0, _ibPickIndustryButton.width-5, 0, 0)];
    [_ibPickFunctionButton setImageEdgeInsets:UIEdgeInsetsMake(0, _ibPickFunctionButton.width-5, 0, 0)];
    [_ibPickNationButton setImageEdgeInsets:UIEdgeInsetsMake(0, _ibPickNationButton.frame.size.width-5, 0, 0)];
    
    _ibExpYearsTextField.inputAccessoryView = [UIToolbar keyboardToolbar:self];
    _ibExpMonthsTextField.inputAccessoryView = [UIToolbar keyboardToolbar:self];
    
    // Load Profile
    [self loadCandidateProfile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView  willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Make  tableView transparent for slower devices
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (void)loadCandidateProfile
{
    _ibFullnameTextField.text = [NSString stringWithFormat:@"%@ %@", _candidateProfile.basicInfo.first_name, _candidateProfile.basicInfo.last_name];
    _ibEmailTextField.text = _candidateProfile.basicInfo.email;
    _ibCurrentLocationTextField.text = _candidateProfile.personalDetail.currentLocation;
    _ibMobileNumberTextField.text = _candidateProfile.personalDetail.phoneNumber;
    
    NSDate *birthday = [NSDate stringToDate:_candidateProfile.personalDetail.dateOfBirth];
    [_ibPickDateOfBirthButton setTitle: [NSDate birthdayFormat:birthday] forState:UIControlStateNormal];
    
    
    [_ibPickGenderButton setTitle:[self getGender:_candidateProfile.personalDetail.gender] forState:UIControlStateNormal];
    
    _ibJobTypeTextField.text = _candidateProfile.professionDetail.jobType;
    _ibExpYearsTextField.text = _candidateProfile.professionDetail.experienceYears;
    _ibExpMonthsTextField.text = _candidateProfile.professionDetail.experienceMonths;
    
    NSString *industry = @"";
    if ([_candidateProfile.professionDetail.currentIndustry isEqual:@"0"])
    {
        industry = @"Current Industry";
    }
    else
    {
        industry = [NSString getIndustryName:_candidateProfile.professionDetail.currentIndustry dict:industryDict];
    }
    
    [_ibPickIndustryButton setTitle:industry forState:UIControlStateNormal];
    
    NSString *function = @"";
    if ([_candidateProfile.professionDetail.function isEqual:@"0"])
    {
        function = @"Function";
    }
    else
    {
        function = [NSString getFunctionName:_candidateProfile.professionDetail.function dict:functionDict];
    }
    [_ibPickFunctionButton setTitle:function forState:UIControlStateNormal];
    
    NSString *nation = @"";
    if ([_candidateProfile.personalDetail.nationality isEqual:@"0"]) {
        nation = @"Nationality";
    }else{
        nation = [NSString getNationalityName:_candidateProfile.personalDetail.nationality dict:nationlityDict];
    }
    [_ibPickNationButton setTitle:nation forState:UIControlStateNormal];
    
    _ibKeySkillsTextField.text = _candidateProfile.professionDetail.keySkills;
    _ibNotice.selected = _candidateProfile.preference.notice;
    _ibSubscribe.selected = _candidateProfile.preference.subscribe;
}

-(IBAction)showGenderPickerView:(id)sender{
    GenderPickerView *pickerView = (id)[GenderPickerView viewWithNibName:@"GenderPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
    pickerView.valueSelected = [self getGender:_candidateProfile.personalDetail.gender];
    [self.view.superview.superview addSubview:pickerView];
    [self.tableView setUserInteractionEnabled:NO];
}

-(IBAction)showDatePickerView:(id)sender{
    BirthdayPickerView *pickerView = (id)[BirthdayPickerView viewWithNibName:@"BirthdayPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height,
                                  [UIView widthDevice], pickerView.frame.size.height);
    pickerView.dateSelect = [NSDate stringToDate:_candidateProfile.personalDetail.dateOfBirth];
    [self.view.superview.superview addSubview:pickerView];
    [self.tableView setUserInteractionEnabled:NO];
}

-(IBAction)showIndustryPickerView:(id)sender{
    IndustryPickerView *pickerView = (id)[IndustryPickerView viewWithNibName:@"IndustryPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
    pickerView.industryIdSelected = _candidateProfile.professionDetail.currentIndustry;
    [self.view.superview.superview addSubview:pickerView];
    [self.tableView setUserInteractionEnabled:NO];
}

-(IBAction)showFunctionPickerView:(id)sender{
    FunctionPickerView *pickerView = (id)[FunctionPickerView viewWithNibName:@"FunctionPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
    pickerView.functionIdSelected = _candidateProfile.professionDetail.function;
    [self.view.superview.superview addSubview:pickerView];
    [self.tableView setUserInteractionEnabled:NO];
}
- (IBAction)showNationPickerView:(id)sender {
    NationPickerView *pickerView = (id)[NationPickerView viewWithNibName:@"NationPickerView"];
    pickerView.delegate = self;
    pickerView.frame = CGRectMake(0, [UIView heightDevice] - pickerView.frame.size.height, [UIView widthDevice], pickerView.frame.size.height);
    pickerView.nationIdSelected = _candidateProfile.personalDetail.nationality;
    [self.view.superview.superview addSubview:pickerView];
    [self.tableView setUserInteractionEnabled:NO];
}
- (IBAction)checkNotice:(id)sender {
    _ibNotice.selected = !_ibNotice.selected;
}
- (IBAction)checkSubscribe:(id)sender {
    _ibSubscribe.selected = !_ibSubscribe.selected;
}


#pragma mark - GenderPickerView's Delegate
-(void)willDismissGenderPickerView:(GenderPickerView *)view{
    [self.tableView setUserInteractionEnabled:YES];
    [view removeFromSuperview];
}

-(void)didSelectComponentInGenderPickerView:(GenderPickerView *)view value:(NSString *)value{
    NSLog(@"Value: %@", value);
    _candidateProfile.personalDetail.gender = [self getGenderValue:value];
    [_ibPickGenderButton setTitle:[self getGender:_candidateProfile.personalDetail.gender] forState:UIControlStateNormal];
    if ([value isEqualToString:@"Male"]) {
        [_ibPickGenderButton setImageEdgeInsets: UIEdgeInsetsMake(0,30,0,0)];
    }else if ([value isEqualToString:@"Female"]){
        [_ibPickGenderButton setImageEdgeInsets: UIEdgeInsetsMake(0,70,0,0)];
    }
}

#pragma mark - BirthdayPickerView's Delegate
-(void)willDismissBirthdayPickerView:(BirthdayPickerView *)view{
    [self.tableView setUserInteractionEnabled:YES];
    [view removeFromSuperview];
}

-(void)didSelectInBirthdayPickerView:(BirthdayPickerView *)view date:(NSDate *)date{
    // day of birth
    if ( [NSString isEmptyString:[NSDate dateFormatDefault:date]] || ![NSDate isValidDateOfBirth:[NSDate dateFormatDefault:date]]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:KErrorMessageBirthInvalid delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    _candidateProfile.personalDetail.dateOfBirth = [NSDate dateFormatDefault:date];
    NSDate *birthday = [NSDate stringToDate:_candidateProfile.personalDetail.dateOfBirth];
    [_ibPickDateOfBirthButton setTitle: [NSDate birthdayFormat:birthday] forState:UIControlStateNormal];
    [self.tableView setUserInteractionEnabled:YES];
    [view removeFromSuperview];
}

#pragma mark - IndustryPickerView's Delegate
-(void)willDismissIndustryPickerView:(IndustryPickerView *)view{
    [self.tableView setUserInteractionEnabled:YES];
    [view removeFromSuperview];
}

-(void)didSelectComponentInIndustryPickerView:(IndustryPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Industry Value: %@", [value objectForKey:kDictIndustryName]);
   
    _candidateProfile.professionDetail.currentIndustry = [[value objectForKey:kDictIndustryId] stringValue];
    NSString *stringTitle = [NSString getIndustryName:_candidateProfile.professionDetail.currentIndustry dict:industryDict];
    [_ibPickIndustryButton setTitle:stringTitle forState:UIControlStateNormal];
    [_ibPickIndustryButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, stringTitle.length)];
}

#pragma mark - FunctionPickerView's Delegate
-(void)willDismissFunctionPickerView:(FunctionPickerView *)view{
    [self.tableView setUserInteractionEnabled:YES];
    [view removeFromSuperview];
}

-(void)didSelectComponentInFunctionPickerView:(FunctionPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Function Value: %@", [value objectForKey:kDictFunctionName]);
    _candidateProfile.professionDetail.function = [[value objectForKey:kDictFunctionId] stringValue];
    [_ibPickFunctionButton setTitle:[NSString getFunctionName:_candidateProfile.professionDetail.function dict:functionDict] forState:UIControlStateNormal];
}

-(void)willDismissNationPickerView:(NationPickerView *)view{
    [self.tableView setUserInteractionEnabled:YES];
    [view removeFromSuperview];
}
-(void)didSelectComponentInNationPickerView:(NationPickerView *)view value:(NSDictionary *)value{
    NSLog(@"Nationality Value: %@", [value objectForKey:kDictNationalityName]);
    _candidateProfile.personalDetail.nationality = [value objectForKey:kDictNationality];
    [_ibPickNationButton setTitle:[NSString getNationalityName:_candidateProfile.personalDetail.nationality dict:nationlityDict] forState:UIControlStateNormal];
}

-(Profile *)getUpdatedProfile
{
    // Basic info
    _candidateProfile.basicInfo.email = _ibEmailTextField.text;
    _candidateProfile.basicInfo.first_name = _ibFullnameTextField.text;
    _candidateProfile.basicInfo.last_name = @"";
   
    // Personal info
    _candidateProfile.personalDetail.currentLocation = _ibCurrentLocationTextField.text;
    _candidateProfile.personalDetail.phoneNumber = _ibMobileNumberTextField.text;
    
    // Profession info
    _candidateProfile.professionDetail.jobType = _ibJobTypeTextField.text;
    _candidateProfile.professionDetail.experienceMonths = _ibExpMonthsTextField.text;
    _candidateProfile.professionDetail.experienceYears = _ibExpYearsTextField.text;
    _candidateProfile.professionDetail.keySkills = _ibKeySkillsTextField.text;
    
    //Preference
    _candidateProfile.preference.notice = _ibNotice.selected;
    _candidateProfile.preference.subscribe = _ibSubscribe.selected;
    
    return _candidateProfile;
}


-(NSString*)getGender:(NSString*)value{
    NSString *gender = @"";
    
    if ([_candidateProfile.personalDetail.gender isEqual:@"0"])
    {
        gender = @"Male";
    }
    else if ([_candidateProfile.personalDetail.gender isEqual:@"1"])
    {
        gender = @"Female";
    }
    else
    {
        gender = @"Select Gender";
    }
    
    return gender;
}

-(NSString*)getGenderValue:(NSString*)name{
    NSString *gender = @"";
    
    if ([name isEqual:@"Male"])
    {
        gender = @"0";
    }
    else if ([name isEqual:@"Female"])
    {
        gender = @"1";
    }
    else
    {
        gender = @"-1";
    }
        
    return gender;
}
#pragma mark - UITextField's Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    int allowedLength;
    switch(textField.tag) {
        case 4:
            allowedLength = 12;   // triggered for input fields with tag = 1
            break;
        default:
            allowedLength = 255;   // length default when no tag (=0) value =255
            break;
    }
    
    if (textField.text.length >= allowedLength && range.length == 0) {
        return NO; // Change not allowed
    } else {
        return YES; // Change allowed
    }
}

-(BOOL)endEditing:(BOOL)force{
    [self.view endEditing:YES];
    return YES;
}


@end
