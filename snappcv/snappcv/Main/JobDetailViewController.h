//
//  JobDetailViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Job.h"
@class TTTAttributedLabel;
@interface JobDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ibJobImageView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibExperienceYearLabel;
@property (weak, nonatomic) IBOutlet UIButton *ibRatingButton;
@property (weak, nonatomic) IBOutlet UITextView *ibTopTextView;
@property (weak, nonatomic) IBOutlet UITextView *ibBottomTextView;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibCompanyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibCompanyLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibJobTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibPostTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibOverviewCompanyLabel;
@property (strong, nonatomic) Job *aJob;
@property (weak, nonatomic) IBOutlet UIButton *ibApplyJobButton;
@end
