//
//  ApplicationHistoryViewController.m
//  snappcv
//
//  Created by Staff on 4/10/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "ApplicationHistoryViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "RecommendedJobTableViewCell.h"
#import "JobDetailViewController.h"
#import "APIManager.h"
#import "Constants.h"
#import "UIView+Utils.h"

static NSString *kRecommendedJobTableViewCell =@"RecommendedJobTableViewCell";
#define kHEIGHT_TABLE_CELL  100.0


@interface ApplicationHistoryViewController()
@property (strong, nonatomic) NSArray *arrHistoryJobs;
@end

@implementation ApplicationHistoryViewController


- (IBAction)gotoMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    _ibTitleLabel.font = kFontDefaultForTitle;
    [_ibHistoryJobsTableView registerNib:[UINib nibWithNibName:kRecommendedJobTableViewCell bundle:nil] forCellReuseIdentifier:kRecommendedJobTableViewCell];
    _ibHistoryJobsTableView.dataSource = self;
    _ibHistoryJobsTableView.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [[APIManager sharedManager] getListJobs:@{kDictJobIsApplied:@(YES)} completion:^(id result, BOOL success, NSString *message) {
        if (success) {
            _arrHistoryJobs =  [NSArray arrayWithArray:result];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                BOOL isScrollOver = ([_arrHistoryJobs count]*kHEIGHT_TABLE_CELL) > _ibHistoryJobsTableView.height ? YES : NO;
                _ibHistoryJobsTableView.scrollEnabled = isScrollOver;
                [_ibHistoryJobsTableView reloadData];
            });
        }
    }];
    
}

#pragma mark UITableview Datasource and Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrHistoryJobs count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kHEIGHT_TABLE_CELL;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommendedJobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRecommendedJobTableViewCell];
    [cell setCellInfo:[_arrHistoryJobs objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JobDetailViewController *jobDetailsVC = (id)[self.storyboard instantiateViewControllerWithIdentifier:kJobDetailViewController];
    jobDetailsVC.aJob =  [_arrHistoryJobs objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:jobDetailsVC animated:YES];
}
@end
