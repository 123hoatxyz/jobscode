//
//  MainViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "MainViewController.h"
#import "MenuViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    id leftSideDrawerViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenuNavControllerID"];
    id centerDrawerViewController    = [self.storyboard instantiateViewControllerWithIdentifier:@"CenterNavControllerID"];
    id rightSideDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RightMenuNavControllerID"];
    
    self.centerViewController = centerDrawerViewController;
    self.leftDrawerViewController = leftSideDrawerViewController;
    self.rightDrawerViewController = rightSideDrawerViewController;
    self.maximumLeftDrawerWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    self.maximumRightDrawerWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    //self.animationVelocity = 1;
    [self setShowsShadow:NO];
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
