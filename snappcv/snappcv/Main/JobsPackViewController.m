//
//  JobsPackViewController.m
//  snappcv
//
//  Created by Staff on 3/12/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "JobsPackViewController.h"
#import "JobPackTableViewCell.h"
#import "Constants.h"

static NSString *kJobPackTableViewCell = @"JobPackTableViewCell";

@implementation NSMutableDictionary (Utils)

+(id)dictWithJobNumber:(NSString*)jobNumber1 money1:(NSString*)money1 jobNumber2:(NSString*)jobNumber2 money2:(NSString*)money2{
    return [NSMutableDictionary dictionaryWithObjects:@[jobNumber1,money1,jobNumber2,money2] forKeys:@[kDictJobsNumber1, kDictMoney1, kDictJobsNumber2 , kDictMoney2]];
}

@end

@interface JobsPackViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *arrJobsPack;
@end

@implementation JobsPackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _ibTitleLabel.font = kFontDefaultForTitle;
    _arrJobsPack = [NSMutableArray arrayWithCapacity:3];
    [_arrJobsPack addObject:[NSMutableDictionary dictWithJobNumber:@"10 Jobs" money1:@"$7.99" jobNumber2:@"20 Jobs" money2:@"$14.99"]];
    [_arrJobsPack addObject:[NSMutableDictionary dictWithJobNumber:@"30 Jobs" money1:@"$21.99" jobNumber2:@"40 Jobs" money2:@"$28.99"]];
    [_arrJobsPack addObject:[NSMutableDictionary dictWithJobNumber:@"50 Jobs" money1:@"$36.99" jobNumber2:@"100 Jobs" money2:@"$68.99"]];
    
     [_ibJobsPackTableView registerNib:[UINib nibWithNibName:kJobPackTableViewCell bundle:nil] forCellReuseIdentifier:kJobPackTableViewCell];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoMenu:(id)sender {
    
}

#pragma mark UITableViewDatasource and Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrJobsPack count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JobPackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kJobPackTableViewCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setCellInfo:_arrJobsPack[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 171;
}
@end
