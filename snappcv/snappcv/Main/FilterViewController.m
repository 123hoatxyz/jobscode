//
//  FilterViewController.m
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import "FilterViewController.h"
#import "Constants.h"
#import "UIViewController+MMDrawerController.h"
#import "HeaderInfo.h"
#import "HeaderFilterView.h"
#import "FilterJobsLocationCell.h"
#import "FilterJobsSalaryCell.h"
#import "RecommendJobsShared.h"
#import "FilterJobsExperienceCell.h"
#import "NSMutableDictionary+FilterUtils.h"
#import "FilterTableViewController.h"
#import "NSString+Utils.h"
#import "NSArray+TSCUtils.h"

#define kMaximumCharacterAllowed                40.0

static NSString *kHeaderFilterView = @"HeaderFilterView";
static NSString *kFilterJobsLocationCell = @"FilterJobsLocationCell";
static NSString *kFilterJobsSalaryCell = @"FilterJobsSalaryCell";
static NSString *kFilterJobsExperienceCell = @"FilterJobsExperienceCell";

@interface FilterViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, HeaderFilterViewDelegate, FilterJobsSalaryCellDelegate, FilterJobsExperienceCellDelegate>

@property (nonatomic, weak) IBOutlet UIView *ibRelevanceHighLightView;
@property (nonatomic, weak) IBOutlet UIView *ibFreshnessHighLightView;
@property (nonatomic, strong) NSMutableArray *arrJobFilters;
@property (nonatomic, assign) BOOL isActiveSearch;
@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([_ibSearchField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        _ibSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:kTitleSearchKeyword attributes:@{NSForegroundColorAttributeName:kColorPlaceHolder}];
    }
    
    //Setup TableView
    [_ibJobsFilterTableView registerNib:[UINib nibWithNibName:kHeaderFilterView bundle:nil] forHeaderFooterViewReuseIdentifier:kHeaderFilterView];
    [_ibJobsFilterTableView registerNib:[UINib nibWithNibName:kFilterJobsLocationCell bundle:nil] forCellReuseIdentifier:kFilterJobsLocationCell];
    [_ibJobsFilterTableView registerNib:[UINib nibWithNibName:kFilterJobsSalaryCell bundle:nil] forCellReuseIdentifier:kFilterJobsSalaryCell];
    [_ibJobsFilterTableView registerNib:[UINib nibWithNibName:kFilterJobsExperienceCell bundle:nil] forCellReuseIdentifier:kFilterJobsExperienceCell];
    _ibTitleLabel.font = kFontDefaultForTitle;
    
    [_ibSearchField addTarget:self action:@selector(searchingChangedText:) forControlEvents:UIControlEventEditingChanged];
    
    _ibSearchField.delegate = self;
    
    _isActiveSearch = NO;
    
    [self registerForKeyboardNotifications];
}

#pragma mark - UIKeyboard
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSLog(@"Height: %f", kbSize.height);
    UIEdgeInsets contentInset = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    _ibJobsFilterTableView.contentInset = contentInset;
    _ibJobsFilterTableView.scrollIndicatorInsets = contentInset;
    
}

-(void)keyboardWillBeHidden:(NSNotification*)aNotification{
    
    _ibJobsFilterTableView.contentInset = UIEdgeInsetsZero;
    _ibJobsFilterTableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSInteger section = [self sectionDidOpen];
    if (section != -1) {
        [self sectionHeaderView:nil sectionClosed:section];
    }
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSArray *arrLocations = [RecommendJobsShared defaultShared].arrayDictionaryMatchedLocation;
    NSMutableArray *arrLocationName = [NSMutableArray arrayWithCapacity:arrLocations.count];
    for (NSDictionary *dict in arrLocations) {
        NSMutableDictionary *newDic = [NSMutableDictionary dictWithTitle:[dict valueForKey:kDictJobLocationName] valueId:[dict valueForKey:kDictJobLocationId] checked:NO];
        [arrLocationName addObject:newDic];
    }
    
    NSArray *arrCompanies = [RecommendJobsShared defaultShared].arrayDicCompany;
    
    NSArray *arrHeaders = @[
                            [NSMutableDictionary dictWithTitle:@"Latest Jobs" subTitles:nil identifier:@""],
                            [NSMutableDictionary dictWithTitle:@"Top Employers" subTitles:arrCompanies identifier:kFilterJobsLocationCell],
                            [NSMutableDictionary dictWithTitle:@"Jobs by Location"
                                                     subTitles:arrLocationName identifier:kFilterJobsLocationCell
                             ],
                            [NSMutableDictionary dictWithTitle:@"Jobs Experiences" subTitles:@[[NSMutableDictionary dictWithExperienceYear:@""]] identifier:kFilterJobsExperienceCell],
                            [NSMutableDictionary dictWithTitle:@"Jobs by Salary" subTitles:@[
                                                                                             [NSMutableDictionary dictWithMinSalary:@"0.0" maxSalary:@"0.0"]
                                                                                             ] identifier:kFilterJobsSalaryCell]
                            ];
    self.openSectionIndex = NSNotFound;
    
    if ((_arrJobFilters == nil) ||
        ([self.arrJobFilters count] != [self numberOfSectionsInTableView:self.ibJobsFilterTableView])) {
        
        _arrJobFilters =[[NSMutableArray alloc]initWithCapacity:10];
        
        for (NSInteger idx = 0; idx < [arrHeaders count]; idx++) {
            HeaderInfo *aHeaderInfo = [[HeaderInfo alloc] init];
            NSMutableDictionary *tempHeader = [arrHeaders objectAtIndex:idx];
            aHeaderInfo.infos = tempHeader;
            aHeaderInfo.rows = [tempHeader countItemsOfSubTitle];
            aHeaderInfo.open = NO;
            if ([[tempHeader getIdentifier] isEqualToString:@""]
                || [[tempHeader getIdentifier] isEqualToString:kFilterJobsLocationCell]
                || [[tempHeader getIdentifier] isEqualToString:kFilterJobsExperienceCell]) {
                for (NSInteger row = 0; row < aHeaderInfo.rows; row++) {
                    [aHeaderInfo insertObject:@44 inRowHeightsAtIndex:row];
                }
            }else{
                for (NSInteger row = 0; row < aHeaderInfo.rows; row++) {
                    [aHeaderInfo insertObject:@158 inRowHeightsAtIndex:row];
                }
            }
            
            [_arrJobFilters addObject:aHeaderInfo];
        }
    }
    [_ibJobsFilterTableView reloadData];
    //hidden table view
    _ibTableSearch.hidden = _isActiveSearch ? NO:YES;
    _ibOptionsFilterView.hidden = _isActiveSearch ? YES:NO;
}

#pragma mark UITableViewDatasource and Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_arrJobFilters count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    HeaderInfo *aHeaderInfo = [_arrJobFilters objectAtIndex:section];
    return (aHeaderInfo.open ? aHeaderInfo.rows : 0);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HeaderInfo *sectionInfo = (_arrJobFilters)[indexPath.section];
    return [[sectionInfo objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    HeaderInfo *headerInfo = [_arrJobFilters objectAtIndex:section];
    HeaderFilterView *headerFilterView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kHeaderFilterView];
    headerFilterView.section = section;
    headerFilterView.delegate = self;
    
    [headerFilterView setHeaderInfo:[headerInfo.infos getTitle]];
    headerInfo.headerFilterView = headerFilterView;
    return headerFilterView;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HeaderInfo *headerInfo = [_arrJobFilters objectAtIndex:indexPath.section];
    
    if ([[headerInfo.infos getIdentifier] isEqualToString:kFilterJobsLocationCell]) {
        FilterJobsLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:kFilterJobsLocationCell];
        
        NSArray *arrSubTitles = [headerInfo.infos valueForKey:kDictSubTitles];
        [cell setCellInfo:[arrSubTitles objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if ([[headerInfo.infos getIdentifier] isEqualToString:kFilterJobsSalaryCell]){
        FilterJobsSalaryCell *cell = [tableView dequeueReusableCellWithIdentifier:kFilterJobsSalaryCell];
        cell.delegate = self;
        
        [cell setCellInfo:headerInfo.infos];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if ([[headerInfo.infos getIdentifier] isEqualToString:kFilterJobsExperienceCell]){
        FilterJobsExperienceCell *cell = [tableView dequeueReusableCellWithIdentifier:kFilterJobsExperienceCell];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setCellInfo:[[headerInfo.infos valueForKey:kDictSubTitles] firstObject]];
        return cell;
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"noCellIdentifier"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HeaderInfo *headerInfo = [_arrJobFilters objectAtIndex:indexPath.section];
    
    if ([[headerInfo.infos getIdentifier] isEqualToString:kFilterJobsLocationCell]) {
        NSArray *arrSubTitles = [headerInfo.infos valueForKey:kDictSubTitles];
        NSMutableDictionary *dictSubTitles = [arrSubTitles objectAtIndex:indexPath.row];
        [dictSubTitles setChecked:![dictSubTitles isChecked]];
        [_ibJobsFilterTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}
#pragma mark - SectionHeaderViewDelegate

- (void)sectionHeaderView:(HeaderFilterView *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    //close keyboard
    [self.view endEditing:YES];
    
    HeaderInfo *aHeaderInfo = [_arrJobFilters objectAtIndex:sectionOpened];
    if (aHeaderInfo.open) {
        [self sectionHeaderView:sectionHeaderView sectionClosed:sectionOpened];
        return;
    }
    aHeaderInfo.open = YES;
    
    
    NSInteger countOfRowsToInsert = aHeaderInfo.rows;
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    
    if (previousOpenSectionIndex != NSNotFound) {
        
        HeaderInfo *previousOpenSection = [_arrJobFilters objectAtIndex:previousOpenSectionIndex];
        previousOpenSection.open = NO;
        [previousOpenSection.headerFilterView toggleOpenWithUserAction:NO];
        NSInteger countOfRowsToDelete = previousOpenSection.rows;
        
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
    }
    
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationNone;
        deleteAnimation = UITableViewRowAnimationNone;
    }
    else {
        insertAnimation = UITableViewRowAnimationNone;
        deleteAnimation = UITableViewRowAnimationNone;
    }
    
    // apply the updates
    [self.ibJobsFilterTableView beginUpdates];
    [self.ibJobsFilterTableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.ibJobsFilterTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.ibJobsFilterTableView endUpdates];
    self.openSectionIndex = sectionOpened;
    
    [self performSelectorOnMainThread:@selector(tableViewScrollCurrentCell) withObject:nil waitUntilDone:1.0];
    
}

-(void)tableViewScrollCurrentCell{
    if (self.openSectionIndex) {
        [self.ibJobsFilterTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:self.openSectionIndex] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
}

- (void)sectionHeaderView:(HeaderFilterView *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    
    HeaderInfo *aHeaderInfo = [_arrJobFilters objectAtIndex:sectionClosed];
    aHeaderInfo.open = NO;
    
    NSInteger countOfRowsToDelete = [self.ibJobsFilterTableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.ibJobsFilterTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationNone];
    }
    self.openSectionIndex = NSNotFound;
    [self.view endEditing:YES];
}

-(NSInteger)sectionDidOpen{
    NSInteger count = 0;
    for (HeaderInfo *headerInfo in _arrJobFilters) {
        if (headerInfo.open == YES) {
            return count;
            break;
        }
        count++;
    }
    return -1;
}

#pragma mark - Event
- (IBAction)modifySearch:(id)sender {
    _isActiveSearch = NO;
    _ibTableSearch.hidden = _isActiveSearch?NO:YES;
    _ibOptionsFilterView.hidden = _isActiveSearch ? YES:NO;
}

- (IBAction)closeButtonDidTouch:(id)sender {
    _isActiveSearch = NO;
    
    _ibSearchField.text = @"";
    
    FilterTableViewController *filterTable = (FilterTableViewController*)[self.childViewControllers firstObject];
    filterTable.stringHighlighted = @"";
    [filterTable clearDataTable];
    //clear data soure
    [self clearInfoDataSource];
    
    NSInteger section = [self sectionDidOpen];
    if (section != -1) {
        [self sectionHeaderView:nil sectionClosed:section];
    }
    [self.view endEditing:YES];
    [self.mm_drawerController closeDrawerAnimated:YES completion:NULL];
}


-(void)clearInfoDataSource{
    //0. Top Company
    HeaderInfo *headerInfo;
    headerInfo = [_arrJobFilters objectAtIndex:1];
    NSArray *arrSubTitlesCompany = [headerInfo.infos valueForKey:kDictSubTitles];
    
    for (NSMutableDictionary *dict in arrSubTitlesCompany) {
        [dict setChecked:NO];
    }
    
    //1. Location
    headerInfo = [_arrJobFilters objectAtIndex:2];
    NSArray *arrSubTitlesLocation = [headerInfo.infos valueForKey:kDictSubTitles];
    
    for (NSMutableDictionary *dict in arrSubTitlesLocation) {
        [dict setChecked:NO];
    }
    
    
    //2. Experience
    headerInfo = [_arrJobFilters objectAtIndex:3];
    
    NSMutableDictionary *dicExperience = [[headerInfo.infos valueForKey:kDictSubTitles] firstObject];
    [dicExperience setExperienceYear:@""];
    
    //3. Salary
    headerInfo = [_arrJobFilters objectAtIndex:4];
    
    NSMutableDictionary *dicSalary = [[headerInfo.infos valueForKey:kDictSubTitles] firstObject];
    [dicSalary setMinSalary:0];
    [dicSalary setMaxSalary:0];
   
    //[_ibJobsFilterTableView reloadData];
}
#pragma mark - Searching
- (IBAction)submitButtonDidTouch:(id)sender {
    
    //0. Top Company
    HeaderInfo *headerInfo;
    headerInfo = [_arrJobFilters objectAtIndex:1];
    NSArray *arrSubTitlesCompany = [headerInfo.infos valueForKey:kDictSubTitles];
    NSMutableArray *arrDidSelectCompany = [NSMutableArray arrayWithCapacity:[arrSubTitlesCompany count]];
    for (NSMutableDictionary *dict in arrSubTitlesCompany) {
        if ([dict isChecked]) {
            [arrDidSelectCompany addObject:dict];
        }
    }
    NSLog(@"Info Companies Are Selected: %@", arrDidSelectCompany);
    
    //1. Location
    headerInfo = [_arrJobFilters objectAtIndex:2];
    NSArray *arrSubTitlesLocation = [headerInfo.infos valueForKey:kDictSubTitles];
    NSMutableArray *arrDidSelectLocation = [NSMutableArray arrayWithCapacity:[arrSubTitlesLocation count]];
    for (NSMutableDictionary *dict in arrSubTitlesLocation) {
        if ([dict isChecked]) {
            [arrDidSelectLocation addObject:dict];
        }
    }
    NSLog(@"Info Locations Are Selected: %@", arrDidSelectLocation);
    
    //2. Experience
    headerInfo = [_arrJobFilters objectAtIndex:3];
    NSArray *arrSubTitlesExperience = [headerInfo.infos valueForKey:kDictSubTitles];
    NSMutableDictionary *dicExperience = [arrSubTitlesExperience firstObject];
    NSLog(@"Info experience's year: %@", [dicExperience getExperienceYear]);
    
    //3. Salary
    headerInfo = [_arrJobFilters objectAtIndex:4];
    NSArray *arrSubTitlesSalary = [headerInfo.infos valueForKey:kDictSubTitles];
    NSMutableDictionary *dicSalary = [arrSubTitlesSalary firstObject];
    NSLog(@"Info Salary Max: %@, Min: %@", [dicSalary getMaxSalary], [dicSalary getMinSalary]);
    
    /*
     
     "experience_year": Number,
     "min_salary": Number,
     "max_salary": Number,
     "top_employers": [ Number, Number, Number,...], //// "top_employers": "number,number,.."
     "jobs_by_location": [ Number, Number, Number,...], /// "jobs_by_location": "number,number,.."
     
     */
    NSString *experience_year = [dicExperience getExperienceYear];
    NSMutableDictionary *dicParam = [[NSMutableDictionary alloc] init];
    if (![NSString isEmptyString:experience_year]) {
        [dicParam setValue:experience_year forKey:kDictJobExperience];
    }
    
    NSNumber *min_salary = [dicSalary getMinSalary];
    NSLog(@"Submit filter min_salary: %@",min_salary);
    if (min_salary != 0) {
        [dicParam setObject:min_salary forKey:kDictMinSalary];
    }else{
        [dicParam removeObjectForKey:kDictMinSalary];
    }
    
    NSNumber *max_salary = [dicSalary getMaxSalary];
    NSLog(@"Submit filter max_salary: %@",max_salary);
    if (max_salary != 0) {
        [dicParam setObject:max_salary forKey:kDictMaxSalary];
    }else{
        [dicParam removeObjectForKey:kDictMaxSalary];
    }
    
    NSArray *top_employers = [arrDidSelectCompany distinctUnionObjectsForKeyPath:kDictValueId];
    if ([top_employers count]) {
        NSString *JSON = [NSString stringWithFormat:@"[%@]", [top_employers componentsJoinedByString:@","]];
        [dicParam setValue:JSON forKey:kDictTopEmployers];
    }
    
    NSArray *jobs_by_location = [arrDidSelectLocation distinctUnionObjectsForKeyPath:kDictValueId];
    if ([jobs_by_location count]) {
         NSString *JSON = [NSString stringWithFormat:@"[%@]", [jobs_by_location componentsJoinedByString:@","]];
        [dicParam setValue:JSON forKey:kDictJobByLocation];
    }
    
    
    [_ibSearchField resignFirstResponder];
    // [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
    _isActiveSearch = YES;
    _ibTableSearch.hidden = _isActiveSearch?NO:YES;
    _ibOptionsFilterView.hidden = _isActiveSearch ? YES:NO;
    for (UIViewController *viewController in self.childViewControllers) {
        if ([viewController isKindOfClass:[FilterTableViewController class]]) {
            FilterTableViewController *filterTable = (FilterTableViewController*)viewController;
            filterTable.ibSearchField = _ibSearchField;
            filterTable.params = dicParam;
            [filterTable reloadDataTable];
            break;
        }
    }
    
}


- (IBAction)relevanceButtonDidTouch:(id)sender {
    self.ibRelevanceHighLightView.backgroundColor = kColorFilterRelevanceHighLightView_Highlighted;
    self.ibFreshnessHighLightView.backgroundColor = kColorFilterFreshnessHighLightView;
}

- (IBAction)freshnessButtonDidTouch:(id)sender {
    self.ibRelevanceHighLightView.backgroundColor = kColorFilterRelevanceHighLightView;
    self.ibFreshnessHighLightView.backgroundColor = kColorFilterFreshnessHighLightView_Highlighted;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}


#pragma mark - FilterJobsSalaryCell's Delegate
-(void)didChangeMaxSalaryFromCell:(FilterJobsSalaryCell *)cell maxValue:(NSNumber *)maxValue{
    NSIndexPath *indexPath = [_ibJobsFilterTableView  indexPathForCell:cell];
    HeaderInfo *headerInfo = [_arrJobFilters objectAtIndex:indexPath.section];
    NSArray *arrSubTitles = [headerInfo.infos valueForKey:kDictSubTitles];
    NSMutableDictionary *dictSubTitles = [arrSubTitles objectAtIndex:0];
    [dictSubTitles setMaxSalary:maxValue];
}

-(void)didChangeMinSalaryFromCell:(FilterJobsSalaryCell *)cell minValue:(NSNumber *)minValue{
    NSIndexPath *indexPath = [_ibJobsFilterTableView  indexPathForCell:cell];
    HeaderInfo *headerInfo = [_arrJobFilters objectAtIndex:indexPath.section];
    NSArray *arrSubTitles = [headerInfo.infos valueForKey:kDictSubTitles];
    NSMutableDictionary *dictSubTitles = [arrSubTitles objectAtIndex:0];
    [dictSubTitles setMinSalary:minValue];
}

#pragma mark - FilterJobsExperienceCell's Delegate
-(void)didChangeTextAtCell:(FilterJobsExperienceCell *)cell valueChanged:(NSString *)valueChanged{
    NSInteger expNumber = [valueChanged integerValue];
    if (expNumber > kMaximumCharacterAllowed) {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Year has to less and equal 40" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
    NSIndexPath *indexPath = [_ibJobsFilterTableView indexPathForCell:cell];
    HeaderInfo *headerInfo = [_arrJobFilters objectAtIndex:indexPath.section];
    NSMutableDictionary *dicExperience = [[headerInfo.infos valueForKey:kDictSubTitles] firstObject];
    [dicExperience setExperienceYear:valueChanged];
}

//#pragma mark - UITextField Search's Delegate
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    [self.view endEditing:YES];
//}
//
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//    if (!decelerate) {
//        [self.view endEditing:YES];
//    }
//}

-(void)searchingChangedText:(id)sender{
    NSString *value = [[_ibSearchField text] lowercaseString];
    value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    FilterTableViewController *filterTable = (FilterTableViewController*)[self.childViewControllers firstObject];
    [filterTable highlightedText:value];

}

@end
