//
//  ChangePasswordViewController.m
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Constants.h"
#import "NSUserDefaults+Utils.h"
#import "Profile.h"
#import "UIView+Utils.h"
#import "UIImageView+AFNetworking.h"
#import "APIManager.h"
#import "UIImage+Addition.h"
#import "NSString+Utils.h"
#import "IndicatorViewHelper.h"

@import QuartzCore;
@import AssetsLibrary;
@import MobileCoreServices;

@interface ChangePasswordViewController ()<UITextFieldDelegate>

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupPlaceHolder];
    
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupPlaceHolder
{
    UIColor *color = [UIColor lightTextColor];
    _ibCurrentPasswordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Current Password" attributes:@{NSForegroundColorAttributeName: color}];
    _ibNewPasswordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: color}];
    _ibConfirmPasswordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
    _ibCurrentPasswordText.delegate = self;
    _ibConfirmPasswordText.delegate = self;
    _ibNewPasswordText.delegate = self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)gotoMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)send:(id)sender {
    if([self validate])
    {
        [IndicatorViewHelper showHUDInWindow:YES];
        
        [[APIManager sharedManager] changePassword:_ibCurrentPasswordText.text newPassword:_ibNewPasswordText.text completion:^(BOOL success, NSString *message) {
            if (!success) {
                // Fail
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                
                [IndicatorViewHelper hideAllHUDForWindow:YES];
            }else {
                // Success
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                
                [IndicatorViewHelper hideAllHUDForWindow:YES];
            }
        }];
    }
}

-(BOOL)validate{
    
    if ([_ibCurrentPasswordText.text isEqual:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Current password can not be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        return NO;
    }
    
    if ([_ibNewPasswordText.text isEqual:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"New password can not be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        return NO;
    }
    
    if ([_ibConfirmPasswordText.text isEqual:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Confirm password can not be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        return NO;
    }
    
    if ([_ibCurrentPasswordText.text isEqual:_ibNewPasswordText.text])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"The New password is the same as Current password. Please change it." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        return NO;
    }

    
    if (![_ibNewPasswordText.text isEqual:_ibConfirmPasswordText.text])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"New password and Confirm password don't match." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextView's Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

@end
