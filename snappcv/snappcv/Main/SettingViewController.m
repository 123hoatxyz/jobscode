//
//  SettingViewController.m
//  snappcv
//
//  Created by Staff on 4/11/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "SettingViewController.h"
#import "Constants.h"
#import "SettingTableViewCellA.h"
#import "SettingTableViewCellB.h"

static NSString *const kSettingTableViewCellA =@"SettingTableViewCellA";
static NSString *const kSettingTableViewCellB =@"SettingTableViewCellB";
@interface SettingViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _ibTitleLabel.font = kFontDefaultForTitle;
    [_ibSettingTableView registerNib:[UINib nibWithNibName:kSettingTableViewCellA bundle:nil] forCellReuseIdentifier:kSettingTableViewCellA];
     [_ibSettingTableView registerNib:[UINib nibWithNibName:kSettingTableViewCellB bundle:nil] forCellReuseIdentifier:kSettingTableViewCellB];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)gotoMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView's Datasource and Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != 1) {
        SettingTableViewCellA *cell = [tableView dequeueReusableCellWithIdentifier:kSettingTableViewCellA];
        cell.ibTitleLabel.text = (indexPath.row == 0)?@"Edit Profile":@"Change Password";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        SettingTableViewCellB *cell = [tableView dequeueReusableCellWithIdentifier:kSettingTableViewCellB];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            id editProfile = [self.storyboard instantiateViewControllerWithIdentifier:kEditProfileViewController];
            [self.navigationController pushViewController:editProfile animated:YES];
            break;
        }
        case 2:
        {
            id changePassword = [self.storyboard instantiateViewControllerWithIdentifier:kChangePasswordViewController];
            [self.navigationController pushViewController:changePassword animated:YES];
            break;
        }
        default:
            break;
    }
}
@end
