//
//  ApplyJobViewController.h
//  snappcv
//
//  Created by Dat Huynh Nguyen on 2/11/15.
//  Copyright (c) 2015 vuta. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TTTAttributedLabel, Job;

@interface ApplyJobViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *ibRatingButton;
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ibJobImageView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *ibExperienceYearLabel;

@property (weak, nonatomic) IBOutlet UILabel *ibCompanyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibCompanyLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibJobTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibPostTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibVideoIntroduceLabel;
@property (weak, nonatomic) IBOutlet UILabel *ibResumeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ibVideoCheckedIcon;
@property (weak, nonatomic) IBOutlet UIImageView *ibCVCheckedIcon;
@property (weak, nonatomic) IBOutlet UILabel *ibCVLabel;
@property (weak, nonatomic) IBOutlet UIButton *ibSubmitButton;

@property (weak, nonatomic) IBOutlet UIScrollView *ibContentScrollView;
@property (weak, nonatomic) IBOutlet UIView *ibContentView;
@property (strong, nonatomic) Job *aJob;
@end
