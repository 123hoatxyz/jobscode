//
//  WebViewController.m
//  snappcv
//
//  Created by Staff on 4/10/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import "WebViewController.h"
#import "UIView+Utils.h"
#import "Constants.h"

@implementation WebViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    _ibTitleLabel.font = kFontDefaultForTitle;
    _ibWebView.delegate = self;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _ibTitleLabel.text = _textTitle;
    NSURL *url = [[NSURL alloc] initWithString:_urlAddress];
    NSLog(@"Go to URL:%@", url.absoluteString);
    [_ibWebView loadRequest:[NSURLRequest requestWithURL:url]];
    _ibWebView.backgroundColor = [UIColor clearColor];
}

- (IBAction)gotoMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
