#import <Foundation/Foundation.h>

@class MBProgressHUD;


@interface IndicatorViewHelper : NSObject
+ (MBProgressHUD *)showHUDInWindow:(BOOL)animated;
+ (void)hideAllHUDForWindow:(BOOL)animated;
@end