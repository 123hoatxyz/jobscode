#import "IndicatorViewHelper.h"
#import "MBProgressHUD.h"

@interface IndicatorViewHelper ()
@end

@implementation IndicatorViewHelper {

}
+ (MBProgressHUD *)showHUDInWindow:(BOOL)animated {
    UIView *view = [UIApplication sharedApplication].delegate.window.rootViewController.view;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:animated];
    hud.labelText = @"Please wait...";
    hud.taskInProgress = YES;
    hud.graceTime = 0.1;
    return hud;
}

+ (void)hideAllHUDForWindow:(BOOL)animated {
    UIView *view = [UIApplication sharedApplication].delegate.window.rootViewController.view;
    [MBProgressHUD hideAllHUDsForView:view animated:animated];
}

@end