//
//  SnappCVApplication.h
//  snappcv
//
//  Created by Staff on 4/15/15.
//  Copyright (c) 2015 Hoat Ha Van. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SnappCVApplication : UIApplication
-(void)enableMultiTouches:(BOOL)enabled;
@end
